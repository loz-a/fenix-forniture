<?php
date_default_timezone_set('Europe/Kiev');

define('APPLICATION_ENV', 'development');
//define('APPLICATION_ENV', 'production');

defined('APPLICATION_PATH')
    || define('APPLICATION_PATH', realpath(dirname(__FILE__) . '/application' ));

set_include_path(implode(PATH_SEPARATOR, array(
    realpath(APPLICATION_PATH . '/../lib'),
    realpath(APPLICATION_PATH . '/../application'),
    get_include_path()
)));

defined('APPLICATION_LOG_FILE')
    || define('APPLICATION_LOG_FILE', APPLICATION_PATH . '/../data/log/error.log');


ini_set('session.gc_maxlifetime', 3600);
ini_set('session.cookie_lifetime', 3600);
ini_set('session.save_path', APPLICATION_PATH . '/../data/session');

try {
	// Bootstrap the application
	require_once 'Core/App/IApp.php';
    require_once 'Core/App/App.php';

	$app = \Core\App\App::GetInstance();
	set_error_handler(array($app, 'error'), E_ALL | E_STRICT);

	spl_autoload_register(array($app, 'autoload'));

	require_once APPLICATION_PATH . '/configs/application.php';

	$app -> run()
	     -> serve();

} catch ( \Exception $e ) {
	if ( !headers_sent() ) {
		header('HTTP/1.1 503 Service Temporarily Unavailable');
		header('Status: 503 Service Temporarily Unavailable');
	}
    $date = date('d.m.Y h:i:s');
    error_log($e -> getMessage() . '| Date: ' . $date . PHP_EOL, 3, APPLICATION_LOG_FILE);

	exit('Core Exception: ' . $e -> getMessage());
}


