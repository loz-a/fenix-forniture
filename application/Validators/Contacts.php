<?php
namespace Validators;

use \Core\App\App,
    \Core\Validate\Collection\Form,
    \Core\Validate\Collection\StringLength,
    \Core\Validate\Collection\Alnum,
    \Core\Validate\Collection\Regexpr,
    \Core\Validate\Collection\EmailAddress,
    \Core\Validate\Chain;

class Contacts extends Form
{
    public function init()
    {
        $this
            -> addChain('Address', $this -> _initAddressChain())
            -> addChain('Fax', $this -> _initFaxChain())
            -> addChain('Phone', $this -> _initPhoneChain())
            -> addChain('Email', $this -> _initEmailChain());
    } // init()


    protected function _initAddressChain()
    {
        $app  = App::GetInstance();
        $lang = $app -> getTranslate() -> getLocale();

        $chain = new Chain();
        $chain
            -> addValidator(new StringLength('Address',
                array(
                     'max' => 255
                )))
            -> addValidator(new Alnum('Address',
                array(
                     'cyrillic' => true,
                     'whitespace' => true,
                     'dot' => true,
                     'coma' => true
                )));
        return $chain;
    } // _initAddressChain()


    protected function _initFaxChain()
    {
        $chain = new Chain();
        $chain
            -> addValidator(new StringLength('Fax',
                array('max' => 255)
            ))
            -> addValidator(new Regexpr('Fax',
                array(
                     'pattern'  => '/^(\+[0-9]{1})?\s?(\([0-9]+\))?[0-9\s\-]+$/',
                     'messages' => array(Regexpr::NOT_MATCH => 'Fax number is wrong')
                )));
        return $chain;
    } // _initFaxChain()


    protected function _initPhoneChain()
    {
        $chain = new Chain();
        $chain
            -> addValidator(new StringLength('Phone',
                array('max' => 255)
            ))
            -> addValidator(new Regexpr('Phone',
                array(
                     'pattern'  => '/^(\+[0-9]{1})?\s?(\([0-9]+\))?[0-9\s\-]+$/',
                     'messages' => array(Regexpr::NOT_MATCH => 'Phone number is wrong')
                )));
        return $chain;
    } // _initPhoneChain()


    protected function _initEmailChain()
    {
        $chain = new Chain();
        $chain
            -> addValidator(new StringLength('Price',
                array(
                     'max' => 255
                )))
            -> addValidator(new EmailAddress('Email'));
        return $chain;
    } // _initEmailChain()

} // Contacts
