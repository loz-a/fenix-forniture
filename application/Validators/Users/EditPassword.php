<?php
namespace Validators\Users;

use \Core\Validate\Collection\Form,
    \Core\Validate\Collection\Required,
    \Core\Validate\Collection\StringLength,
    \Core\Validate\Collection\UserAuth,
    \Core\Validate\Collection\PasswordConfirm,
    \Core\Validate\Chain;

class EditPassword extends Form
{
    public function init()
    {
        $this
            -> addChain('current_password', $this -> _initCurrentPasswordChain())
            -> addChain('password', $this -> _initPasswordChain())
            -> addChain('confirm_password', $this -> _initConfirmChain());
    } // init()


    protected function _initCurrentPasswordChain()
    {
        $chain = new Chain();
        $chain
            -> addValidator(new Required())
            -> addValidator(new UserAuth());
        return $chain;
    } // _initCurrentPasswordChain()


    protected function _initPasswordChain()
    {
        $chain = new Chain();
        $chain
            -> addValidator(new Required())
            -> addValidator(new StringLength('New password',
                array(
                     'min' => \Models\Users::MIN_PASSWORD_LENGTH
                )));
        return $chain;
    } // _initPasswordChain()


    public function _initConfirmChain()
    {
        $chain = new Chain();
        $chain -> addValidator(new PasswordConfirm());
        return $chain;
    } // _initConfirmChain()

} // \Validators\Users\EditPassword
