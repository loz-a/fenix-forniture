<?php
namespace Plugins;

use \Core\Controller\AController as Controller,
    \Core\App\App,
    \Core\Helper\Collection\CacheIdGenerator;

class Cache extends \Core\Plugin\APlugin
{
    public function actionBefore()
    {
        $app = App::GetInstance();

        $cacheEnbled = $app -> getConfig('cache_enable');
        if ( !$cacheEnbled ) {
            return;
        }

        $cache = $this -> _getCacheInstance($app);
        $cacheId = CacheIdGenerator::Factory() -> generate();

        if ( $cacheId and $cache -> test($cacheId)) {
            $data = $cache -> load($cacheId);
            echo $data;
            die;
        }

        $app -> setCache($cache);
    } // actionBefore()


    protected function _getCacheInstance(\Core\App\App $app)
    {
        $cacheLifetime = $app -> getConfig('cache_lifetime');
        $cacheDir      = $app -> getConfig('cache_dir');

        $fEndOptions = array(
            'lifetime' => $cacheLifetime,
            'automatic_serialization' => true
        );

        $bEndOptions = array(
            'cache_dir' => $cacheDir
        );

        return \Zend_Cache::factory('Core', 'File', $fEndOptions, $bEndOptions);
    } // _getCacheInstance()


} // Cache
