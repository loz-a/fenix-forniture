<?php
namespace Plugins;

use \Core\Controller\AController as Controller;

class Acl extends \Core\Plugin\APlugin
{
    public function actionBefore()
    {
        $controller = strtolower($this -> _app -> getController() -> getControllerName());
        $action     = strtolower($this -> _app -> getController() -> getActionName());
        $role       = $this -> _app -> getConfig('acl_default_role');

        $auth = \Core\Auth\Auth::GetInstance();
        if ( $auth -> hasIdentity() ) {
            $role = $auth -> getIdentity() -> role;
        }

        if ( !$this -> _isAllowed($role, $controller, $action) ) {
            Controller::Redirect('/error/access-denied');
        }
    } // actionBefore()


    protected function _isAllowed($role, $controller, $action)
    {
        $configFile = $this -> _app -> getConfig('acl_config_file');
        $config = \Core\Config::Factory($configFile) -> toArray();

        array_walk_recursive($config, function(&$item, &$key) {
            $item = strtolower($item);
        });

        return in_array('all', $config[$role]['allow'])
            or in_array('all', $config[$role]['allow'][$controller])
            or in_array($action, $config[$role]['allow'][$controller]);
    } // _isAllowed()

} // \Plugins\Acl