<?php
namespace Plugins;

class Language extends \Core\Plugin\APlugin
{
    /**
     * @var bool $_needRedirect
     */
    protected $_needRedirect = false;

    public function dispatchBefore()
    {
        $this -> _checkSession();

        $translate = new \Core\Translate(
            array(
                 'availablesLocales' => $this -> _app -> getConfig('locale_availables'),
                 'defaultLocale'     => $this -> _app -> getConfig('locale_default'),
                 'localeFile'        => $this -> _app -> getConfig('locale_file')
            ));

        $locale = null;
        $locale = \Core\Locale::FromUri();
        if ( $locale ) {
            $this -> _needRedirect = true;
        }
        if ( ! $locale ) {
            $locale = \Core\Locale::FromSession();
        }
        $translate -> setLocale($locale);

        if ($this -> _needRedirect) {
            $target = preg_replace('/\/lang\/\w+($|\/)/i', '/', $_SERVER['REQUEST_URI']);
            \Core\Controller\AController::Redirect($target);
        }

        $translate -> loadData();
        $this -> _app -> setTranslate($translate);
    } // dispatchBefore()


    protected function _checkSession()
    {
        if ( ! isset($_SESSION) ) {
            session_start();
        }
    } // _checkSession()

} // Language