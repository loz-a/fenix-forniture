<?php
namespace Plugins;

class FlashMessenger extends \Core\Plugin\APlugin
{
    public function actionAfter()
    {
        $flashMessenger = $this -> _app -> helper -> flashMessenger();
        $flashMessenger -> stepForward();
    } // actionAfter()

} // \Plugins\FlashMessenger
