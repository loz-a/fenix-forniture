<?php
namespace Plugins;

class Orm extends \Core\Plugin\APlugin
{
    public function actionBefore()
    {
        $dbLoc    = $this -> _app -> getConfig('db_location');
        $dbName   = $this -> _app -> getConfig('db_name');
        $dbUser   = $this -> _app -> getConfig('db_user');
        $dbPasswd = $this -> _app -> getConfig('db_password');
        $dbLog    = $this -> _app -> getConfig('db_logging');

        $this -> _configure($dbLoc, $dbName, $dbUser, $dbPasswd, $dbLog);
    } // actionBefore


    protected function _configure($dbLoc, $dbName, $dbUser, $dbPasswd, $dbLog)
    {
        \Core\Orm::configure("mysql:host=$dbLoc;dbname=$dbName");
        \Core\Orm::configure('username', $dbUser);
        \Core\Orm::configure('password', $dbPasswd);
        \Core\Orm::configure('driver_options', array(\PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES utf8'));
        \Core\Orm::configure('logging', $dbLog);
    } // _configure()

} // \Plugins\Orm
