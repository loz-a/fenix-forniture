<?php
namespace Models;

use \Core\Orm,
    \Core\App\App,
    \Core\Model\Result,
    \Core\Helper\Collection\DataCleaner;

class Contacts
{
    public function getByKeyAndLang($key, $lang)
    {
        return Orm::for_table('contacts')
            -> select('key')
            -> select('value')
            -> select('lang')
            -> where_equal('key', $key)
            -> where_equal('lang', $lang)
            -> find_one();
    } // getByKey()


    public function fetchAll()
    {
        $lang = App::GetInstance() -> getTranslate() -> getLocale();
        $contacts = Orm::for_table('contacts')
            -> select('key')
            -> select('value')
            -> select('lang')
            -> where_equal('lang', $lang)
            -> find_many();

        $result = array();
        foreach ($contacts as $contact ) {
            $result[$contact -> key] = $contact -> value;
        }
        return $result;
    } // fetcthAll()


    public function edit(array $data)
    {
        $lang = App::GetInstance() -> getTranslate() -> getLocale();
        $result = new Result();
        $validator = new \Validators\Contacts();

        unset($data['token']);

        if ( $validator -> isValid($data) ) {
            $data = DataCleaner::Factory() -> process($data);

            Orm::get_db() -> beginTransaction();
            try {
                foreach ( $data as $key => $value ) {
                    $contact = $this -> getByKeyAndLang($key, $lang);
                    if ( $contact ) {
                        $contact -> value = $value;
                        $contact -> use_id_column('key');
                        $contact -> update_where('lang');
                    }
                    else {
                        if ( $value ) {
                            $contact = Orm::for_table('contacts') -> create();
                            $contact -> lang = $lang;
                            $contact -> key = $key;
                            $contact -> value = $value;
                            $contact -> save();
                        }
                    }
                }
                $result -> setSuccessStatus();
                Orm::get_db() -> commit();
                App::GetInstance() -> getCache() -> clean(\Zend_Cache::CLEANING_MODE_ALL);
            }
            catch( \Exception $e ) {
                Orm::get_db() -> rollBack();
                $result -> setErrorStatus();
                $result -> setMessage('common_error', 'Error saving data to the database');
            }
        }
        else {
            $result -> setErrorStatus();
            $result -> setMessages($validator -> getMessages());
        }
        return $result;
    } // edit()

} // Contacts
