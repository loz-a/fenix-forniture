<?php
namespace Models;

use \Core\Orm,
    \Core\App\App,
    \Core\Model\Result,
    \Core\Helper\Collection\DataCleaner,
    \Core\Helper\Collection\Translit,
    \Core\Helper\Collection\FileTransfer,
    \Core\Helper\Collection\Imagick\ImageResizer;

class Products
{
    public function getById($id, $hydrateAsArray = true)
    {
        $lang = App::GetInstance() -> getTranslate() -> getLocale();
        $id = (int) $id;

        $product = Orm::for_table('products')
            -> table_alias('p')
            -> select('p.*')
            -> select('t.title')
            -> select('t.size')
            -> select('t.material')
            -> select('t.price')
            -> select('t.description')
            -> left_outer_join('products_translation', "p.id=t.id AND t.lang='$lang'", 't')
            -> where_equal('p.id', $id)
            -> find_one();

        if ( $product ) {
            if ( $hydrateAsArray ) {
                return $product -> as_array();
            }
            return $product;
        }
        return null;
    } // getById()


    public function getByAlias($alias)
    {
        $lang = App::GetInstance() -> getTranslate() -> getLocale();

        $clean = new DataCleaner();
        $alias = $clean($alias);

        $product = Orm::for_table('products')
            -> table_alias('p')
            -> select('p.*')
            -> select('t.title')
            -> select('t.size')
            -> select('t.material')
            -> select('t.price')
            -> select('t.description')
            -> left_outer_join('products_translation', "p.id=t.id AND t.lang='$lang'", 't')
            -> where_equal('p.alias', $alias)
            -> find_one();

        if ( $product ) {
            return $product -> as_array();
        }
        return null;
    } // getByAlias()


    public function fetchAllByCatalogId($catalogId)
    {
        $lang = App::GetInstance() -> getTranslate() -> getLocale();

        return Orm::for_table('products')
            -> table_alias('p')
            -> select('p.*')
            -> select('t.title')
            -> select('t.size')
            -> select('t.material')
            -> select('t.price')
            -> select('t.description')
            -> left_outer_join('products_translation', "p.id=t.id AND t.lang='$lang'", 't')
            -> where_equal('catid', $catalogId)
            -> find_many();
    } // fetchAll()


    public function fetchRandom($limit = null)
    {
        $lang = App::GetInstance() -> getTranslate() -> getLocale();
        $select = Orm::for_table('products')
            -> table_alias('p')
            -> select('p.img')
            -> select('t.title')
            -> select('t.size')
            -> select('t.material')
            -> select('t.price')
            -> select('t.description')
            -> left_outer_join('products_translation', "p.id=t.id AND t.lang='$lang'", 't')
            -> order_by_rand();

        if ($limit and is_int($limit)) {
            $select -> limit($limit);
        }
        return $select -> find_many();
    } // fetchRandom()


    public function create(array $data, $returnItem = false)
    {
        $lang = App::GetInstance() -> getTranslate() -> getLocale();
        $result    = new Result();
        $validator = new \Validators\Product();

        if ( $validator -> isValid($data) ) {
            $data = DataCleaner::Factory() -> process($data);

            Orm::get_db() -> beginTransaction();
            try {
                $product = Orm::for_table('products') -> create();
                $product -> alias = Translit::Factory() -> process($data['title']);
                $product -> catid = $data['catalogId'];
                $product -> save();

                $prodId = Orm::get_db() -> lastInsertId();

                $tProduct = Orm::for_table('products_translation') -> create();
                $tProduct -> id    = $prodId;
                $tProduct -> lang  = $lang;
                $tProduct -> title = $data['title'];
                $tProduct -> size  = $data['size'];
                $tProduct -> price = $data['price'];
                $tProduct -> material = $data['material'];
                $tProduct -> description = $data['description'];
                $tProduct -> save();

                $filename = $this -> _receiveImage('image', $prodId);
                if ( $filename ) {
                    $product -> img = $filename;
                    $product -> save();
                }

                $result -> setSuccessStatus();
                if ( $returnItem ) {
                    $result -> populate($this -> getById($prodId));
                }

                Orm::get_db() -> commit();
                $this -> _clearCache();
            }
            catch( \Exception $e ) {
                Orm::get_db() -> rollBack();
                $result -> setErrorStatus();
                $result -> setMessage('common_error', 'Error saving data to the database');
            }
        }
        else {
            $result -> setErrorStatus();
            $result -> setMessages($validator -> getMessages());
        }
        return $result;

    } // create()


    public function edit(array $data, $returnItem = false)
    {
        $lang = App::GetInstance() -> getTranslate() -> getLocale();
        $result    = new Result();
        $validator = new \Validators\Product();

        if ( $validator -> isValid($data) ) {
            $data = DataCleaner::Factory() -> process($data);

            Orm::get_db() -> beginTransaction();
            try {
                $tProduct = Orm::for_table('products_translation')
                    -> where_equal('id', $data['id'])
                    -> where_equal('lang', $lang)
                    -> find_one();

                if ( $tProduct ) {
                    $tProduct -> title = $data['title'];
                    $tProduct -> size  = $data['size'];
                    $tProduct -> price = $data['price'];
                    $tProduct -> material = $data['material'];
                    $tProduct -> description = $data['description'];
                    $tProduct -> update_where('lang');
                }
                else {
                    $tProduct = Orm::for_table('products_translation') -> create();
                    $tProduct -> id    = $data['id'];
                    $tProduct -> lang  = $lang;
                    $tProduct -> title = $data['title'];
                    $tProduct -> size  = $data['size'];
                    $tProduct -> price = $data['price'];
                    $tProduct -> material = $data['material'];
                    $tProduct -> description = $data['description'];
                    $tProduct -> save();
                }
                $this -> _receiveImage('image', $data['id']);

                $result -> setSuccessStatus();
                if ( $returnItem ) {
                    $result -> populate($this -> getById($data['id']));
                }

                Orm::get_db() -> commit();
                $this -> _clearCache();
            }
            catch(\Exception $e) {
                Orm::get_db() -> rollBack();
                $result -> setErrorStatus();
                $result -> setMessage('common_error', 'Error saving data to the database');
            }
        }
        else {
            $result -> setErrorStatus();
            $result -> setMessages($validator -> getMessages());
        }
        return $result;
    } // edit()


    public function remove($id)
    {
        $result = new Result();
        $product = $this -> getById($id, false);

        if ( !$product ) {
            $result -> setErrorStatus();
            $result -> setMessage('common_error', 'Error removing data from the database');
        }
        $imageConfig = $this -> _getImageConfig();

        $image = sprintf('%s/%s', $imageConfig['upload_filepath'], $product -> img);
        if ( file_exists($image) ) {
            unlink($image);
        }

        $thumb = sprintf('%s/%s/%s', $imageConfig['upload_filepath'], $imageConfig['thumb']['folder'], $product -> img);
        if ( file_exists($thumb) ) {
            unlink($thumb);
        }
        $result -> populate($product -> as_array());
        $product -> delete();

        $this -> _clearCache();
        return $result;
    } // remove()


    protected function _receiveImage($file, $filename)
    {
        $isFile = is_uploaded_file($_FILES[$file]['tmp_name']);
        if ( !$isFile ) {
            return;
        }
        $config = $this -> _getImageConfig();

        $fileTransfer = new FileTransfer();
        $imageResizer = new ImageResizer();

        $dest = $config['upload_filepath'];
        $filename = $fileTransfer -> process($file, $dest, $filename);

        $resizeOptions = array(
            'strategy' => 'Fit',
            'height'   => $config['height'],
            'width'    => $config['width']
        );
        $filename = $imageResizer -> process($filename, $resizeOptions);

        $thumbOptions = array(
            'strategy' => 'CropCenter',
            'height'   => $config['thumb']['height'],
            'width'    => $config['thumb']['width'],
            'quality'  => $config['thumb']['quality'],
            'outputDir' => $config['upload_filepath'] . '/' . $config['thumb']['folder']
        );
        $imageResizer -> process($filename, $thumbOptions);

        $result = explode('/', $filename);
        return array_pop($result);
    } // _receiveImage()


    protected function _getImageConfig()
    {
        $configFile = App::GetInstance() -> getConfig('upload_config_file');
        $config = \Core\Config::Factory($configFile) -> toArray();
        return $config['products'];
    } // _getImageConfig()


    protected function _clearCache()
    {
        App::GetInstance() -> getCache() -> clean(\Zend_Cache::CLEANING_MODE_ALL);
    } // _clearCache()

} // Products
