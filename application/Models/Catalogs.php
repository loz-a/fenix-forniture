<?php
namespace Models;

use \Core\App\App,
    \Core\Orm,
    \Core\Model\Result,
    \Core\Helper\Collection\DataCleaner,
    \Core\Helper\Collection\Translit,
    \Core\Helper\Collection\FileTransfer,
    \Core\Helper\Collection\Imagick\ImageResizer;

class Catalogs
{
    public function getById($id, $hydrateAsArray = true)
    {
        $lang = App::GetInstance() -> getTranslate() -> getLocale();
        $id = (int) $id;

        $catalog = Orm::for_table('catalogs')
            -> table_alias('c')
            -> select('c.*')
            -> select('t.lang')
            -> select('t.title')
            -> left_outer_join('catalogs_translation', "c.id=t.id AND t.lang='$lang'", 't')
            -> where_equal('c.id', $id)
            -> find_one();

        if ( $catalog ) {
            if ( $hydrateAsArray ) {
                return $catalog -> as_array();
            }
            return $catalog;
        }
        return null;
    } // getById()


    public function getByAlias($alias)
    {
        $lang = App::GetInstance() -> getTranslate() -> getLocale();

        $clean = new DataCleaner();
        $alias = $clean($alias);

        $catalog = Orm::for_table('catalogs')
            -> table_alias('c')
            -> select('c.*')
            -> select('t.lang')
            -> select('t.title')
            -> left_outer_join('catalogs_translation', "c.id=t.id AND t.lang='$lang'", 't')
            -> where_equal('c.alias', $alias)
            -> find_one();

        if ( $catalog ) {
            return $catalog -> as_array();
        }
        return null;
    } // getById()


    public function fetchAll()
    {
        $lang = App::GetInstance() -> getTranslate() -> getLocale();

        return Orm::for_table('catalogs')
            -> table_alias('c')
            -> select('c.*')
            -> select('t.lang')
            -> select('t.title')
            -> left_outer_join('catalogs_translation', "c.id=t.id AND t.lang='$lang'", 't')
            -> find_many();
    } // fetchAll()


    public function create(array $data, $returnItem = false)
    {
        $lang = App::GetInstance() -> getTranslate() -> getLocale();
        $result    = new Result();
        $validator = new \Validators\Catalog();

        if ( $validator -> isValid($data) ) {
            $data = DataCleaner::Factory() -> process($data);

            Orm::get_db() -> beginTransaction();
            try {
                $catalog = Orm::for_table('catalogs') -> create();
                $catalog -> alias = Translit::Factory() -> process($data['title']);
                $catalog -> save();

                $catalogId = Orm::get_db() -> lastInsertId();

                $tCatalog = Orm::for_table('catalogs_translation') -> create();
                $tCatalog -> id = $catalogId;
                $tCatalog -> lang = $lang;
                $tCatalog -> title = $data['title'];
                $tCatalog -> save();

                $filename = $this -> _receiveImage('image', $catalogId);
                if ( $filename ) {
                    $catalog -> img = $filename;
                    $catalog -> save();
                }

                $result -> setSuccessStatus();
                if ( $returnItem ) {
                    $result -> populate($this -> getById($catalogId));
                }
                Orm::get_db() -> commit();
                $this -> _clearCache();
            }
            catch( \Exception $e ) {
                Orm::get_db() -> rollBack();
                $result -> setErrorStatus();
                $result -> setMessage('common_error', 'Error saving data to the database');
            }
        }
        else {
            $result -> setErrorStatus();
            $result -> setMessages($validator -> getMessages());
        }
        return $result;
    } // create()


    public function edit(array $data, $returnItem = false)
    {
        $lang = App::GetInstance() -> getTranslate() -> getLocale();
        $result    = new Result();
        $validator = new \Validators\Catalog();

        if ( $validator -> isValid($data) ) {
            $catalogId = null;
            $data = DataCleaner::Factory() -> process($data);

            Orm::get_db() -> beginTransaction();
            try {
                $tCatalog = Orm::for_table('catalogs_translation')
                    -> where_equal('id', $data['id'])
                    -> where_equal('lang', $lang)
                    -> find_one();

                if ($tCatalog) {
                    $tCatalog -> title = $data['title'];
                    $catalogId = $tCatalog -> id;
                    $tCatalog -> update_where('lang');
                }
                else {
                    $tCatalog = Orm::for_table('catalogs_translation') -> create();
                    $tCatalog -> id    = $data['id'];
                    $tCatalog -> lang  = $lang;
                    $tCatalog -> title = $data['title'];
                    $tCatalog -> save();
                }
                $filename = $this -> _receiveImage('image', $data['id']);

                $result -> setSuccessStatus();
                if ( $returnItem ) {
                    $result -> populate($this -> getById($catalogId));
                }
                Orm::get_db() -> commit();
                $this -> _clearCache();
            }
            catch(\Exception $e) {
                Orm::get_db() -> rollBack();
                $result -> setErrorStatus();
                $result -> setMessage('common_error', 'Error saving data to the database');
            }
        }
        else {
            $result -> setErrorStatus();
            $result -> setMessages($validator -> getMessages());
        }
        return $result;
    } // edit()


    public function remove($catid)
    {
        $result = new Result();
        $catalog = $this -> getById($catid, false);

        if ( !$catalog ) {
            $result -> setErrorStatus();
            $result -> setMessage('common_error', 'Error removing data from the database');
        }
        $imageConfig = $this -> _getImageConfig();

        if ( $catalog -> img ) {
            $image = sprintf('%s/%s', $imageConfig['upload_filepath'], $catalog -> img);
            if ( file_exists($image) ) {
                unlink($image);
            }
        }
        $result -> populate($catalog -> as_array());
        $catalog -> delete();

        $this -> _clearCache();
        return $result;
    } // remove()


    public function isCatalogEmpty($catid)
    {
        $catid = (int) $catid;
        $result = (bool) Orm::for_table('products')
            -> where_equal('catid', $catid)
            -> count();
        return !$result;
    } // isCatalogEmpty()


    protected function _receiveImage($file, $filename)
    {
        $isFile = is_uploaded_file($_FILES[$file]['tmp_name']);
        if ( !$isFile ) {
            return;
        }
        $config = $this -> _getImageConfig();

        $fileTransfer = new FileTransfer();
        $imageResizer = new ImageResizer();

        $dest = $config['upload_filepath'];
        $filename = $fileTransfer -> process($file, $dest, $filename);

        $resizeOptions = array(
            'strategy' => 'CropCenter',
            'height'   => $config['height'],
            'width'    => $config['width']
        );
        $filename = $imageResizer -> process($filename, $resizeOptions);

        $result = explode('/', $filename);
        return array_pop($result);
    } // _receiveImage()


    protected function _getImageConfig()
    {
        $configFile = App::GetInstance() -> getConfig('upload_config_file');
        $config = \Core\Config::Factory($configFile) -> toArray();
        return $config['catalogs'];
    } // _getImageConfig()


    protected function _clearCache()
    {
        App::GetInstance() -> getCache() -> clean(\Zend_Cache::CLEANING_MODE_ALL);
    } // _clearCache()

} // Catalogs
