<?php
namespace Models;

class Users implements \Core\Auth\IModel
{
    const MIN_PASSWORD_LENGTH = 8;

    /**
     * @var int $_unsuccessfulLoginCount
     */
    protected $_unsuccessfulLoginCount = 3;


    public function editPassword(array $data)
    {
        $result    = new \Core\Model\Result();
        $validator = new \Validators\Users\EditPassword();

        if ( $validator -> isValid($data) ) {
            $clean = new \Core\Helper\Collection\DataCleaner();

            $login = $clean($data['login']);
            $paswd = $clean($data['password']);

            $paswd = \Core\Auth\Crypter::Crypt($paswd);

            $user = \Core\Orm::for_table('users')
                -> where_equal('login', $login)
                -> find_one();

            if ( !$user ) {
                $result -> setErrorStatus();
            }
            else {
                $user -> password = $paswd;
                if ( $user -> save() ) {
                    $result -> setSuccessStatus();
                }
                else {
                    $result -> setErrorStatus();
                }
            }
        }
        else {
            $result -> setErrorStatus();
            $result -> setMessages($validator -> getMessages());
        }

        if ( $result -> isError() ) {
            $result -> setMessage('common_error', 'An error occurred while trying to change password');
        }
        return $result;
    } // editPassword()


    public function login($login, $passwd)
    {
        $flashMessenger = \Core\App\App::GetInstance() -> helper -> flashMessenger();

        $user = \Core\Orm::for_table('users')
            -> where_equal('login', $login)
            -> find_one();

        if (! $user) {
            $flashMessenger -> addError('Authorization error. Wrong password or login');
            return false;
        }

        $now = time();

        $dt = new \DateTime($user -> last_failure_timeout);
        $failureTimeout = $dt -> getTimestamp();
        if (
            $user -> last_failure_count > $this -> _unsuccessfulLoginCount
            and $failureTimeout > $now
        ) {
            $flashMessenger -> addError('Authorization error. Please, expect the next 15 seconds to login');
            $user -> last_failure_timeout = date('Y-m-d H:i:s', $now + 15);
            $user -> save();
            return false;
        }

        $result = $this -> authenticate($login, $passwd);
        if ( $result -> isSuccess() ) {
            $flashMessenger -> addSuccess('You are successfully logged');

            $user -> last_failure_count = 0;
            $user -> save();
        }
        else {
            $flashMessenger -> addError('Authorization error. Wrong password or login');
            $user -> last_failure_count += 1;
            $user -> last_failure_timeout = date('Y-m-d H:i:s', $now + 15);
            $user -> save();
        }
        return $result;
    } // login()


   /**
    * @param string $login
    * @param string $paswd
    * @return \Core\Auth\Result
    */
    public function authenticate($login, $paswd)
    {
        $flashMessenger = \Core\App\App::GetInstance() -> helper -> flashMessenger();
        $adapter = self::AuthAdapterFactory($login, $paswd);

        return \Core\Auth\Auth::GetInstance() -> authenticate($adapter);
    } // authenticate()


    /**
     * @static
     * @param string $login
     * @param string $paswd
     * @return \Core\Auth\Adapter\DbTable
     */
    public static function AuthAdapterFactory($login, $paswd)
    {
        $adapter = new \Core\Auth\Adapter\DbTable();
        $adapter
            -> setTableName('users')
            -> setIdentityColumn('login')
            -> setCredentialColumn('password')
            -> setIdentity($login)
            -> setCredential($paswd);
        return $adapter;
    } // AuthAdapterFactory()

} // \Models\Users
