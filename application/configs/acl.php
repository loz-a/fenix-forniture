<?php
return
    array(
        'admin' => array(
            'allow' => array('all')
        ),
        'guest' => array(
            'allow' => array(
                'index' => array('index', 'notImplemented'),
                'error' => array('error404', 'accessDenied'),
                'users' => array('login', 'notImplemented'),
                'catalogs' => array(
                    'index',
                    'notImplemented',
                    'view'
                ),
                'products' => array(
                    'index',
                    'notImplemented',
                    'view'
                ),
                'contacts' => array('index', 'notImplemented')
            )
        )
    );