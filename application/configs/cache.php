<?php
return
    array(
        'index' => array('index'),
        'error' => array('error404', 'accessDenied'),
        'catalogs' => array('index', 'view'),
        'products' => array('view'),
        'contacts' => array('index' )
    );