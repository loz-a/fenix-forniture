<?php
/* #Site settings
============================================================*/
$app -> setConfig('site_name', 'Fenix-Forniture');

/* #View settings
============================================================*/
$app -> setConfig('view_class', '\Core\View\Twig');
$app -> setConfig('twig_autoloader_class', 'Twig/Autoloader');
$app -> setConfig('twig_templates_path', APPLICATION_PATH . '/templates');
$app -> setConfig('twig_compilation_cache_path', APPLICATION_PATH . '/../data/templates/cache');

if ( APPLICATION_ENV === 'development' ) {
    $app -> setConfig('twig_auto_reload', true);
    $app -> setConfig('twig_debug', true);
}
else if ( APPLICATION_ENV === 'production' ) {
    $app -> setConfig('twig_auto_reload', false);
    $app -> setConfig('twig_debug', false);
}

/* #Db settings
============================================================*/
$app -> setConfig('db_location', 'localhost');

if (APPLICATION_ENV === 'development') {
	$app -> setConfig('db_name', 'fenix_forniture');
    $app -> setConfig('db_user', 'root');
    $app -> setConfig('db_password', 'AM23naRmysql');
    $app -> setConfig('db_logging', true);
}
else if (APPLICATION_ENV === 'production') {
	$app -> setConfig('db_name', 'fenixmeb_fenixmebli');
    $app -> setConfig('db_user', 'fenixmeb_admin');
    $app -> setConfig('db_password', 'ftXDGW$7N@;&');
    $app -> setConfig('db_logging', false);
}

/* #Cache
============================================================*/
$app -> setConfig('cache_enable', true);
$app -> setConfig('cache_config_file', APPLICATION_PATH . '/configs/cache.php');
$app -> setConfig('cache_lifetime', 300);
$app -> setConfig('cache_dir', APPLICATION_PATH . '/../data/cache');

/* #Languages and translate settings
============================================================*/
$app -> setConfig('locale_availables', array('uk', 'ru'));
$app -> setConfig('locale_default', 'uk');
$app -> setConfig('locale_file', APPLICATION_PATH . '/../data/translate');

/* #Acl
============================================================*/
$app -> setConfig('acl_config_file', APPLICATION_PATH . '/configs/acl.php');
$app -> setConfig('acl_default_role', 'guest');

/* #Uploads
============================================================*/
$app -> setConfig('upload_config_file', APPLICATION_PATH . '/configs/upload.php');
