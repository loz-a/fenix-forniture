<?php
return
    array(
        'catalogs' =>  array(
            'upload_filepath' => APPLICATION_PATH . '/../media/images/catalogs',
            'filepath' => '/media/images/catalogs',
            'height'   => 205,
            'width'    => 280
        ),
        'products' => array(
            'upload_filepath' => APPLICATION_PATH . '/../media/images/products',
            'filepath' => '/media/images/products',
            'height' => 0,
            'width'  => 400,
            'thumb'  => array(
                'folder' => 'thumb',
                'quality' => 60,
                'height' => 145,
                'width'  => 195,
            )
        )
    );