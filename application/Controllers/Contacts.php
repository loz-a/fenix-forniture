<?php
namespace Controllers;

use \Core\Validate\Collection\Csrf;

class Contacts extends \Core\Controller\AController
{
    /**
     * breadcrumbs
     * @var array $_bc
     */
    protected $_bc = array();

    public function init()
    {
        $this -> _bc = array('Home' => '');
    } // init()

    public function indexAction()
    {
        array_push($this -> _bc, 'Contacts');
        $contacts = $this -> _getModel() -> fetchAll();

        $this -> view -> assign('bc', $this -> _bc);
        $this -> view -> assign('contacts', $contacts);
    } // index


    public function editAction()
    {
        $this -> _bc['Contacts'] = '/contacts';
        $this -> _bc[] = 'Create';

        $csrf = Csrf::Factory(md5(__FILE__ . 'edit'));

        if ( $this -> getRequest() -> isPost() ) {
            $data = $this -> getRequest() -> getParams();
            $flashMessenger = $this -> _app -> helper -> flashMessenger();

            if ( !$csrf -> isValid($data['token']) ) {
                self::Redirect('/error/error404');
            }
            $result = $this -> _getModel() -> edit($data);

            if ( $result -> isSuccess() ) {
                $flashMessenger -> addSuccess('Contacts successfuly edited');
                self::Redirect('/contacts');
            }
            else {
                $this -> view -> assign('errors', $result -> getMessages());
                $this -> view -> assign('data', $data);
            }
        }
        else {
            $data = $this -> _getModel() -> fetchAll();
            $this -> view -> assign('data', $data);
        }
        $this -> view -> assign('bc', $this -> _bc);
        $this -> view -> assign('csrfToken', $csrf -> getToken());
    } // edit


    protected function _getModel()
    {
        return new \Models\Contacts();
    } // _getModel()

} // Contacts
