<?php
namespace Controllers;

use \Core\Validate\Collection\Csrf,
    \Core\Validate\Collection\File\Size;

class Products extends \Core\Controller\AController
{
    protected $title = 'Product';

    /**
     * breadcrumbs
     * @var array $_bc
     */
    protected $_bc = array();

    /**
     * @var array $_imageConfig
     */
    protected $_uploadConfig = array();

    public function init()
    {
        $this -> _bc = array('Home' => '', 'Catalogs' => 'Catalogs');

        $configFile = \Core\App\App::GetInstance() -> getConfig('upload_config_file');
        $this -> _uploadConfig = \Core\Config::Factory($configFile) -> toArray();
    } // init()


    public function indexAction()
    {
        self::Redirect('/');
    } // index


    public function createAction()
    {
        $csfr = Csrf::Factory(md5(__FILE__ . 'add'));
        $catalogModel = new \Models\Catalogs();

        if ( $this -> getRequest() -> isPost() ) {
            $params = $this -> getRequest() -> getParams();
            $flashMessenger = $this -> _app -> helper -> flashMessenger();

            if ( !$csfr -> isValid($params['token']) ) {
                self::Redirect('/error/error404');
            }
            $result = $this -> _getModel() -> create($params);
            $catalog = $catalogModel -> getById($params['catalogId']);

            if ( $result -> isSuccess() ) {
                $flashMessenger -> addSuccess('Product successfuly added');
                self::Redirect('/catalogs/view/catalog/' . $catalog['alias']);
            }
            else {
                $flashMessenger -> addError($result -> getMessage('common_error'));
                $this -> view -> assign('errors', $result -> getMessages());
                $this -> view -> assign('data', $params);
            }
        }
        else {
            $catalogId = $this -> getRequest() -> getParam('catid');
            if ( !$catalogId ) {
                self::Redirect('/error/error404');
            }
            $catalog = $catalogModel -> getById($catalogId);
        }
        $this -> _bc[$catalog['title']] = '/catalogs/view/catalog/' . $catalog['alias'];
        $this -> _bc[] = 'Add';

        $this -> view -> assign('bc', $this -> _bc);
        $this -> view -> assign('catalogId', $catalog['id']);
        $this -> view -> assign('csrfToken', $csfr -> getToken());
        $this -> view -> assign('maxFileSize', Size::FromByteString('2Mb'));
    } // create()


    public function editAction()
    {
        $csrf = Csrf::Factory(md5(__FILE__ . 'edit'));
        $catalogModel = new \Models\Catalogs();

        if ( $this -> getRequest() -> isPost() ) {
            $params = $this -> getRequest() -> getParams();
            $flashMessenger = $this -> _app -> helper -> flashMessenger();

            if ( !$csrf -> isValid($params['token']) ) {
                self::Redirect('/errors/error404');
            }
            $result = $this -> _getModel() -> edit($params);
            $catalog = $catalogModel -> getById($params['catalogId']);

            if ( $result -> isSuccess() ) {
                $flashMessenger -> addSuccess('Product successfuly edited');
                self::Redirect('/catalogs/view/catalog/' . $catalog['alias']);
            }
            else {
                $product = $this -> _getModel() -> getById(intval($params['id']));
                $params['img'] = $product['img'];

                $flashMessenger -> addError($result -> getMessage('common_error'));
                $this -> view -> assign('errors', $result -> getMessages());
                $this -> view -> assign('data', $params);
            }
        }
        else {
            $id = (int) $this -> getRequest() -> getParam('id');
            if ( !$id ) {
                self::Redirect('/error/error404');
            }
            $product = $this -> _getModel() -> getById($id);
            $catalog = $catalogModel -> getById($product['catid']);

            $this -> view -> assign('data', $product);
        }
        $this -> _bc[$catalog['title']] = '/catalogs/view/catalog/' . $catalog['alias'];
        $this -> _bc[$product['title']] = '/products/view/prod/' . $product['alias'];
        $this -> _bc[] = 'Edit';

        $this -> view -> assign('bc', $this -> _bc);
        $this -> view -> assign('catalogId', $catalog['id']);
        $this -> view -> assign('csrfToken', $csrf -> getToken());
        $this -> view -> assign('maxFileSize', Size::FromByteString('2Mb'));
        $this -> view -> assign('imageConfig', $this -> _uploadConfig['products']);
    } // edit()


    public function removeAction()
    {
        $id = $this -> getRequest() -> getParam('id');
        if ( !$id ) {
            self::Redirect('/error/error404');
        }
        $product = $this -> _getModel() -> remove($id);

        $catalogModel = new \Models\Catalogs();
        $catalog = $catalogModel -> getById($product -> catid);

        $this -> _app -> helper -> flashMessenger() -> addSuccess('Product successfuly removed');
        self::Redirect('/catalogs/view/catalog/' . $catalog['alias']);
    } // remove()


    public function viewAction()
    {
        $alias = $this -> getRequest() -> getParam('prod');

        $product = $this -> _getModel() -> getByAlias($alias);
        if ( !$product ) {
            self::Redirect('/error/error404');
        }
        $catalogModel = new \Models\Catalogs();
        $catalog = $catalogModel -> getById($product['catid']);

        $this -> _bc[$catalog['title']] = '/catalogs/view/catalog/' . $catalog['alias'];
        $this -> _bc[] = $product['title'];

        $this -> view -> assign('bc', $this -> _bc);
        $this -> view -> assign('product', $product);
        $this -> view -> assign('imageConfig', $this -> _uploadConfig['products']);
    } // view()


    protected function _getModel()
    {
        return new \Models\Products();
    } // _getModel()

} // \Controllers\Products
