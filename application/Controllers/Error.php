<?php

namespace Controllers;

class Error extends \Core\Controller\AController
{

    public function indexAction()
    {
        $this -> _app -> setAutorenderTemplate(false);
        $this -> error404Action();
        $this -> view -> render('error404');
    } // index


    public function error404Action()
    {
        $this -> metaTitle = 'Error 404';
        $this -> _app -> helper -> ResponseHeader(404);
    } // error404


    public function accessDeniedAction()
    {
        $this -> metaTitle = 'Access Denied';
        $this -> _app -> helper -> ResponseHeader(403);
    } // accessDenied

} // Index
