<?php

namespace Controllers;

class Index extends \Core\Controller\AController
{
    /**
     * @var array $_imageConfig
     */
    protected $_uploadConfig = array();

    public function init()
    {
        $configFile = \Core\App\App::GetInstance() -> getConfig('upload_config_file');
        $this -> _uploadConfig = \Core\Config::Factory($configFile) -> toArray();
    } // init()


	public function indexAction()
	{
		$this -> metaTitle = 'Home';

        $prodModel = new \Models\Products();
        $products = $prodModel -> fetchRandom(20);

        $this -> view -> assign('product', current($products));
        $this -> view -> assign('products', $products);
        $this -> view -> assign('uploadConfig', $this -> _uploadConfig['products']);
	} // index();

} // Index
