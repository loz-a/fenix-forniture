<?php
namespace Controllers;

use \Core\Validate\Collection\Csrf;

class Users extends \Core\Controller\AController
{
    /**
     * breadcrumbs
     * @var array $_bc
     */
    protected $_bc = array();

    public function init()
    {
        $this -> _bc = array('Home' => '/');
    } // init()


    public function indexAction()
    {
        $this -> loginAction();
        $this -> _app -> helper -> responseHeader('301');
        $this -> view -> render('login');
    } // index


    public function loginAction()
    {
        array_push($this -> _bc, 'Login');
        $csrf = Csrf::Factory(md5(__FILE__ . 'login'));

        if ( $this -> getRequest() -> isPost() ) {

            $params = $this -> getRequest() -> getParams();
            if ( !$csrf -> isValid($params['token']) ) {
                self::Redirect('/error/error404');
            }

            $login = $params['login'];
            $paswd = $params['passwd'];

            $result = $this -> _getModel() -> login($login, $paswd);
            if ( $result and $result -> isSuccess() ) {
                self::Redirect('/index');
            }

            sleep(1); // small bruteforce shield
            $this -> view -> assign('data', $params);
        }
        $this -> view -> assign('csrfToken', $csrf -> getToken());
        $this -> view -> assign('bc', $this -> _bc);
    } // login


    public function logoutAction()
    {
        $auth = \Core\Auth\Auth::GetInstance();

        if ( $auth -> hasIdentity() ) {
            $auth -> clearIdentity();
            //$this -> _app -> helper -> flashMessenger() -> addNotice('Logout successfull');
        }
        self::Redirect('/index');
    } // logout


    public function editPasswordAction()
    {
        array_push($this -> _bc, 'Edit password');
        $csrf = Csrf::Factory(md5(__FILE__ . 'edit_password'));

        if ( $this -> getRequest() -> isPost() ) {

            $params = $this -> getRequest() -> getParams();
            if ( !$csrf -> isValid($params['token']) ) {
                self::Redirect('/error/error404');
            }

            $model = $this -> _getModel();
            $flashMessenger = $this -> _app -> helper -> flashMessenger();

            $result = $model -> editPassword($params);
            if ( $result -> isSuccess() ) {
                 $flashMessenger -> addSuccess('Password was changed successfully');
                self::Redirect('/index');
            }
            else {
                $flashMessenger -> addError($result -> getMessage('common_error'));
                $this -> view -> assign('errors', $result -> getMessages());
            }
        }

        $this -> view -> assign('login', \Core\Auth\Auth::GetInstance() -> getIdentity() -> login);
        $this -> view -> assign('csrfToken', $csrf -> getToken());
        $this -> view -> assign('bc', $this -> _bc);
    } // editPassword()


    /**
     * @return \Models\Users
     */
    protected function _getModel()
    {
        return new \Models\Users();
    } // _getModel()

} // \Controllers\Admin
