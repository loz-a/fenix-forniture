<?php
namespace Controllers;

use \Core\Validate\Collection\Csrf,
    \Core\Validate\Collection\File\Size;

class Catalogs extends \Core\Controller\AController
{
    protected $title = 'Catalog';

    /**
     * breadcrumbs
     * @var array $_bc
     */
    protected $_bc = array();

    /**
     * @var array $_imageConfig
     */
    protected $_uploadConfig = array();


    public function init()
    {
        $this -> _bc = array('Home' => '', 'Catalogs' => 'Catalogs');

        $configFile = \Core\App\App::GetInstance() -> getConfig('upload_config_file');
        $this -> _uploadConfig = \Core\Config::Factory($configFile) -> toArray();
    } // init()


    public function indexAction()
    {
        $catalogs = $this -> _getModel() -> fetchAll();
        $this -> view -> assign('catalogs', $catalogs);
        $this -> view -> assign('imageConfig', $this -> _uploadConfig['catalogs']);
        $this -> view -> assign('bc', $this -> _bc);
    } // index()


    public function createAction()
    {
        array_push($this -> _bc, 'Create');
        $csrf = Csrf::Factory(md5(__FILE__ . 'create'));

        if ( $this -> getRequest() -> isPost() ) {

            $params = $this -> getRequest() -> getParams();
            if ( !$csrf -> isValid($params['token']) ) {
                self::Redirect('/error/error404');
            }

            $result = $this -> _getModel() -> create($params);
            $flashMessenger = $this -> _app -> helper -> flashMessenger();

            if ( $result -> isSuccess() ) {
                $flashMessenger -> addSuccess('Catalog successfully created');
                self::Redirect('/catalogs');
            }
            else {
                $flashMessenger -> addError($result -> getMessage('common_error'));
                $this -> view -> assign('errors', $result -> getMessages());
                $this -> view -> assign('data', $params);
            }
        }
        $this -> view -> assign('bc', $this -> _bc);
        $this -> view -> assign('csrfToken', $csrf -> getToken());
        $this -> view -> assign('maxFileSize', Size::FromByteString('2Mb'));
    } // create()


    public function editAction()
    {
        if ( $this -> getRequest() -> isPost() ) {

            if ( !$this -> _getCsrf('_edit') -> isValid($_POST['token'])) {
                self::Redirect('/error/error404');
            }

            $data = $this -> getRequest() -> getParams();
            $result = $this -> _getModel() -> edit($data);
            if ($result -> isSuccess()) {
                self::Redirect('/catalogs/view/catalog/' . $data['alias']);
                $flashMessenger -> addSuccess('Catalog successfully edited');
            }
            else {
                $this -> view -> assign('errors', $result -> getMessages());
            }
        }
        else {
            $id = $this -> getRequest() -> getParam('catid');
            if ( !$id ) {
                self::Redirect('/error/eror404');
            }
            $data = $this -> _getModel() -> getById($id);
        }
        $this -> _bc[$data['title']] = '/catalogs/view/catalog/' . $data['alias'];
        $this -> _bc[] = 'Edit';

        $this -> view -> assign('bc', $this -> _bc);
        $this -> view -> assign('data', $data);
        $this -> view -> assign('imageConfig', $this -> _uploadConfig['catalogs']);
        $this -> view -> assign('csrfToken', $this -> _getCsrf('_edit') -> getToken());
        $this -> view -> assign('maxFileSize', Size::FromByteString('2Mb'));
    } // edit()


    public function removeAction()
    {
        $catid = $this -> getRequest() -> getParam('catid');
        $flashMessenger = $this -> _app -> helper -> flashMessenger();

        if ( !$catid ) {
            self::Redirect('/error/error404');
        }

        $catModel = $this -> _getModel();
        if ( $catModel -> isCatalogEmpty($catid) ) {
            $catModel -> remove($catid);
            $flashMessenger -> addSuccess('Catalog successfuly removed');
        }
        else {
            $flashMessenger -> addNotice('Directory not empty. First remove the content of this directory');
            $temp = explode('/', $_SERVER['HTTP_REFERER']);
            self::Redirect('/catalogs/view/catalog/' . $temp[count($temp) - 1]);
        }
        self::Redirect('/catalogs');
    } // remove()


    public function viewAction()
    {
        $alias = $this -> getRequest() -> getParam('catalog');

        $catalog = $this -> _getModel() -> getByAlias($alias);
        if ( !$catalog ) {
            self::Redirect('/error/error404');
        }
        $productModel = new \Models\Products();
        $products = $productModel -> fetchAllByCatalogId($catalog['id']);

        array_push($this -> _bc, $catalog['title']);
        $this -> view -> assign('bc', $this -> _bc);
        $this -> view -> assign('catalog', $alias);
        $this -> view -> assign('catalogid', $catalog['id']);
        $this -> view -> assign('products', $products);
        $this -> view -> assign('imageConfig', $this -> _uploadConfig['products']);
    } // viewAction()


    /**
     * @return \Models\Catalogs
     */
    protected function _getModel()
    {
        return new \Models\Catalogs();
    } // _getModel()


    /**
     * @param $hash
     *
     * @return \Core\Validate\Collection\Csrf
     */
    protected function _getCsrf($hash)
    {
        return new \Core\Validate\Collection\Csrf(md5(__FILE__ . $hash));
    } // _getCsrfToken()

} // Catalogs
