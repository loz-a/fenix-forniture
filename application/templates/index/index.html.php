{% extends 'layout.html.php' %}

{% block content %}
    <div class="content-wrapper row">

        <aside class="six columns round-border-box">
            <p class="aside-p">
                {{ trans('We can offer a wide range of wooden products: kennels, summerhouses, gazebos, swing') }}
            </p>
            <p class="aside-p">
                {{ trans('For over five years we have successfully been manufacturing and selling various products of good quality wood') }}
            </p>
            <p class="aside-p">
                {{ trans('We guarantee high quality, reliability and durability of our products') }}
            </p>
        </aside><!-- asisd -->

        <div id="content" class="ten columns row index">
            {% if product %}
            <div class="view-image">
                <p class="image-wrapper">
                    <img src="{{ uploadConfig.filepath }}/{{ product.img|e }}"
                         alt="{{ product.title|e }}"
                         title="{{ product.title|e }}"
                         width="300"
                         height="400" />
                </p>

                <div class="image-caption">
                    <div class="image-caption-inner-wrapper">
                        <span id="view-counter" class="counter">1/{{ products|length }}</span>
                        <span id="view-image-title">{{ product.title|e }}</span><br>
                        <span class="info">{{ trans('Material') }}: <span id="view-image-material">{{ product.material|e }}</span>.</span>
                    </div>
                    <div class="transparent"></div>
                </div>
            </div>
            {% endif %}
        </div><!-- #conent -->

        {% if products|length %}
        <div class="gallery-slider sixteen columns">
            <div class="wrapper">
                <ul class="move">
                {% for prod in products %}
                    <li class="gal-item">
                        <a class="gal-link" href="{{ uploadConfig.filepath }}/{{ prod.img|e }}">
                            <img
                                class="gal-image"
                                src="{{ uploadConfig.filepath }}/{{ uploadConfig.thumb.folder }}/{{ prod.img|e }}"
                                alt="{{ prod.title|e }}. Material: {{ prod.material }}"
                                title="{{ prod.title|e }}"
                                height="100"
                                width="140">
                        </a>
                    </li>
                {% endfor %}
                </ul>
            </div>

            <div class="left hide-text">Left</div>
            <div class="right hide-text">Right</div>
        </div>
        {% endif %}

    </div><!-- #content -->
{% endblock content %}

{% block scripts %}
    {{ parent() }}

    <script src="{{ baseUrl('resources/scripts/gallery.js') }}"></script>
{% endblock scripts %}