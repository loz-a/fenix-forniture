{% extends 'layout.html.php' %}

{% block title %}
    {{ trans('Catalog') }}
{% endblock title %}

{% block content %}
<div class="content-wrapper row">
    <div id="content" class="view-catalog">

    {% if products|length > 0 %}
        <div class="products-list">
        {% for product in products %}
            {% if loop.index0 is even %}
                <div class="row">
            {% endif %}

            <div class="eight columns product round-border-box">
                <p class="image-wrapper">
                    <img
                        src="{{ imageConfig.filepath }}/{{ imageConfig.thumb.folder }}/{{ product.img }}"
                        alt="{{ product.title }}"
                        width="{{ imageConfig.thumb.width }}"
                        height="{{ imageConfig.thumb.height }}">
                </p>
                <dl class="product-attrs">
                    <dt>{{ trans('Size') }}:</dt><dd>{{ product.size }};</dd>
                    <dt>{{ trans('Material') }}:</dt><dd>{{ product.material }};</dd>
                    <dt>{{ trans('Price') }}:</dt><dd>{{ product.price }};</dd>
                </dl>
                <h1>
                    <a href="{{ baseUrl('/products/view/prod') }}/{{ product.alias }}">
                        {{ product.title ? product.title : trans('No translate') }}
                    </a>
                </h1>
            </div>

            {% if loop.index0 is odd %}
                </div><!--.row -->
            {% endif %}
        {% endfor %}
        </div><!--.products-list-->
    {% endif %}

    </div><!-- #content -->
</div><!-- .content-wrapper -->
{% endblock content %}

{% if isUserHasRole('admin') %}
    {% block adminMenuItems %}
    <li class="am-item">
        <a class="am-link hide-text am-edit-cat-icon"
           href="{{ baseUrl('catalogs/edit') }}/catid/{{ catalogid }}">
            {{ trans('Edit catalog') }}
        </a>
    </li>
    <li class="am-item">
        <a class="am-link hide-text am-rem-cat-icon"
           href="{{ baseUrl('catalogs/remove') }}/catid/{{ catalogid }}">
            {{ trans('Remove catalog') }}
        </a>
    </li>
    <li class="am-item">
        <a class="am-link hide-text am-add-prod-icon"
           href="{{ baseUrl('products/create') }}/catid/{{ catalogid }}">
            {{ trans('Create product') }}
        </a>
    </li>
    {% endblock %}
{% endif %}

{% block scripts %}
{{ parent() }}
<script>
    snack.wrap('#admin-menu a[href*="/remove/catid/"]')
        .each(function(item) {
            if (item.nodeType && item.nodeType == 1) {
                var params = {node: item, event: 'click'};

                snack.listener(params, function(evt) {
                    if (!confirm('{{ trans('Are you sure you want to delete?') }}')) {
                        evt.preventDefault();
                    }
                });
            }
        });


</script>
{% endblock scripts %}