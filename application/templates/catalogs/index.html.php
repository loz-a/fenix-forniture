{% extends 'layout.html.php' %}

{% block title %}
    {{ trans('Catalog') }}
{% endblock title %}

{% block content %}
<div class="content-wrapper row">
    <div id="cotent" class="catalog">

    {% if catalogs|length > 0 %}
        <ul class="catalog-list">
        {% for catalog in catalogs %}
            <li class="catalog-item five columns">

            {% if catalog.img %}
                <img
                    class="catalog-img"
                    src="{% if catalog.img %} {{ imageConfig.filepath }}/{{ catalog.img }} {% endif %}"
                    alt="{{ catalog.title|e }}"
                    width="{{ imageConfig.width }}"
                    height="{{ imageConfig.height }}">
                <br>
            {% endif %}

                <a class="catalog-link" href="{{ baseUrl('/catalogs/view/catalog') }}/{{ catalog.alias }}">
                    {% if catalog.title %} {{ catalog.title|e }} {% else %} {{ trans('No name') }} {% endif %}
                </a>
            </li>
        {% endfor %}
        </ul>
    {% endif %}

    </div><!-- .catalog -->

</div><!-- .content-wrapper -->
{% endblock content %}

{% if isUserHasRole('admin') %}
    {% block adminMenuItems %}
        <li class="am-item">
            <a class="am-link hide-text am-add-cat-icon" href="{{ baseUrl('catalogs/create') }}">
                {{ trans('Create catalog') }}
            </a>
        </li>
    {% endblock %}
{% endif %}