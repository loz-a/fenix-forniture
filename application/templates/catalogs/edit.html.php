{% extends 'layout.html.php' %}

{% block title %}
    {{ trans('Edit') }}
{% endblock title %}

{% block content %}

    <div class="content-wrapper row clearfix">
        <div id="content" class="catalog-edit offset-by-two row">

            <div class="form-wrapper round-border-box twelve columns">
                <p class="three columns aside-image">
                    <img
                        {% if data.img %}
                            src="{{ baseUrl(imageConfig.filepath) }}/{{ data.img }}"
                        {% else %}
                            src="{{ baseUrl('/resources/images/default-image.png') }}"
                        {% endif %}
                        alt=""
                        height="{{ imageConfig.height / 2 }}"
                        width="{{ imageConfig.width / 2 }}">
                </p>

                <form
                        class="eight columns"
                        action=""
                        method="POST"
                        enctype="multipart/form-data">

                    <input type="hidden" name="token" value="{{ csrfToken }}">
                    <input type="hidden" name="id" value="{{ data.id|e }}">
                    <input type="hidden" name="alias" value="{{ data.alias|e }}">
                    <input type="hidden" name="MAX_FILE_SIZE" value="{{ maxFileSize }}">

                    <dl>
                        <dt>
                            <label for="product-title">
                                {{ trans('Title') }}:
                            </label>
                        </dt>
                        <dd>
                            <input
                                    id="product-title"
                                    class="tree columns"
                                    name="title"
                                    value="{{ data.title|e }}"
                                    type="text"
                                    maxlength="50">
                            {{ errors(errors.title) }}
                        </dd>
                    </dl>

                    <div class="upload-wrapper">
                        <div class="text-wrapper">
                            {{ data.img }}
                        </div>
                        <div class="input-wrapper">
                            <input
                                    id="product-image"
                                    class="upload-input"
                                    name="image"
                                    type="file"
                                    size="31">
                        </div>
                        {{ errors(errors.image) }}
                    </div>

                    <p class="submit-form">
                        <input value="Ok" type="submit">
                    </p>
                </form>
            </div><!-- .form-wrapper -->

        </div><!-- #content -->
    </div><!-- .content-wrapper -->

{% endblock content %}

{% if isUserHasRole('admin') %}
    {% block adminMenuItems %}
    <li class="am-item">
        <a class="am-link hide-text am-plus-icon" href="{{ baseUrl('catalogs/create') }}">
            {{ trans('Create catalog') }}
        </a>
    </li>
    {% endblock %}
{% endif %}