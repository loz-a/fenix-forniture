{% extends 'layout.html.php' %}

{% block title %}
{{ trans('Edit Password') }}
{% endblock title %}

{% block content %}

<div class="content-wrapper row clearfix">
    <div id="content" class="catalog-create offset-by-four row">

        <form
            class="eight columns round-border-box"
            action=""
            method="POST"
            enctype="application/x-www-form-urlencoded">

            <input name="token" type="hidden" value="{{ csrfToken }}">
            <input name="login" type="hidden" value="{{ login }}">

            <dl>
                <dt>
                    <label for="current-passwd">
                        {{ trans('Cur. pass.') }}:
                    </label>
                </dt>
                <dd>
                    <input
                        id="current-passwd"
                        class="tree columns"
                        name="current_password"
                        placeholder="{{ trans('Current password') }}"
                        value=""
                        type="password">
                    {{ errors(errors.current_password) }}
                </dd>
                <dt>
                    <label for="new-passwd">
                        {{ trans('New pass.') }}:
                    </label>
                </dt>
                <dd>
                    <input
                        id="new-passwd"
                        class="tree columns"
                        name="password"
                        placeholder="{{ trans('New password') }}"
                        value=""
                        type="password">
                    {{ errors(errors.password) }}
                </dd>

                <dt>
                    <label for="confirm-passwd">
                        {{ trans('Confirm') }}:
                    </label>
                </dt>
                <dd>
                    <input
                            id="confirm-passwd"
                            class="tree columns"
                            name="confirm_password"
                            placeholder="{{ trans('Confirm password') }}"
                            value=""
                            type="password">
                    {{ errors(errors.confirm_password) }}
                </dd>
            </dl>
            <p class="submit-form">
                <input value="Ok" type="submit">
            </p>
        </form>

    </div><!-- #content -->
</div><!-- .content-wrapper -->

{% endblock content %}
