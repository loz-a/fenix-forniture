{% extends 'layout.html.php' %}

{% block title %}
{{ trans('Login') }}
{% endblock title %}

{% block content %}



<div class="content-wrapper row clearfix">
    <div id="content" class="catalog-create offset-by-four row">

        <form
            class="eight columns round-border-box"
            action=""
            method="POST"
            enctype="application/x-www-form-urlencoded">

            <input name="token" type="hidden" value="{{ csrfToken }}">

            <dl>
                <dt>
                    <label for="product-login">
                        {{ trans('Login') }}:
                    </label>
                </dt>
                <dd>
                    <input
                        id="product-login"
                        class="tree columns"
                        name="login"
                        value="{{ data.login|e }}"
                        type="text"
                        size="50">
                </dd>
                <dt>
                    <label for="product-passwd">
                        {{ trans('Password') }}:
                    </label>
                </dt>
                <dd>
                    <input
                        id="product-passwd"
                        class="tree columns"
                        name="passwd"
                        value=""
                        type="password">
                </dd>
            </dl>
            <p class="submit-form">
                <input value="Ok" type="submit">
            </p>
        </form>

    </div><!-- #content -->
</div><!-- .content-wrapper -->

{% endblock content %}
