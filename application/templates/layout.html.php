<!DOCTYPE html>
<!--[if lt IE 7 ]><html class="ie ie6" lang="en"> <![endif]-->
<!--[if IE 7 ]><html class="ie ie7" lang="en"> <![endif]-->
<!--[if IE 8 ]><html class="ie ie8" lang="en"> <![endif]-->
<!--[if (gte IE 9)|!(IE)]><!--><html lang="en"> <!--<![endif]-->
<head>
    <!-- Basic Page Needs
   ================================================== -->
    <meta charset="utf-8">
    <title>
        {% block title %} {{ metaTitle|e }} {% endblock %} - {{ siteName|e }}
    </title>

    {% block metas %}
        <meta name="description" content="{{ metaDescription|e }}">
        <!--<meta name="author" content="">-->

        <!-- Mobile Specific Metas -->
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    {% endblock metas %}

    <!-- CSS
   ================================================== -->
    {% block stylesheets %}
        <link rel="stylesheet" href="{{ baseUrl('resources/css/base.css') }}">
        <link rel="stylesheet" href="{{ baseUrl('resources/css/skeleton.css') }}">
        <link rel="stylesheet" href="{{ baseUrl('resources/css/layout.css') }}">
    {% endblock stylesheets %}

    {% block headscrips %}
        <!--[if lt IE 9]
             <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
         <![endif]-->

        <script src="{{ baseUrl('resources/scripts/modernizr-2.5.3.js') }}"></script>
    {% endblock headscrips %}

    <!-- Favicons
     ================================================== -->
    {% block links %}
    <link rel="shortcut icon" href="{{ baseUrl('resources/images/favicon.ico') }}">
    <link rel="apple-touch-icon" href="{{ baseUrl('resources/images/apple-touch-icon.png') }}">
    <link rel="apple-touch-icon" sizes="72x72" href="{{ baseUrl('resources/images/apple-touch-icon-72x72.png') }}">
    <link rel="apple-touch-icon" sizes="114x114" href="{{ baseUrl('resources/images/apple-touch-icon-114x114.png') }}">

    <!-- Authors
    ================================================== -->
    <link rel="author" href="{{ baseUrl('humans.txt') }}" />
    {% endblock links %}
</head>

<body>
    <div id="page" class="container">

        <header class="sixteen columns row">

            <div id="langs">
                <ul class="langs-item-list">
                    <li class="lang-item">
                        <a class="lang-link li-uk-icon hide-text"
                           href="{{ requestUri('/lang/uk') }}">
                            uk
                        </a>
                    </li>
                    <li class="lang-item">
                        <a class="lang-link li-ru-icon hide-text"
                           href="{{ requestUri('/lang/ru') }}">
                            ru
                        </a>
                    </li>
                </ul>
            </div><!-- #langs -->

            <nav class="row offset-by-five">
                <ul class="navigation">
                    <li class="nav-item two columns">
                        <a class="nav-link mi-home-icon" href="{{ baseUrl('') }}">
                            {{ trans('Home') }}
                        </a>
                    </li>
                    <li class="nav-item two columns">
                        <a class="nav-link mi-catalog-icon" href="{{ baseUrl('catalogs') }}">
                            {{ trans('Catalog') }}
                        </a>
                    </li>
                    <li class="nav-item two columns">
                        <a class="nav-link mi-contacts-icon" href="{{ baseUrl('contacts') }}">
                            {{ trans('Contacts') }}
                        </a>
                    </li>
                </ul>
            </nav>

            <div class="logo-wrapper row offset-by-three">
                <a class="logo logo-icon-{{ locale() }} hide-text" href="{{ baseUrl() }}">
                    {{ trans('Fenix Forniture') }}
                </a>
            </div>

            {{ breadcrumbs(bc) }}


        </header><!-- header -->

        {{ successMessages() }}
        {{ infoMessages() }}
        {{ errorMessages() }}

        {% block content %}{% endblock %}

        <footer class="sixteen columns">
            <nav>
                <ul class="navigation">
                    <li class="nav-item">
                        <a class="nav-link" href="{{ baseUrl() }}">
                            {{ trans('Home') }}
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="{{ baseUrl('catalogs') }}">
                            {{ trans('Catalog') }}
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="{{ baseUrl('contacts') }}">
                            {{ trans('Contacts') }}
                        </a>
                    </li>
                </ul>
                <div class="transparent"></div>
            </nav>

            <div class="copyright-wrapper">
                <div class="copyright">
                    &copy; 2012. {{ trans('All rights reserved') }}
                </div>
                <div class="transparent"></div>
            </div>

        </footer><!-- footer -->

        <div class="altanka"></div>
    </div><!-- #page -->

{% if isUserHasRole('admin') %}
    <div id="admin-menu">
        <ul class="am">
            {% block adminMenuItems %}{% endblock %}
            <li class="am-item">
                <a class="am-link hide-text am-key-icon" href="{{ baseUrl('users/edit-password') }}">
                    {{ trans('Edit password') }}
                </a>
            </li>
            <li class="am-item">
                <a class="am-link hide-text am-exit-icon" href="{{ baseUrl('users/logout') }}">
                    {{ trans('Logout') }}
                </a>
            </li>
        </ul>
    </div><!-- #admin-menu -->
{% endif %}


    {% block scripts %}
        <!-- JS
       ================================================== -->
    <script src="{{ baseUrl('resources/scripts/snack-slick.js') }}"></script>
    <script src="{{ baseUrl('resources/scripts/script.js') }}"></script>
    {% endblock scripts %}
</body>
</html>