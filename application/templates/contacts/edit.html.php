{% extends 'layout.html.php' %}

{% block title %}
    {{ trans('Contacts') }}
{% endblock title %}

{% block content %}

<div class="content-wrapper row clearfix">
    <div id="content" class="contacts-edit offset-by-two row">

        <div class="form-wrapper round-border-box twelve columns offset-by-two">
            <form
                class="eight columns"
                action=""
                method="POST"
                enctype="application/x-www-form-urlencoded">

                <input type="hidden" name="token" value="{{ csrfToken }}">

                <dl>
                    <dt>
                        <label for="contacts-address">
                            {{ trans('Address') }}:
                        </label>
                    </dt>
                    <dd>
                        <input
                            id="contacts-address"
                            class="tree columns"
                            name="address"
                            value="{{ data.address|e }}"
                            type="text"
                            maxlength="255"
                            placeholder="{{ trans('No translate') }}">
                        {{ errors(errors.address) }}
                    </dd>

                    <dt>
                        <label for="contacts-fax">
                            {{ trans('Fax') }}:
                        </label>
                    </dt>
                    <dd>
                        <input
                            id="contacts-fax"
                            class="tree columns"
                            name="fax"
                            value="{{ data.fax|e }}"
                            type="text"
                            maxlength="255"
                            placeholder="{{ trans('No translate') }}">
                        {{ errors(errors.fax) }}
                    </dd>

                    <dt>
                        <label for="contacts-phone">
                            {{ trans('Phone') }}:
                        </label>
                    </dt>
                    <dd>
                        <input
                            id="contacts-phone"
                            class="tree columns"
                            name="phone"
                            value="{{ data.phone|e }}"
                            type="text"
                            maxlength="255"
                            placeholder="{{ trans('No translate') }}">
                        {{ errors(errors.phone) }}
                    </dd>

                    <dt>
                        <label for="contacts-email">
                            Email:
                        </label>
                    </dt>
                    <dd>
                        <input
                            id="contacts-email"
                            class="tree columns"
                            name="email"
                            value="{{ data.email|e }}"
                            type="text"
                            maxlength="255"
                            placeholder="{{ trans('No translate') }}">
                        {{ errors(errors.email) }}
                    </dd>
                </dl>

                <p class="submit-form">
                    <input value="Ok" type="submit">
                </p>
            </form>
        </div><!-- .form-wrapper -->

    </div><!-- #content -->
</div><!-- .content-wrapper -->
{% endblock content %}