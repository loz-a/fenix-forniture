{% extends 'layout.html.php' %}

{% block title %}
    {{ trans('Contacts') }}
{% endblock title %}

{% block content %}
<div class="content-wrapper row">

    <aside class="six columns round-border-box">
        <p class="aside-p">
            {{ trans('We can offer a wide range of wooden products: kennels, summerhouses, gazebos, swing') }}
        </p>
        <p class="aside-p">
            {{ trans('For over five years we have successfully been manufacturing and selling various products of good quality wood') }}
        </p>
        <p class="aside-p">
            {{ trans('We guarantee high quality, reliability and durability of our products') }}
        </p>
    </aside><!-- asisd -->

    <div id="content" class="ten columns row round-border-box contacts">
        <h1>
            {{ trans('Our contacts') }}
        </h1>

        <dl class="contacts">
            <dt>{{ trans('Address') }}:</dt>
            <dd>{{ contacts.address|e }}</dd>

            <dt>{{ trans('Fax') }}:</dt>
            <dd>{{ contacts.fax|e }}</dd>

            <dt>{{ trans('Phone') }}.:</dt>
            <dd>{{ contacts.phone|e }}</dd>

            <dt>Email:</dt>
            <dd>{{ contacts.email|e }}</dd>
        </dl>

    </div><!-- #conent -->

</div><!-- #content -->

{% endblock content %}



{% if isUserHasRole('admin') %}
    {% block adminMenuItems %}
    <li class="am-item">
        <a class="am-link hide-text am-edit-prod-icon" href="{{ baseUrl('contacts/edit') }}">
            {{ trans('Edit contacts') }}
        </a>
    </li>
    {% endblock %}
{% endif %}