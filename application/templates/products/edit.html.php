{% extends 'layout.html.php' %}

{% block title %}
    {{ trans('Edit') }}
{% endblock title %}

{% block content %}

<div class="content-wrapper row clearfix">
    <div id="content" class="product-edit offset-by-two row">

        <div class="form-wrapper round-border-box twelve columns">
            <p class="three columns aside-image">
                <img
                    src="{{ imageConfig.filepath }}/{{ imageConfig.thumb.folder }}/{{ data.img }}"
                    alt="{{ product.title }}"
                    width="{{ imageConfig.thumb.width }}"
                    height="{{ imageConfig.thumb.height }}">
            </p>

            <form
                class="eight columns"
                action=""
                method="POST"
                enctype="multipart/form-data">

                <input type="hidden" name="token" value="{{ csrfToken }}">
                <input type="hidden" name="MAX_FILE_SIZE" value="{{ maxFileSize }}">
                <input type="hidden" name="catalogId" value="{{ catalogId }}">
                <input type="hidden" name="id" value="{{ data.id|e }}">

                <dl>
                    <dt>
                        <label for="product-title">
                            {{ trans('Title') }}:
                        </label>
                    </dt>
                    <dd>
                        <input
                            id="product-title"
                            class="tree columns"
                            name="title"
                            value="{{ data.title|e }}"
                            type="text"
                            maxlength="50"
                            placeholder="{{ trans('No translate') }}">
                        {{ errors(errors.title) }}
                    </dd>

                    <dt>
                        <label for="product-size">
                            {{ trans('Size') }}:
                        </label>
                    </dt>
                    <dd>
                        <input
                            id="product-size"
                            class="tree columns"
                            name="size"
                            value="{{ data.size|e }}"
                            type="text"
                            maxlength="25"
                            placeholder="{{ trans('No translate') }}">
                        {{ errors(errors.size) }}
                    </dd>

                    <dt>
                        <label for="product-material">
                            {{ trans('Material') }}:
                        </label>
                    </dt>
                    <dd>
                        <input
                            id="product-material"
                            class="tree columns"
                            name="material"
                            value="{{ data.material|e }}"
                            type="text"
                            maxlength="50"
                            placeholder="{{ trans('No translate') }}">
                        {{ errors(errors.material) }}
                    </dd>

                    <dt>
                        <label for="product-price">
                            {{ trans('Price') }}:
                        </label>
                    </dt>
                    <dd>
                        <input
                            id="product-price"
                            class="tree columns"
                            name="price"
                            value="{{ data.price|e }}"
                            type="text"
                            maxlength="50"
                            placeholder="{{ trans('No translate') }}">
                        {{ errors(errors.price) }}
                    </dd>

                    <dt>
                        <label for="product-description">
                            {{ trans('Description') }}:
                        </label>
                    </dt>
                    <dd>
                        <textarea
                            id="product-description"
                            class="five columns"
                            name="description"
                            rows="5"
                            placeholder="{{ trans('No translate') }}"
                        >{{ data.description|e }}</textarea>
                        {{ errors(errors.description) }}
                    </dd>

                </dl>

                <div class="upload-wrapper">
                    <div class="text-wrapper">
                        {{ data.img|e }}
                    </div>
                    <div class="input-wrapper">
                        <input
                            id="product-image"
                            class="upload-input"
                            name="image"
                            type="file"
                            size="31">
                    </div>
                    {{ errors(errors.image) }}
                </div>

                <p class="submit-form">
                    <input value="Ok" type="submit">
                </p>
            </form>
        </div><!-- .form-wrapper -->

    </div><!-- #content -->
</div><!-- .content-wrapper -->

{% endblock content %}