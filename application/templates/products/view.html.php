{% extends 'layout.html.php' %}

{% block title %}
    {{ product.title }}
{% endblock title %}

{% block content %}

<div class="content-wrapper row">
    <div id="content" class="product">

        <div class="sixteen columns round-border-box">
            <p class="image-wrapper seven columns">
                <img
                    src="{{ imageConfig.filepath }}/{{ product.img }}"
                    alt="{{ product.title }}"
                    width="{{ imageConfig.width }}">
            </p>

            <div class="product-attribs eight columns">
                <h1>
                    {{ product.title }}
                </h1>
                <dl class="clearfix">
                    <dt>{{ trans('Size') }}:</dt><dd>{{ product.size }};</dd>
                    <dt>{{ trans('Material') }}:</dt><dd>{{ product.material }};</dd>
                    <dt>{{ trans('Price') }}:</dt><dd>{{ product.price }};</dd>
                </dl>
                <p class="info">
                    {{ product.description }}
                </p>
            </div>
        </div>

    </div><!-- #conent -->
</div><!-- #content -->
{% endblock content %}

{% if isUserHasRole('admin') %}
    {% block adminMenuItems %}
    <li class="am-item">
        <a class="am-link hide-text am-edit-prod-icon" href="{{ baseUrl('products/edit') }}/id/{{ product.id }}">
            {{ trans('Edit product') }}
        </a>
    </li>
    <li class="am-item">
        <a class="am-link hide-text am-rem-prod-icon" href="{{ baseUrl('products/remove') }}/id/{{ product.id }}">
            {{ trans('Remove product') }}
        </a>
    </li>
    {% endblock %}
{% endif %}

{% block scripts %}
    {{ parent() }}
<script>
    snack.wrap('#admin-menu a[href*="/remove/id/"]')
        .each(function(item) {
            if (item.nodeType && item.nodeType == 1) {
                var params = {node: item, event: 'click'};

                snack.listener(params, function(evt) {
                    if (!confirm('{{ trans('Are you sure you want to delete?') }}')) {
                        evt.preventDefault();
                    }
                });
            }
        });


</script>
{% endblock scripts %}