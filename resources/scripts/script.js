/*
  jquery version

$(document).ready(function() {
    initNavSelect();
});

function initNavSelect() {
    var $nav = $('nav', 'header');
    var $select = $('<select />').appendTo($nav);

    $('<option />', {
        class: 'nav-first-select',
        selected: 'selected',
        value: '',
        text: 'Перейти до...'
    }).appendTo($select);

    $('a', $nav).each(function() {
        var $el = $(this);
        $('<option />', {
            class: 'nav-select',
            value: $el.attr('href'),
            text:  $el.text()
        }).appendTo($select);

    });

    $($select).change(function() {
        window.location = $(this).find("option:selected").val();
    });

}  // initNavSelect()
*/

/**
 * snakejs version
 */

var util = (function(snack) {

    var util = {
        navSelect: {
            init: function() {
                var navNode = snack.wrap('header nav')[0];

                var selNode = document.createElement('select');
                var firstItem = document.createElement('option');

                firstItem.className = 'nav-first-select';
                firstItem.setAttribute('selected', 'selected');
                firstItem.value = '';
                firstItem.innerHTML = 'Перейти до...';

                var linksList = snack.wrap('header nav a');

                snack.each(linksList, function(node) {
                    var optionNode = document.createElement('option');
                    optionNode.className = 'nav-select';
                    optionNode.value = node.getAttribute('href');
                    optionNode.innerHTML = node.innerText ? node.innerText : node.textContent;
                    selNode.appendChild(optionNode);
                });

                selNode.appendChild(firstItem);
                navNode.appendChild(selNode);

                snack.listener({
                    node: selNode,
                    event: 'change'
                }, function() {
                    window.location = snack.wrap('header nav option:selected')[0].value;
                }).attach();
            } // init()
        }
    };
    return util;
})(snack);


util.navSelect.init();

snack.wrap('#content .upload-input')
    .each(function(item) {
        if (item.nodeType && item.nodeType == 1) {
            var params = {node: item, event: 'change'};

            snack.listener(params, function() {
                var elem = this.parentNode.previousSibling;

                while (elem) {
                    if (elem.className == 'text-wrapper') {
                        elem.textContent ? elem.textContent = this.value : elem.innerText = this.value;
                        break;
                    }
                    else {
                        elem = elem.previousSibling;
                    }
                }
            });
        }
    });
