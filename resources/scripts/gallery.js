var util = (function(snack, Modernizr) {

    var topic = snack.publisher();

    var util = {

        helpers: {
            camelize: function(str) {
                /* camelize("border-bottom-color"); output -> borderBottomColor */
                return (str + "").replace(/-\D/g, function(match) {
                    return match.charAt(1).toUpperCase();
                });
            }, // camelize()

            hyphenate: function (str) {
                /* hyphenate("borderBottomColor"); output -> border-bottom-color */
                return (str + "").replace(/[A-Z]/g, function(match) {
                    return "-" + match.toLowerCase();
                });
            }, // hyphenate()

            getComputedStyle: function(node, prop) {
                if (! node) throw Error('Node is undefined');
                if (! prop) throw Error('Prop is undefined');

                if (window.getComputedStyle) {
                    return window.getComputedStyle(node, null).getPropertyValue(prop);
                }
                else {
                    if (prop == 'float') {
                        prop = 'styleFloat';
                    }
                    else {
                        prop = this.camelize(prop);
                    }
                    return node.currentStyle[prop];
                }
            }, // getComputedStyle()

            nodeClass: {
                add: function(node, cls) {
                    if(node.classList) {
                        node.classList.add(cls);
                    }
                    else {
                        var nodeClsList = node.className.split(/\s/);
                        nodeClsList.push(cls);
                        node.className = nodeClsList.join(' ');
                    }
                } // add
            } // util.helpers.nodeClass

        }, // util.helpers

        slider: {
            _canMove: true,

            init: function() {
                var prevBtn = snack.wrap('.gallery-slider .left')[0];
                var nextBtn = snack.wrap('.gallery-slider .right')[0];

                prevBtn.style.display = 'none';
                if((document.all) && (navigator.appVersion.indexOf("MSIE 7.") != -1)) {
                    nextBtn.style.display = 'none';
                }

                snack.listener({
                    node: nextBtn,
                    event: 'click'
                }, function(evt) {
                    if (util.slider._canMove) {
                        util.slider._canMove = false;
                        util.slider.moveToNext();
                    }
                }).attach();

                snack.listener({
                    node: prevBtn,
                    event: 'click'
                }, function() {
                    if (util.slider._canMove) {
                        util.slider._canMove = false;
                        util.slider.moveToPrev();
                    }
                }).attach();

                var sliderNode = snack.wrap('.gallery-slider')[0];

                topic.subscribe('transition/end', function() {
                    util.slider._canMove = true;

                    var galleryNode = snack.wrap('.gallery-slider ul')[0];
                    var prevBtn = snack.wrap('.gallery-slider .left')[0];
                    var nextBtn = snack.wrap('.gallery-slider .right')[0];

                    prevBtn.style.display = util.slider._isPrev(galleryNode) ? 'block' : 'none';
                    nextBtn.style.display = util.slider._isNext(galleryNode) ? 'block' : 'none';
                });

                snack.listener({
                    node: sliderNode,
                    event: 'transitionend'
                }, function() {
                    topic.publish('transition/end');
                }).attach();

                snack.listener({
                    node: sliderNode,
                    event: 'webkitTransitionEnd'
                }, function() {
                    topic.publish('transition/end');
                }).attach();

                snack.listener({
                    node: sliderNode,
                    event: 'oTransitionEnd'
                }, function() {
                    topic.publish('transition/end');
                }).attach();

            }, // init()

            moveToPrev: function() {
                var galleryNode = snack.wrap('.gallery-slider ul')[0];
                if (! this._isPrev(galleryNode)) {
                    this._canMove = true;
                    return;
                }

                var galleryItemNode = snack.wrap('.gallery-slider .gal-item')[0];
                var newMargin = this._getNextPosition(galleryItemNode);

                if (Modernizr.csstransitions) {
                    galleryNode.style.marginLeft = newMargin + 'px';
                }
                else {
                    this.animate(galleryNode, newMargin);
                }
            }, // moveToPrev()

            moveToNext: function() {
                var galleryNode = snack.wrap('.gallery-slider ul')[0];
                if (! this._isNext(galleryNode)) {
                    this._canMove = true;
                    return;
                }

                var galleryItemNode = snack.wrap('.gallery-slider .gal-item')[0];
                var newMargin = this._getPrevPosition(galleryItemNode);

                if (Modernizr.csstransitions) {
                    galleryNode.style.marginLeft = newMargin + 'px';
                }
                else {
                    this.animate(galleryNode, newMargin);
                }
            }, // moveToNext()

            _getOuterWidth: function(node) {
                return node.offsetWidth
                    + parseInt(util.helpers.getComputedStyle(node, 'margin-left'))
                    + parseInt(util.helpers.getComputedStyle(node, 'margin-right'));
            }, // _getOuterWidth()

            _getPrevPosition: function(node) {
                var nodeWidth = this._getOuterWidth(node);
                var galleryBox = snack.wrap('.gallery-slider ul')[0];
                return parseInt(util.helpers.getComputedStyle(galleryBox, 'margin-left')) - nodeWidth;
            }, // _getPrevPosition()

            _getNextPosition: function(node) {
                var nodeWidth = this._getOuterWidth(node);
                var galleryBox = snack.wrap('.gallery-slider ul')[0];
                return parseInt(util.helpers.getComputedStyle(galleryBox, 'margin-left')) + nodeWidth;
            }, // _getNextPosition()

            _isNext: function(galleryNode) {
                var galleryMarginLeft = parseInt(util.helpers.getComputedStyle(galleryNode, 'margin-left'));

                var galleryItemsList = snack.wrap('.gallery-slider li');
                var galleryItemWidth = this._getOuterWidth(galleryItemsList[0]);

                var visibleItemsCount = Math.ceil(this._getOuterWidth(galleryNode) / galleryItemWidth);
                var prevInvisibleCount = Math.abs(galleryMarginLeft) / galleryItemWidth;

                return (galleryItemsList.length - (prevInvisibleCount + visibleItemsCount)) > 0;
            }, // _isNext()

            _isPrev: function(galleryNode) {
                var leftMargin = parseInt(util.helpers.getComputedStyle(galleryNode, 'margin-left'));
                var galleryItemList = snack.wrap('.gallery-slider li');
                var itemWidth = this._getOuterWidth(galleryItemList[0]);

                return (Math.abs(leftMargin) / itemWidth) > 0;
            }, // _isPrev()

            animate: function(galleryNode, newMargin) {
                var currentMargin = parseInt(util.helpers.getComputedStyle(galleryNode, 'margin-left'));
                var stepsCount = 20;
                var stepLength = Math.floor((Math.abs(newMargin) - Math.abs(currentMargin)) / stepsCount);

                for (var i = 0; i <= stepsCount; i ++) {
                    (function(pos) {
                        setTimeout(function() {
                            galleryNode.style.marginLeft = (currentMargin - (pos * stepLength)) + 'px';
                            if (pos == stepsCount) {
                                setTimeout(function() {
                                    galleryNode.style.marginLeft = newMargin + 'px';
                                    topic.publish('transition/end');
                                }, 10);
                            }
                        }, (pos + 1) * 10);
                    })(i);
                }
            } // util.slider.animate()

        }, // util.slider

        image: {
            init: function() {
                var self = this;
                var imageViewer = snack.wrap('.view-image img')[0];

                snack.listener({
                    node: imageViewer,
                    event: 'load'
                }, function() {
                    self.removePreloader();
                }).attach();

                var imagesList = snack.wrap('.gallery-slider img');
                imagesList.attach('click', function(evt) {
                        evt.preventDefault ? evt.preventDefault() : evt.returnValue = false;

                        var srcImage = evt.srcElement ? evt.srcElement : this;
                        self.updateImage(srcImage, imageViewer);
                        self.updateCaption(srcImage);
                        self.updateCounter(srcImage, imagesList);
                    });
            }, // init()

            updateImage: function(srcImage, imageViewer) {
                this.createPreloader(imageViewer);
                var srcFilename = srcImage.parentNode.href.split('/').pop();
                var temp = imageViewer.src.split('/');
                temp.pop();
                temp.push(srcFilename);

                imageViewer.src = temp.join('/');
                imageViewer.alt = srcImage.title;
                imageViewer.title = srcImage.title;
            }, // updateImage()

            updateCaption: function(srcImage) {
                if (srcImage.alt) {
                    var searchKeyword = 'Material:';
                    var startPos = srcImage.alt.indexOf(searchKeyword) + searchKeyword.length;
                    document.getElementById('view-image-material').innerHTML = srcImage.alt.substring(startPos);
                }
                document.getElementById('view-image-title').innerHTML = srcImage.title;
            }, // updateCaption()

            updateCounter: function(srcImage, imagesList) {
                var imageIndex = snack.indexOf(srcImage, imagesList) + 1;
                var viewCounterNode = document.getElementById('view-counter');
                var imagesQty = viewCounterNode.innerHTML.split('/').pop();
                viewCounterNode.innerHTML = imageIndex + '/' + imagesQty;
            }, // updateCounter()

            createPreloader: function(imageViewer) {
                var preloader = document.createElement('div');
                preloader.className = 'preloader';

                var imageCaption = snack.wrap('.view-image .image-caption')[0];
                imageViewer.parentNode.parentNode.insertBefore(preloader, imageCaption);
            }, // createPreloader()

            removePreloader: function() {
                snack.wrap('.view-image .preloader')
                    .each(function(item) {
                        if (item.nodeType && item.nodeType == 1) {
                            item.parentNode.removeChild(item);
                        }
                    });
            } // removePreloader()

        } // util.image

    }; // util

    return util;
})(snack, Modernizr);

util.slider.init();
util.image.init();