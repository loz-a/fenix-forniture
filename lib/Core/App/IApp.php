<?php
namespace Core\App;

interface IApp
{
    /**
     * @abstract
     * @return \Core\App\IApp
     */
    public function run();

    /**
     * @abstract
     * @return \Core\App\IApp
     */
    public function serve();

    /**
     * @abstract
     *
     * @param $variable
     */
    public function getConfig($variable);

    /**
     * @abstract
     * @param string $variable
     * @param string $value
     * @return \Core\App\IApp
     */
    public function setConfig($variable, $value);

    /**
     * @abstract
     * @return string
     */
    public function getRootPath();


    /**
     * @abstract
     * @return array
     *
     */
    public function getArgs();

    /**
     * @abstract
     * @return \Core\Controller\IController
     */
    public function getController();

    /**
     * @abstract
     * @return \Core\View\IView
     *
     */
    public function getView();

    /**
     * @abstract
     * @param string $hookName
     * @param array $params
     * @return \Core\App\IApp
     */
    public function registerHook($hookName, array $params = array());

    /**
     * @abstract
     * @param $className
     */
    public function autoload($className);

    /**
     * @abstract
     * @param integer $number
     * @param string $string
     * @param string $file
     * @param integer $line
     */
    public function error($number, $string, $file, $line);

    /**
     * @abstract
     * @return bool
     */
    public function isDispatched();

    /**
     * @abstract
     * @param \Core\Translate $translate
     */
    public function setTranslate(\Core\Translate $translate);

    /**
     * @abstract
     * @return \Core\Translate
     */
    public function getTranslate();
}
