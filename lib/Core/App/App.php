<?php
namespace Core\App;

class App implements \Core\App\IApp
{
    /**
     * @var \Core\App\App $_instance;
     */
    protected static $_instance = null;

    /**
     * @var array $_args
     */
    protected $_args = array();

    /**
     * @var array $_config
     */
    protected $_config = array();

    /**
     * @var \Core\Controller\IController $_controller
     */
    protected $_controller;

    /**
     * @var \Core\Controller\Request\Http $_request
     */
    protected $_request;

    /**
     * @var array $_hooks
     */
    protected $_hooks = array();

    /**
     * @var array $_plugins
     */
    protected $_plugins = array();

    /**
     * @var string $_rootPath
     */
    protected $_rootPath = '/';

     /**
     * @var \Core\View\IView $_view
     */
    protected $_view;

    /**
     * @var bool
     */
    protected $_autorenderTemplate = true;

    /**
     * @var string $_controllerPath
     */
    protected $_controllerPath = '/Controllers';

    /**
     * @var string $_pluginPath
     */
    protected $_pluginPath = '/Plugins';

    /**
     * @var string $_helperPath
     */
    protected $_helperPath = '';

    /**
     * @var string $_modelPath
     */
    protected $_modelPath = '/Models';

    /**
     * @var \Core\Helper\Broker $_helperBroker
     */
    protected $_helperBroker;

    /**
     * @var \Core\Plugin\Broker $_pluginBroker
     */
    protected $_pluginBroker;

    /**
     * @var \Core\Translate $_translate
     */
    protected $_translate;

    protected $_cache;

    /**
     * @var bool $_isDispatched
     */
    protected $_isDispatched = false;


    protected function __construct()
    {} // __construct()


    protected function __clone()
    {} // __clone()


    /**
     * @static
     * @return App|null
     */
    public static function GetInstance()
    {
        if ( null === self::$_instance ) {
            self::$_instance = new self();
        }
        return self::$_instance;
    } // GetInstance()


    /**
     * Run the application
     * @return \Core\App\App
     */
    public function run()
    {
        $this -> _pluginBroker = new \Core\Plugin\Broker($this, $this -> getPluginPath());
        $this -> registerHook('dispatchBefore');

        // Determine the client-side path to root
        if ( !empty($_SERVER['REQUEST_URI']) ) {
            $this -> _rootPath = preg_replace('/(index\.php)?(\?.*)?$/', '', $_SERVER['REQUEST_URI']);

            if ( !empty($_GET['q']) ) {
                $this -> _rootPath = preg_replace('/' . preg_quote($_GET['q'], '/') . '$/', '', $this -> _rootPath);
            }
        }

        // Extract controller name, view name, action name and arguments from URL
        $controllerName = 'Index';
        $actionName = 'index';

        if ( !empty($_GET['q']) ) {
            $this -> _args = explode('/', $_GET['q']);

            if ( $this -> _args ) {
                $controllerName = str_replace(' ', '/', ucwords(str_replace('_', ' ', str_replace('-', '', array_shift($this -> _args)))));
            }

            if ( $this -> _args ) {
                $actionName = array_shift($this -> _args);
                $actionName = str_replace('-', '', $actionName);
            }
        }
        $this -> _request = new \Core\Controller\Request\Http($this -> _args);

        $controllerPath = $this -> getControllerPath();
        $controllerFile = sprintf('%s%s/%s.php', APPLICATION_PATH, $controllerPath, $controllerName);

        if ( !is_file($controllerFile) ) {
            $controllerName = 'Index';
            $actionName = 'Error404';
        }

        if ( $this -> hasConfig('view_class') ) {
            $viewClass = $this -> getConfig('view_class');
            $this -> _view = new $viewClass($this);
        }
        else {
            $this -> _view = new \Core\View\View($this);
        }

        // Instantiate the controller
        $controllerNS = self::Path2ns($controllerPath);
        $controllerClassName = $controllerNS . basename($controllerName);
        $this -> _controller = new $controllerClassName($this, $this -> _view, $controllerName, $actionName);

        $this -> _isDispatched = true;

        $this -> _helperBroker = new \Core\Helper\Broker($this -> getHelperPath());

        // Call the controller action
        $this->registerHook('actionBefore');

        $actionName .= 'Action';
        if ( method_exists($this -> _controller, $actionName) ) {
            $method = new \ReflectionMethod($this -> _controller, $actionName);

            if (
                $method -> isPublic()
                and !$method -> isFinal()
                and !$method -> isConstructor()
            ) {
                $this -> _controller -> {$actionName}();
            } else {
                $this -> _controller -> notImplemented();
            }
        } else {
            $this -> _controller -> notImplemented();
        }
        $this -> registerHook('actionAfter');

        unset($controllerNS, $controllerClassName, $controllerFile, $controllerPath, $actionName);
        return $this;
    } // run()


    /**
     * Serve the page
     * @return \Core\App\App
     */
    public function serve()
    {
        if ( $this -> getAutorenderTemplate() ) {
            $this -> _view -> render();
        }
        return $this;
    } // serve()


    public function setAutorenderTemplate($flag = true)
    {
        $this -> _autorenderTemplate = (bool) $flag;
        return $this;
    } // setAutorenderTemplate()


    public function getAutorenderTemplate()
    {
        return $this -> _autorenderTemplate;
    } // getAutorenderTemplate()


    /**
     * check is configuration value exists
     * @param $variable
     * @return bool
     */
    public function hasConfig($variable)
    {
        return isset($this -> _config[$variable]);
    } // hasConfig()

    /**
     * Get a configuration value
     * @param string $variable
     * @return mixed
     */
    public function getConfig($variable)
    {
        if ( isset($this -> _config[$variable]) ) {
            return $this -> _config[$variable];
        }
        throw new \OutOfBoundsException('Wrong variable name');
    }


    /**
     * Set a configuration value
     * @param string $variable
     * @param mixed $value
     * @return \Core\App\App
     */
    public function setConfig($variable, $value)
    {
        $this -> _config[$variable] = $value;
        return $this;
    } // setConfig()


    /**
     * Get the client-side path to root
     * @return string
     */
    public function getRootPath()
    {
        return $this -> _rootPath;
    } // getRootPath()


    /**
     * Get the arguments
     * @return array
     */
    public function getArgs()
    {
        return $this -> _args;
    } // getArgs()


    /**
     * @return bool
     */
    public function isDispatched()
    {
        return $this -> _isDispatched;
    } // isDispatched()


    public function __get($name)
    {
        if ( $name === 'helper') {
            return $this -> _helperBroker;
        }
    } // __get()


    public function __call($name, $args)
    {
        switch ( $name ) {
            case 'getControllerPath':
                return $this -> _controllerPath;
            case 'getPluginPath':
                return $this -> _pluginPath;
            case 'getHelperPath':
                return $this -> _helperPath;
            case 'getModelPath':
                return $this -> _modelPath;
        }
    } // __call()


    /**
     * @return \Core\Controller\IController
     */
    public function getController()
    {
        return $this -> _controller;
    } // getController()


    public function getRequest()
    {
        return $this -> _request;
    } // getRequest()


    /**
     * @return \Core\View
     */
    public function getView()
    {
        return $this -> _view;
    } // getView()


    /**
     * @param Translate $translate
     * @return \Core\App
     */
    public function setTranslate(\Core\Translate $translate)
    {
        $this -> _translate = $translate;
        return $this;
    } // setTranslate()


    /**
     * @return \Core\Translate
     * @throws \LogicException
     */
    public function getTranslate()
    {
        if ( !$this -> _translate ) {
            throw new \LogicException('Translate is undefined');
        }
        return $this -> _translate;
    } // getTranslate()


    /**
     * @param $cacheInstace
     */
    public function setCache($cacheInstace)
    {
        $this -> _cache = $cacheInstace;
        return $this;
    } // setCache()


    public function getCache()
    {
        if ( !$this -> _cache ) {
            throw new \LogicException('Cache is undefined');
        }
        return $this -> _cache;
    } // getCache()


    /**
     * @param string $hookName
     * @param array $params
     * @return \Core\App\App
     */
    public function registerHook($hookName, array $params = array())
    {
        $this -> _pluginBroker -> registerHook($hookName, $params);
        return $this;
    } // registerHook()


    /**
     * Class autoloader
     * @param $className
     * @see https://github.com/php-fig/fig-standards/blob/master/accepted/PSR-0.md
     */
    public function autoload($className)
    {
        preg_match('/(^.+\\\)?([^\\\]+)$/', ltrim($className, '\\'), $match);

        $file = str_replace('\\', '/', $match[1]) . str_replace('_', '/', $match[2]) . '.php';

        require $file;
    } // autoload()

    /**
     * Error handler
     * @param int $number
     * @param string $string
     * @param string $file
     * @param int $line
     */
    public function error($number, $string, $file, $line)
    {
        throw new \Exception('Error #' . $number . ': ' . $string . ' in ' . $file . ' on line ' . $line);
    } // error()


    /**
     * @param string $path
     * @return string
     */
    public static function Path2ns($path)
    {
        $path = str_replace(array('/', '\\'), DIRECTORY_SEPARATOR, $path);
        $explodedPath = explode(DIRECTORY_SEPARATOR, $path);
        $explodedPath = array_map('ucfirst', $explodedPath);
        return implode('\\', $explodedPath) . '\\';
    } // Path2ns()

} // \Core\App\App
