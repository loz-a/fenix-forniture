<?php
namespace Core\Controller;

abstract class AController implements \Core\Controller\IController
{
    /**
     * @var \Core\App\IApp $_app
     */
    protected $_app;

    /**
     * @var \Core\View\IView $view
     */
    public $view;

    /**
     * page meta title
     * @var string $metaTitle
     */
    public $metaTitle;

    /**
     * page meta description
     * @var string $metaDescription
     */
    public $metaDescription;

    /**
     * @var string $_controllerName
     */
    protected $_controllerName;

    /**
     * @var string $_actionName
     */
    protected $_actionName;

    public function __construct(
        \Core\App\IApp $app,
        \Core\View\IView $view,
        $controllerName,
        $actionName)
    {
        $this -> _app = $app;
        $this -> view = $view;
        $this -> _controllerName = $controllerName;
        $this -> _actionName = $actionName;

        $this -> init();
    } // __construct()


    public function init()
    {
    } // init();


    /**
     * Fallback in case action doesn't exist
     */
    public function notImplemented()
    {
        self::Redirect('/index');
    } // notImplement()


    public function __call($methodName, $args)
    {
        if ( 'Action' == substr($methodName, -6) ) {
            $action = substr($methodName, 0, strlen($methodName) - 6);
            throw new \Exception(sprintf('Action "%s" does not exist and was not trapped in __call()', $action), 404);
        }
        throw new \LogicException(sprintf('Method "%s" does not exist and was not trapped in __call()', $methodName), 500);
    } // __call



    //TODO make protected all methods, which not are actions
    //TODO or add 'Action' in the end of action method names
    /**
     * @return \Core\Controller\Request\Http
     */
    public function getRequest()
    {
        return $this -> _app -> getRequest();
    } // getRequest()


    /**
     * @return string
     */
    public function getControllerName()
    {
        return $this -> _controllerName;
    } // getControllerName()


    /**
     * @return string
     */
    public function getActionName()
    {
        return $this -> _actionName;
    } // getActionName()


    protected function _getModel()
    {
        throw new \Exception('Method not implement');
    } // getModel()


    /**
     * @static
     * @param string $target
     */
    public static function Redirect($target, $statusCode = null)
    {
        $host = $_SERVER['HTTP_HOST'];
        $target = rtrim($target, '/');
        if ( !headers_sent() ) {

            if ( $statusCode ) {
                $rh = new \Core\Helper\Collection\ResponseHeader();
                $rh -> setStatus($statusCode);
            }

            header('Location: http://' . $host . $target);
            exit;
        };
        exit('<meta http-equiv="refresh" content="0; url=http://' . $host . $target. '"/>');
    } // Redirect()

} //  \Core\AController
