<?php
namespace Core\Controller\Request;

class Http implements \Core\Controller\Request\IRequest
{
    /**
     * Scheme for http
     *
     */
    const SCHEME_HTTP  = 'http';

    /**
     * Scheme for https
     *
     */
    const SCHEME_HTTPS = 'https';

    /**
     * @var array $_params
     */
    protected $_params = array();

    /**
     * Raw request body
     * @var string|false
     */
    protected $_rawBody;


    public function __construct(array $argsArray = array())
    {
        $this -> _parseParams($argsArray);
    } // __construct()


    public function getParam($key, $default = null)
    {
        if ( isset($this -> _params[$key]) ) {
            return $this -> _params[$key];
        }
        return $default;
    }  // getParam()


    public function hasParam($key)
    {
        return isset($this -> _params[$key]);
    } // hasParam()


    public function getParams()
    {
        return $this -> _params;
    } // getParams()


    /**
     * Return the raw body of the request, if present
     *
     * @return string|false Raw body, or false if not present
     */
    public function getRawBody()
    {
        if ( null === $this -> _rawBody ) {
            $body = file_get_contents('php://input');

            if ( strlen(trim($body)) > 0 ) {
                $this -> _rawBody = $body;
            } else {
                $this -> _rawBody = false;
            }
        }
        return $this -> _rawBody;
    } // getRawBody()


    /**
     * Retrieve a member of the $_COOKIE superglobal
     *
     * If no $key is passed, returns the entire $_COOKIE array.
     *
     * @param string $key
     * @param mixed $default Default value to use if key not found
     * @return mixed Returns null if key does not exist
     */
    public function getCookie($key = null, $default = null)
    {
        if ( null === $key ) {
            return $_COOKIE;
        }

        return ( isset($_COOKIE[$key]) ) ? $_COOKIE[$key] : $default;
    } // getCookie()

    /**
     * Retrieve a member of the $_SERVER superglobal
     *
     * If no $key is passed, returns the entire $_SERVER array.
     *
     * @param string $key
     * @param mixed $default Default value to use if key not found
     * @return mixed Returns null if key does not exist
     */
    public function getServer($key = null, $default = null)
    {
        if ( null === $key ) {
            return $_SERVER;
        }

        return ( isset($_SERVER[$key]) ) ? $_SERVER[$key] : $default;
    } // getServer()

    /**
     * Retrieve a member of the $_ENV superglobal
     *
     * If no $key is passed, returns the entire $_ENV array.
     *
     * @param string $key
     * @param mixed $default Default value to use if key not found
     * @return mixed Returns null if key does not exist
     */
    public function getEnv($key = null, $default = null)
    {
        if ( null === $key ) {
            return $_ENV;
        }

        return ( isset($_ENV[$key]) ) ? $_ENV[$key] : $default;
    } // getEnv()


    /**
     * Return the method by which the request was made
     *
     * @return string
     */
    public function getMethod()
    {
        return $this -> getServer('REQUEST_METHOD');
    } // getMethod()


    /**
     * Return the value of the given HTTP header. Pass the header name as the
     * plain, HTTP-specified header name. Ex.: Ask for 'Accept' to get the
     * Accept header, 'Accept-Encoding' to get the Accept-Encoding header.
     *
     * @param string $header HTTP header name
     * @return string|false HTTP header value, or false if not found
     * @throws \InvalidArgumentException
     */
    public function getHeader($header)
    {
        if (empty($header)) {
            throw new \InvalidArgumentException('An HTTP header name is required');
        }

        // Try to get it from the $_SERVER array first
        $temp = 'HTTP_' . strtoupper(str_replace('-', '_', $header));
        if ( isset($_SERVER[$temp]) ) {
            return $_SERVER[$temp];
        }

        // This seems to be the only way to get the Authorization header on
        // Apache
        if ( function_exists('apache_request_headers') ) {
            $headers = apache_request_headers();
            if (isset($headers[$header])) {
                return $headers[$header];
            }
            $header = strtolower($header);
            foreach ($headers as $key => $value) {
                if (strtolower($key) == $header) {
                    return $value;
                }
            }
        }
        return false;
    } // getHeader()


    /**
     * Get the request URI scheme
     *
     * @return string
     */
    public function getScheme()
    {
        return ( $this -> getServer('HTTPS') == 'on' ) ? self::SCHEME_HTTPS : self::SCHEME_HTTP;
    } // getScheme()


    /**
     * Get the HTTP host.
     *
     * "Host" ":" host [ ":" port ] ; Section 3.2.2
     * Note the HTTP Host header is not the same as the URI host.
     * It includes the port while the URI host doesn't.
     *
     * @return string
     */
    public function getHttpHost()
    {
        $host = $this -> getServer('HTTP_HOST');
        if ( !empty($host) ) {
            return $host;
        }

        $scheme = $this -> getScheme();
        $name   = $this -> getServer('SERVER_NAME');
        $port   = $this -> getServer('SERVER_PORT');

        if( null === $name ) {
            return '';
        }
        elseif (
            ($scheme == self::SCHEME_HTTP and $port == 80 )
            or ( $scheme == self::SCHEME_HTTPS and $port == 443 )
        ) {
            return $name;
        } else {
            return $name . ':' . $port;
        }
    } // getHttpHost()


    /**
     * Get the client's IP addres
     *
     * @param  boolean $checkProxy
     * @return string
     */
    public function getClientIp($checkProxy = true)
    {
        if ($checkProxy && $this -> getServer('HTTP_CLIENT_IP') != null) {
            $ip = $this -> getServer('HTTP_CLIENT_IP');
        } else if ($checkProxy && $this -> getServer('HTTP_X_FORWARDED_FOR') != null) {
            $ip = $this -> getServer('HTTP_X_FORWARDED_FOR');
        } else {
            $ip = $this -> getServer('REMOTE_ADDR');
        }
        return $ip;
    } // getClientIp()


    /**
     * Was the request made by POST?
     *
     * @return boolean
     */
    public function isPost()
    {
        if ('POST' == $this -> getMethod()) {
            return true;
        }
        return false;
    } // isPost()


    /**
     * Was the request made by GET?
     *
     * @return boolean
     */
    public function isGet()
    {
        if ('GET' == $this -> getMethod()) {
            return true;
        }
        return false;
    } // isGet()


    /**
     * Was the request made by PUT?
     *
     * @return boolean
     */
    public function isPut()
    {
        if ('PUT' == $this -> getMethod()) {
            return true;
        }
        return false;
    } // isPut()


    /**
     * Was the request made by DELETE?
     *
     * @return boolean
     */
    public function isDelete()
    {
        if ('DELETE' == $this -> getMethod()) {
            return true;
        }
        return false;
    } // isDelete()


    /**
     * Was the request made by HEAD?
     *
     * @return boolean
     */
    public function isHead()
    {
        if ('HEAD' == $this -> getMethod()) {
            return true;
        }
        return false;
    } // isHead()


    /**
     * Was the request made by OPTIONS?
     *
     * @return boolean
     */
    public function isOptions()
    {
        if ('OPTIONS' == $this -> getMethod()) {
            return true;
        }
        return false;
    } // isOptions()


    /**
     * Is the request a Javascript XMLHttpRequest?
     *
     * Should work with Prototype/Script.aculo.us, possibly others.
     *
     * @return boolean
     */
    public function isXmlHttpRequest()
    {
        return ( $this -> getHeader('X_REQUESTED_WITH') == 'XMLHttpRequest' );
    } // isXmlHttpRequest()


    /**
     * Is this a Flash request?
     *
     * @return boolean
     */
    public function isFlashRequest()
    {
        $header = strtolower($this -> getHeader('USER_AGENT'));
        return ( strstr($header, ' flash') ) ? true : false;
    } // isFlashRequest()


    /**
     * Is https secure request
     *
     * @return boolean
     */
    public function isSecure()
    {
        return ( $this -> getScheme() === self::SCHEME_HTTPS );
    } // getSecure()


    protected function _parseParams(array $argsArray)
    {
        if ( $this -> isPost() ) {
            $this -> _params = $_POST;
        }
        else if ( $this -> isGet() ) {
            if ( !$argsArray ) {
                $this -> _params = $_GET;
            }
            else {
                while ( $argsArray ) {
                    $key = array_shift($argsArray);
                    $val = '';
                    if ( $argsArray ) {
                        $val = array_shift($argsArray);
                    }
                    $this -> _params[$key] = $val;
                }
            }
        }

        if ( count($_FILES) ) {
            foreach ( $_FILES as $file => $info) {
                $this -> _params[$file] = $info['name'];
            }
        }
    } // parseParams()

} // \Core\Controller\Request\Http
