<?php
namespace Core\Controller\Request;

interface IRequest
{
    public function __construct(array $argsArray = array());

    public function getParam($key, $default = null);

    public function hasParam($key);

    public function getParams();

} // \Core\Controller\Request\IRequest
