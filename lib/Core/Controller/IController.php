<?php

namespace Core\Controller;

interface IController
{
    public function __construct(
        \Core\App\IApp $app,
        \Core\View\IView $view,
        $controllerName,
        $actionName
    );

    public function indexAction();

    public function getControllerName();

    public function getActionName();

    public function notImplemented();

} // \Core\Controller\IController