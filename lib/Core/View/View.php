<?php
namespace Core\View;

class View extends \Core\View\AView
{
    /**
     * @var array $_variables
     */
    protected $_variables = array();

    /**
     * Get a view variable
     * @params string $variable
     * @params bool $htmlEncode
     * @return mixed
     */
    public function get($variable, $htmlEncode = true)
    {
        if ( isset($this -> _variables[$variable]) ) {
            if ( $htmlEncode ) {
                return $this -> htmlEncode($this -> _variables[$variable]);
            } else {
                return $this -> _variables[$variable];
            }
        }
    } // get()


    /**
     * Set a view variable
     * @param string $variable
     * @param mixed $value
     */
    public function set($variable, $value = null)
    {
        $this -> _variables[$variable] = $value;
    } // set()


    /**
     * @param $variable
     *
     * @return bool
     */
    public function has($variable)
    {
        return isset($this -> _variables[$variable]);
    } // isExists()


    /**
     * Recursively make a value safe for HTML
     * @param mixed $value
     * @return mixed
     */
    public function htmlEncode($value)
    {
        switch ( gettype($value) ) {
            case 'array':
                foreach ( $value as $k => $v ) {
                    $value[$k] = $this -> htmlEncode($v);
                }

                break;
            case 'object':
                foreach ( $value as $k => $v ) {
                    $value -> $k = $this -> htmlEncode($v);
                }

                break;
            default:
                $value = htmlentities($value, ENT_QUOTES, 'UTF-8');
        }

        return $value;
    } // htmlEncode()


    /**
     * Recursively decode an HTML encoded value
     * @param mixed $value
     * @return mixed
     */
    public function htmlDecode($value)
    {
        switch ( gettype($value) ) {
            case 'array':
                foreach ( $value as $k => $v ) {
                    $value[$k] = $this -> htmlDecode($v);
                }

                break;
            case 'object':
                foreach ( $value as $k => $v ) {
                    $value -> $k = $this -> htmlDecode($v);
                }

                break;
            default:
                $value = html_entity_decode($value, ENT_QUOTES, 'UTF-8');
        }

        return $value;
    } // htmlDecode()


    /**
     * @param $translateId
     * @param null $locale
     * @return mixed
     */
    public function _($translateId, $locale = null)
    {
        return $this -> _app -> getTranslate() -> translate($translateId, $locale);
    } // _()


    /**
     * @throws \Exception
     */
    public function render()
    {
        $viewPath   = $this -> _app -> getConfig('view_path');
        $controller = $this -> _app -> getController() -> getControllerName();
        $action     = $this -> _app -> getController() -> getActionName();
        $file = sprintf('%s/%s/%s.html.php', $viewPath, strtolower($controller), strtolower($action));

        $this -> view -> set('siteName', $this -> _app -> getConfig('site_name'));
        $this -> view -> set('pageTitle', $this -> _app -> getController() -> title);
        $this -> view -> set('pageDescription', $this -> _app -> getController() -> description);

        if ( is_file($file) ) {
            header('X-Generator: Swiftlet');

            require $file;
        } else {
            throw new \Exception('View not found');
        }
    } // render()

} // View
