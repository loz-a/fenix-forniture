<?php
namespace Core\View\Twig;

class Environment extends \Twig_Environment
{
    /**
     * @var \Core\View\IView $view;
     */
    protected $view;


    /**
     * @param \Core\View\IView           $view
     * @param null|\Twig_LoaderInterface $loader
     * @param array                      $options
     */
    public function __construct(
        \Core\View\IView $view,
        \Twig_LoaderInterface $loader = null,
        $options = array())
    {
        $this->setView($view);
        parent::__construct($loader, $options);
    } // __construct()


    /**
     * @return \Core\View\IView
     */
    public function getView()
    {
        return $this -> view;
    } // getView()


    /**
     * @param \Core\View\IView $view
     *
     * @return \Core\View\Twig\Environment
     */
    public function setView(\Core\View\IView $view)
    {
        $this -> view = $view;
        return $this;
    } // setView()

} // \Core\View\Twig\Environment