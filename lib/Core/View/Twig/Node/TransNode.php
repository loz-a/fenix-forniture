<?php
namespace Core\View\Twig\Node;

class TransNode extends \Twig_Node
{
    /**
     * @param \Twig_NodeInterface $body
     * @param                     $lineno
     * @param null                $tag
     */
    public function __construct(\Twig_NodeInterface $body, $lineno, $tag = null)
    {
        parent::__construct(array('body' => $body), array(), $lineno, $tag);
    } // __construct()


    /**
     * Compiles the node to PHP
     *
     * @param \Twig_Compiler $compiler
     */
    public function compile(\Twig_Compiler $compiler)
    {
        $compiler -> addDebugInfo($this)
            -> write(' echo $this -> env -> getView() -> _(')
            -> subcompile($this -> getNode('body'))
            -> raw(');');
    } // compile()

} // \Core\View\Twig\Node\TransNode
