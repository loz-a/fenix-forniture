<?php
namespace Core\View\Twig\TokenParser;

class TransTokenParser extends \Twig_TokenParser
{
    /**
     * Parses a token and returns a node.
     *
     * @param \Twig_Token $token A Twig_Token instance
     * @return \Twig_NodeInterface A Twig_NodeInterface instance
     */
    function parse(\Twig_Token $token)
    {
        $lineno = $token -> getLine();
        $stream = $this -> parser -> getStream();

        $body = null;
        if ( !$stream -> test(\Twig_Token::BLOCK_END_TYPE) ) {
            // {% trans "message" %}
            $body = $this -> parser -> getExpressionParser() -> parseExpression();
        }

        if ( null === $body ) {
            // {% trans %}message{% endtrans %}
            $stream -> expect(\Twig_Token::BLOCK_END_TYPE);
            $body = $this -> parser -> subparse(array($this, 'decideTransFork'), true);
        }

        $stream -> expect(\Twig_Token::BLOCK_END_TYPE);

        return new \Core\View\Twig\Node\TransNode($body, $lineno, $this -> getTag());
    } // parse()


    public function decideTransFork(\Twig_Token $token)
    {
        return $token -> test(array('endtrans'));
    } // decideTransFork()


    public function getTag()
    {
        return 'trans';
    } // getTag()

} // \Core\View\Twig\TokenParser\TransTokenParser
