<?php
namespace Core\View\Twig\Extension;

use Core\Helper\Collection\FlashMessenger;

class HelpersExtension extends \Twig_Extension
{
    public function getFunctions()
    {
        return array(
            'baseUrl' => new \Twig_Function_Method($this, 'getBaseUrl',
                array('needs_environment' => true)
            ),
            'requestUri' => new \Twig_Function_Method($this, 'getRequestUri',
                array('needs_environment' => true)
            ),
            'trans' => new \Twig_Function_Method($this, 'translate',
                array('needs_environment' => true)
            ),
            'isUserHasRole' => new \Twig_Function_Method($this, 'isUserHasRole'),
            'errors' => new \Twig_Function_Method($this, 'printErrors',
                array('is_safe' => array('html'))
            ),
            'successMessages' => new \Twig_Function_Method($this, 'printSuccessMessages',
                array('is_safe' => array('html'))
            ),
            'infoMessages' => new \Twig_Function_Method($this, 'printInfoMessages',
                array('is_safe' => array('html'))
            ),
            'errorMessages' => new \Twig_Function_Method($this, 'printErrorMessages',
                array('is_safe' => array('html'))
            ),
            'locale' => new \Twig_Function_Method($this, 'getCurrentLocale')
        );
    } // getFunctions()


    public function getBaseUrl(\Core\View\Twig\Environment $env, $requestUri = '')
    {
        return $env -> getView() -> baseUrl($requestUri);
    } // getBaseUrl()


    public function getRequestUri(\Core\View\Twig\Environment $env, $params = '')
    {
        return $env -> getView() -> requestUri($params);
    } // getRequestUri()


    public function translate(\Core\View\Twig\Environment $env, $messageId, $locale = null)
    {
        return $env -> getView() -> _($messageId, $locale);
    } // translate()


    public function isUserHasRole($role)
    {
        $auth = \Core\Auth\Auth::GetInstance();
        if ( $auth -> hasIdentity() ) {
            return $auth -> getIdentity() -> role === $role;
        }
        return false;
    } // isUserHasRole()


    public function printErrors($errors = array())
    {
        if ( $errors ) {
            if ( !is_array($errors) ) {
                $errors = array($errors);
            }
            $htmlOut = array();
            $htmlOut[] = '<ul class="errors">';

            foreach( $errors as $error) {
                $htmlOut[] = '<li class="error-item">' . current($error) .  '</li>';
            }
            $htmlOut[] = '</ul>';
            return implode(PHP_EOL, $htmlOut);
        }
        return '';
    } // printErrors()


    protected function _printMessages($type)
    {
        $htmlOut = '';
        $app = \Core\App\App::GetInstance();
        $flashMessenger = $app -> helper -> flashMessenger();
        $translator = $app -> getTranslate();

        if ( $flashMessenger -> hasMessages($type) ) {
            $htmlOut[] = '<div class="flash-messages sixteen columns row">';
            $htmlOut[] = '<ul class="' . $type .'-flash-messages">';
            $messages = $flashMessenger -> getMessages($type);
            foreach ($messages as $msg) {
                $htmlOut[] = '<li class="' . $type . '-flash-item">' . $translator -> _($msg['message']) . '</li>';
            }
            $htmlOut[] = '</ul></div>';
            $htmlOut = implode(PHP_EOL, $htmlOut);
        }
        return $htmlOut;
    } // _printMessages()


    public function printSuccessMessages()
    {
        return $this -> _printMessages(FlashMessenger::TYPE_SUCCESS);
    } // printSuccessMessages()


    public function printInfoMessages()
    {
        return $this -> _printMessages(FlashMessenger::TYPE_INFO);
    } // printInfoMessages()


    public function printErrorMessages()
    {
        return $this -> _printMessages(FlashMessenger::TYPE_ERROR);
    } // printErrorMessages()


    public function getCurrentLocale()
    {
        return \Core\App\App::GetInstance() -> getTranslate() -> getLocale();
    } // getCurrentLocale()


    /**
     * Returns the name of the extension.
     *
     * @return string The extension name
     */
    public function getName()
    {
        return 'core_helpers';
    } // getName()

} // \Core\View\Twig\Extension\FilterExtension
