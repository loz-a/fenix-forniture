<?php
namespace Core\View\Twig\Extension;

class TransExtension extends \Twig_Extension
{

    public function getFilters()
    {
            // {% tag "message"|trans %}
        return array(
            'trans' => new \Twig_Filter_Method($this, 'translate',
                array(
                    'needs_environment' => true
                ))
        );
    } // getFunctions()


    public function getTokenParser()
    {
        // {% trans "Hello world" %}
        return array(
            new \Core\View\Twig\TokenParser\TransTokenParser()
        );
    } // getTokenParser()


    public function translate(\Core\View\Twig\Environment $env, $translateId)
    {
        return $env -> getView() -> _app -> getTranslate() -> translate($translateId);
    } // translate()


    /**
     * Returns the name of the extension.
     *
     * @return string The extension name
     */
    function getName()
    {
        return 'core_translator';
    } // getName()

} // Core\View\Twig\Extension\TransExtension
