<?php
namespace Core\View\Twig\Extension;

class BreadCrumbsExtension extends \Twig_Extension
{
    public function getFunctions()
    {
        return array(
            'breadcrumbs' => new \Twig_Function_Method(
                $this, 'getBreadCrumbs',
                array(
                     'needs_environment' => true,
                     'is_safe' => array('html')
                )
            )
        );
    } // getFunction()

    public function getBreadCrumbs(\Core\View\Twig\Environment $env, $bc)
    {
        if ( !is_array($bc) or !count($bc) ) {
            return '';
        }

        $temp[] = '<ul class="bread-crumbs">';

        $last = array_pop($bc);
        foreach( $bc as $title => $link ) {
            $temp[] = '<li class="bc-item">';
            $temp[] = '<a href="' . $env -> getView() -> baseUrl($link). '" class="bc-link">';
            $temp[] = $env -> getView() -> _($title) . ' </a></li>';
        }
        $temp[] = '<li class="bc-item">' . $env -> getView() -> _($last) . '</li>';
        $temp[] = '</ul>';

        return implode(PHP_EOL, $temp);
    } // getBreadCrumbs()


    public function getName()
    {
        return 'core_breadcrumbs';
    } // getName()

} // \Core\View\Twig\Extension\BreadCrumbsExtension
