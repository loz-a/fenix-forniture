<?php
namespace Core\View;

class Smarty implements \Core\View\IView
{
    /**
     * Smarty object
     *
     * @var Smarty
     */
    protected $_smarty;

    /**
     * @var mixed
     */
    protected $_cacheId;


    public function __construct(\Core\App\IApp $app)
    {
        $this -> app = $app;
        $this -> name = $name;

        $classPath = $this -> app -> getConfig('smarty_classpath');
        require_once $classPath . '.php';
        $this -> _smarty = new \Smarty();

        $templateDir = $this -> app -> getConfig('smarty_template_dir');
        $compileDir  = $this -> app -> getConfig('smarty_compile_dir');
        $cacheDir    = $this -> app -> getConfig('smarty_cache_dir');
        $configDir   = $this -> app -> getConfig('smarty_config');

        $this -> setTemplateDir($templateDir)
            -> setCompileDir($compileDir)
            -> setCacheDir($cacheDir)
            -> setConfigDir($configDir);

        $this -> _smarty -> cache_lifetime = $this -> app -> getConfig('smarty_cache_lifetime');
        $this -> _smarty -> force_compile  = $this -> app -> getConfig('smarty_force_compile');
        $this -> _smarty -> caching   = $this -> app -> getConfig('smarty_caching');
        $this -> _smarty -> debugging = $this -> app -> getConfig('smarty_debugging');
    } // _construct()


    /**
     * @return \Smarty
     */
    public function getEngine()
    {
        return $this -> _smarty;
    } // getEngine()


    /**
     * @param string $path
     *
     * @return \Core\View
     */
    public function setTemplateDir($path)
    {
        $this -> _smarty -> setTemplateDir($path);
        return $this;
    } // setTemplateDir()


    /**
     * @param string $path
     * @return \Core\View
     */
    public function setCompileDir($path)
    {
        $this -> _smarty -> setCompileDir($path);
        return $this;
    } // setCompileDir()


    /**
     * @param string $path
     * @return \Core\View
     */
    public function setCacheDir($path)
    {
        $this -> _smarty -> setCacheDir($path);
        return $this;
    } // setCacheDir()


    public function setConfigDir($path)
    {
        $this -> _smarty -> setConfigDir($path);
        return $this;
    } // setConfigDir()

    /**
     * @param mixed $cacheId
     */
    public function setCacheId($cacheId)
    {
        $this->_cacheId = $cacheId;
    }


    public function set($tplVar, $value = null)
    {
        return $this -> _smarty -> assign($tplVar, $value);
    } // get()


    public function get($variable, $htmlEncode = true)
    {
        throw new \Exception('Method not implement');
    } // get()


    public function clearVars()
    {
        $this -> _smarty -> clearAllAssign();
    } // clearVars()


    public function render()
    {
        $template = $file = sprintf('%s/%s.tpl', $this -> name, $this -> app -> getAction());
       // return $this -> _smarty -> fetch($template, $this -> _cacheId);
//        var_dump(is_file(APPLICATION_PATH . '/views/index/helloworld.tpl'));die;

        $this -> _smarty -> fetch(APPLICATION_PATH . '/views/index/index.tpl');
    } // render()

} // View
