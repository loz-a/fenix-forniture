<?php
namespace Core\View;


interface IView
{
    public function __construct(\Core\App\IApp $app);

    public function render();

} // \Core\View\IView
