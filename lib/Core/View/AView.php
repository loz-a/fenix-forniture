<?php
namespace Core\View;

abstract class AView implements \Core\View\IView
{
    /**
     * @var \Core\App\IApp $_app
     */
    protected $_app;

    /**
     * @param \Core\App\IApp $app
     */
    public function __construct(\Core\App\IApp $app)
    {
        $this -> _app  = $app;
    } // __construct()


    abstract public function get($variable, $htmlEncode = true);

    abstract public function set($variable, $value = null);

    abstract public function has($variable);

    abstract public function htmlEncode($value);

    abstract public function htmlDecode($value);

    public function render()
    {} // render()


    /**
     * @param string $requestUri
     * @return string
     */
    public function baseUrl($requestUri = '')
    {
        return $this -> _app -> getRootPath() . ltrim($requestUri, '/');
    } // baseUrl()


    /**
     * @param string $params
     * @return string
     */
    public function requestUri($params = '')
    {
        return rtrim($_SERVER['REQUEST_URI'], '/') . $params;
    } // requestUri()

} // \Core\View\AView
