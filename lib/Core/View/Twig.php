<?php
namespace Core\View;

use \Core\Helper\Collection\CacheIdGenerator;

class Twig implements \Core\View\IView
{
    /**
     * Smarty object
     *
     * @var Twig_Environment
     */
    protected $_twig;

    /**
     * @var \Core\App\IApp $_app
     */
    protected $_app;

    /**
     * @var array $_variables
     */
    protected $_variables = array();

    /**
     * @param \Core\App\IApp $app
     */
    public function __construct(\Core\App\IApp $app)
    {
        $this -> _app = $app;

        $autoloaderClass = $this -> _app -> getConfig('twig_autoloader_class');
        $templatesPath   = $this -> _app -> getConfig('twig_templates_path');
        $compilationPath = $this -> _app -> getConfig('twig_compilation_cache_path');
        $autoReload      = $this -> _app -> getConfig('twig_auto_reload');
        $isDebug         = $this -> _app -> getConfig('twig_debug');

        require_once $autoloaderClass . '.php';

        $loader = new \Twig_Loader_Filesystem($templatesPath);
        $this -> _twig = new \Core\View\Twig\Environment($this, $loader, array(
            'cache'       => $compilationPath,
            'auto_reload' => $autoReload,
            'debug'       => $isDebug
        ));

        if ( APPLICATION_ENV === 'development' ) {
            $this -> _twig -> addExtension(new \Twig_Extension_Debug());
        }

        $this -> _twig -> addExtension(new \Core\View\Twig\Extension\HelpersExtension());
        $this -> _twig -> addExtension(new \Core\View\Twig\Extension\BreadCrumbsExtension());
    } // _construct()


    /**
     * @return Twig_Environment|\Twig_Environment
     */
    public function getEngine()
    {
        return $this -> _twig;
    } // getEngine()


    public function getApp()
    {
        return $this -> _app;
    } // getApp()


    /**
     * @return \Twig_LoaderInterface
     */
    public function getLoader()
    {
        return $this -> _twig -> getLoader();
    } // getLoader()


    /**
     * @param array|string $spec
     * @param null $value
     */
    public function assign($spec, $value = null)
    {
        if ( is_array($spec) ) {
            $this -> _variables = array_merge($this -> _variables, $spec);
        }
        $this -> _variables[$spec] = $value;
    } // assign()


    /**
     * clear all assign variables
     */
    public function clearVars()
    {
        $this -> _variables = array();
    } // clearVars()


    /**
     * Processes a tamplete and returns the output
     *
     * $param string $actin
     */
    public function render($action = null)
    {
        $controller = $this -> _app -> getController() -> getControllerName();
        if ( !$action ) {
            $action = $this -> _app -> getController() -> getActionName();
        }
        $file = sprintf('%s/%s.html.php', strtolower($controller), strtolower($action));

        $this -> assign('siteName', $this -> _app -> getConfig('site_name'));
        $this -> assign('metaTitle', $this -> _($this -> _app -> getController() -> metaTitle));
        $this -> assign('metaDescription', $this -> _($this -> _app -> getController() -> metaDescription));

        $template  = $this -> _twig -> loadTemplate($file);
        $cTemplate = $template -> render($this -> _variables);

        $cacheEnabled = $this -> _app -> getConfig('cache_enable');
        if ( $cacheEnabled ) {
            $cacheId = CacheIdGenerator::Factory() -> generate();
            if ( $cacheId ) {
                $this -> _app -> getCache() -> save($cTemplate, $cacheId);
            }
        }
        echo $cTemplate;
    } // render()


    /**
     * @param string $requestUri
     * @return string
     */
    public function baseUrl($requestUri = '')
    {
        return $this -> _app -> getRootPath() . ltrim($requestUri, '/');
    } // baseUrl()


    /**
     * @param string $params
     * @return string
     */
    public function requestUri($params = '')
    {
        return rtrim($_SERVER['REQUEST_URI'], '/') . $params;
    } // requestUri()


    /**
     * @param $translateId
     * @param null $locale
     * @return mixed
     */
    public function _($translateId, $locale = null)
    {
        return $this -> _app -> getTranslate() -> translate($translateId, $locale);
    } // _()

} // Twig
