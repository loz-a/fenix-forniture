<?php
namespace Core\Helper\Collection;

class FlashMessenger extends \Core\Helper\AHelper
{
    const TYPE_ERROR   = 'error';
    const TYPE_SUCCESS = 'success';
    const TYPE_INFO    = 'info';

    /**
     * @var array $_types
     */
    protected $_types = array(
        self::TYPE_ERROR,
        self::TYPE_SUCCESS,
        self::TYPE_INFO
    );

    /**
     * @var string $_namespace
     */
    protected $_namespace = 'Core_FlashMessenger';

    public function __construct()
    {
        if ( !isset($_SESSION) ) {
            session_start();
        }
    } // __construct()


    /**
     * @return \Core\Helper\Collection\FlashMessenger
     */
    public function process()
    {
        return $this;
    } // process()


    /**
     * @param string $notice
     *
     * @return \Core\Helper\Collection\FlashMessenger
     */
    public function addNotice($notice, $hops = 1)
    {
        if ( $notice ) {
            $_SESSION[$this -> _namespace][self::TYPE_INFO][] = array('message' => $notice, 'hopsCount' => $hops);
        }
        return $this;
    } // addNotice()


    /**
     * @param string $error
     * @return \Core\Helper\Collection\FlashMessenger
     */
    public function addError($error, $hops = 1)
    {
        if ( $error ) {
            $_SESSION[$this -> _namespace][self::TYPE_ERROR][] = array('message' => $error, 'hopsCount' => $hops);
        }
        return $this;
    } // addError()


    /**
     * @param string $success
     * @return \Core\Helper\Collection\FlashMessenger
     */
    public function addSuccess($success, $hops = 1)
    {
        if ( $success ) {
            $_SESSION[$this -> _namespace][self::TYPE_SUCCESS][] = array('message' => $success, 'hopsCount' => $hops);
        }
        return $this;
    } // addSuccess()


    /**
     * @return \Core\Helper\Collection\FlashMessenger
     */
    public function stepForward()
    {
        if ( !isset($_SESSION[$this -> _namespace]) ) {
            return;
        }

        foreach ( $_SESSION[$this -> _namespace] as $type => $msgs ) {
            if ( count($msgs) ) {

                $temp = array();
                foreach ( $msgs as $m ) {
                    if ( $m['hopsCount'] > 0 ) {
                        $m['hopsCount'] -= 1;
                        $temp[] = $m;
                    }
                }
                $_SESSION[$this -> _namespace][$type] = $temp;
            }
        }
        unset($temp);
        return $this;
    } // stepForward()


    /**
     * @return \Core\Helper\Collection\FlashMessenger
     */
    public function reset()
    {
        $_SESSION[$this -> _namespace] = array();
        return $this;
    } // reset()


    /**
     * @param null $type
     * @return bool
     * @throws \InvalidArgumentException
     */
    public function hasMessages($type = null)
    {
        if (
            $type !== null
            and !in_array($type, $this -> _types)
        ) {
            throw new \InvalidArgumentException('Wrong message type');
        }

        if ( $type ) {
            if ( isset($_SESSION[$this -> _namespace][$type]) ) {
                return (bool) count($_SESSION[$this -> _namespace][$type]);
            }
            return false;
        }
        return (bool) count($_SESSION[$this -> _namespace]);
    } // hasMessages()


    public function hasInfoMessages()
    {
        return $this -> hasMessages(self::TYPE_INFO);
    } // hasInfoMessages()


    public function hasSuccessMessages()
    {
        return $this -> hasMessages(self::TYPE_SUCCESS);
    } // hasInfoMessages()


    public function hasErrorMessages()
    {
        return $this -> hasMessages(self::TYPE_ERROR);
    } // hasInfoMessages()


    /**
     * @param null $type
     * @return array
     * @throws \InvalidArgumentException
     */
    public function getMessages($type = null)
    {
        if (
            $type !== null
            and !in_array($type, $this -> _types)
        ) {
            throw new \InvalidArgumentException('Wrong message type');
        }

        if ($type) {
            return $_SESSION[$this -> _namespace][$type];
        }
        return $_SESSION[$this -> _namespace];
    } // getMessages()


    public function getInfoMessages()
    {
        return $this -> getMessages(self::TYPE_INFO);
    } // getInfoMessages()


    public function getSuccessMessages()
    {
        return $this -> getMessages(self::TYPE_SUCCESS);
    } // getSuccessMessages()


    public function getErrorMessages()
    {
        return $this -> getMessages(self::TYPE_ERROR);
    } // getErrorMessages()

} // \Core\Helper\Collection\FlashMessenger