<?php
namespace Core\Helper\Collection;

class FileTransfer extends \Core\Helper\AHelper
{
    public function __construct()
    {
        if ( ini_get('file_uploads') == false ) {
            throw new \Core\Helper\HelperException('File uploads are not allowed in your php config!');
        }
    } // __construct()


    /**
     * @param string $file
     * @param string $dest
     * @param string $filename
     *
     * @return string
     * @throws \Core\Helper\HelperException
     */
    public function process()
    {
        $args = func_get_args();
        if ( !count($args) ) {
            throw new \Core\Helper\HelperException('Filename is undefined');
        }
        $file = array_shift($args);

        if ( !count($args) ) {
            throw new \Core\Helper\HelperException('Destination is undefined');
        }
        $dest = array_shift($args);
        $dest = $this -> _checkDestination($dest);

        $filename = '';
        if ( count($args) ) {
            $filename = array_shift($args);
        }

        if ( isset($_FILES[$file]) ) {
            return $this -> _receive($file, $dest, $filename);
        }
        return null;
    } // process()

    /**
     * @param string $dest
     * @return string
     * @throws \Core\Helper\HelperException
     */
    public function _checkDestination($dest)
    {
        $dest = rtrim($dest, '/\\');
        if ( !is_dir($dest) ) {
            mkdir($dest);
        }

        if ( !is_writable($dest) ) {
            throw new \Core\Helper\HelperException('The given destination is not writeable');
        }

        return $dest;
    } // _checkDestination()


    /**
     * @param string $file
     * @param string $dest
     * @param string $filename
     * @return string
     * @throws \Core\Helper\HelperException
     */
    public function _receive($file, $dest, $filename = '')
    {
        $isFile = is_uploaded_file($_FILES[$file]['tmp_name']);
        if ( !$isFile ) {
            throw new \Core\Helper\HelperException('File was not uploaded');
        }

        if ( !$filename ) {
            $filename = str_replace(
                array('/\s+/', '/[^-\.\w]+/'),
                array('_', ''),
                trim($_FILES[$file]['name']));
        }

        $filename = $this -> _getFilename($dest, $filename, $file);
        move_uploaded_file($_FILES[$file]['tmp_name'], $filename);

        return $filename;
    } // _receive()


    protected function _getFilename($dest, $filename, $file)
    {
        $filepath = str_replace(array('/', '\\'), DIRECTORY_SEPARATOR, $dest);
        $extension = strtolower(pathinfo(trim($_FILES[$file]['name']), PATHINFO_EXTENSION));

        return sprintf('%s/%s.%s', $dest, $filename,  $extension);
    } // _getFilename()

} // \Core\Helper\Collection\FileTransfer
