<?php
namespace Core\Helper\Collection\Imagick\ResizerStrategy;

class Fit extends AStrategy
{
    public function resize($filename, $width, $height, $quality, $outputDir = null)
    {
        $image = new \Imagick($filename);
        $image -> setimagecompression(\Imagick::COMPRESSION_JPEG);
        $image -> setimagecompressionquality($quality);
        $image -> stripimage();

        $geo = $image -> getimagegeometry();

        if ( $geo['width'] >= $geo['height'] and $width ) {
            $image -> resizeimage($width, 0, \Imagick::FILTER_LANCZOS, 1);
        }
        else if ( $geo['height'] > $geo['width'] and $height ) {
            $image -> resizeimage(0, $height, \Imagick::FILTER_LANCZOS, 1);
        }
        else {
            $image -> resizeImage($width, $height, \Imagick::FILTER_LANCZOS, 1);
        }

        $this -> _writeImage($image, $outputDir);
    } // resize()

} // Fit
