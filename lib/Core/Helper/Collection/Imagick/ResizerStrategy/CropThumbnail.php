<?php
namespace Core\Helper\Collection\Imagick\ResizerStrategy;

class CropThumbnail extends AStrategy
{
    public function resize($filename, $width, $height, $quality, $outputDir = null)
    {
        $image = new \Imagick($filename);
        $image -> setimagecompression(\Imagick::COMPRESSION_JPEG);
        $image -> setimagecompressionquality($quality);
        $image -> stripimage();

        $geo = $image->getimagegeometry();
        $ratio = min($geo['width'], $geo['height']) / max($width, $height);

        $rWidth = $width * $ratio;
        $rHeight = $height * $ratio;

        $image -> cropImage($rWidth, $rHeight, 0, 0);
        $image -> thumbnailimage($width, $height, true);

        $this -> _writeImage($image, $outputDir);
    } // resize()

} // CropThumbnail

