<?php
namespace Core\Helper\Collection\Imagick\ResizerStrategy;

class CropCenter extends AStrategy
{
    public function resize($filename, $width, $height, $quality, $outputDir = null)
    {
        $image = new \Imagick($filename);
        $image -> setImageCompression(\Imagick::COMPRESSION_JPEG);
        $image -> setImageCompressionQuality($quality);
        $image -> stripImage();

        $geo = $image -> getImageGeometry();

        if ( $geo['width'] <= $geo['height'] ) {
            $image -> scaleImage($width, 0);
            $scaledH = $image -> getImageHeight();

            $top = 0;
            if ( $scaledH > $height ) {
                $top = floor(($scaledH - $height) / 2);
            }
            $image -> cropImage($width, $height, 0, $top);
        }
        else {
            $image -> scaleImage(0, $height);
            $scaledW = $image -> getImageWidth();

            $left = 0;
            if ( $scaledW > $width ) {
                $left = round(($scaledW - $width) / 2);
            }
            $image -> cropImage($width, $height, $left, 0);
        }
        $this -> _writeImage($image, $outputDir);
    } // resize()

} // CropCenter
