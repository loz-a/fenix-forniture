<?php
namespace Core\Helper\Collection\Imagick\ResizerStrategy;

abstract class AStrategy
{
    /**
     * Return canvas resized according to the given dimensions.
     * @param string $filename
     * @param int $width Output width
     * @param int $height Output height
     * @param int $quality Output quality
     * @param string|null $outputDir
     */
    abstract public function resize($filename, $width, $height, $quality, $outputDir = null);


    /**
     * @param string $filename
     * @param string $outputDir
     *
     * @return string
     */
    protected function _getOutputFilename($filename, $outputDir)
    {
        $result = str_replace(array('\\', '/'), DIRECTORY_SEPARATOR, $filename);
        $result = explode(DIRECTORY_SEPARATOR, $result);
        $filename = array_pop($result);

        $outputDir = rtrim(str_replace(array('\\', '/'), DIRECTORY_SEPARATOR, $outputDir), DIRECTORY_SEPARATOR);

        return $outputDir . DIRECTORY_SEPARATOR . $filename;
    } // _getOutputFilename()


    protected function _writeImage(\Imagick $image, $outputDir = null)
    {
        $filename = $image -> getimagefilename();
        if ($outputDir) {
            $filename = $this -> _getOutputFilename($filename, $outputDir);
        }

        $image -> writeimage($filename);
        $image -> destroy();
    } // _writeImage

} // AStrategy

