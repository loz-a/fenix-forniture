<?php
namespace Core\Helper\Collection\Imagick;

use Core\Helper\Collection\Imagick\ResizerStrategy\AStrategy;

class ImageResizer extends \Core\Helper\AHelper
{
    /**
     * Default output width.
     * @var int
     */
    const DEFAULT_WIDTH = 32;
    /**
     * Default output height.
     * @var int
     */
    const DEFAULT_HEIGHT = 32;

    /**
     * Width of output image.
     * @var integer
     */
    protected $_width = self::DEFAULT_WIDTH;
    /**
     * Height of output image.
     * @var integer
     */
    protected $_height = self::DEFAULT_HEIGHT;

    /**
     * Quality for JPEG output.
     * @var int
     */
    protected $_quality = 75;

    /**
     * Directory to write output in.
     * @var string
     */
    protected $_outputDir = null;

    /**
     * Resizing strategy.
     * @var \Core\Helper\Collection\Imagick\ResizeStrategy\AStrategy
     */
    protected $_strategy = null;


    /**
     * Returns the result of filtering $value
     *
     * @param string $filename Path to an image.
     * @param array $optons Image options.
     * @return string Path to the resized image.
     * @throws \Exception|\InvalidArgumentException
     */

    public function process()
    {
        if (! extension_loaded('imagick')) {
            throw new \Exception("Imagick extension is not available. Can't process image.");
        }

        $args = func_get_args();
        if ( !count($args) ) {
            throw new \InvalidArgumentException('Expected image filename');
        }
        $filename = array_shift($args);

        $options = array();
        if ( count($args) ) {
            $options = array_shift($args);

            if ( !is_array($options) ) {
                throw new \InvalidArgumentException('Expected array');
            }
        }

        $strategy = '';
        if ( array_key_exists('strategy', $options) ) {
            $strategy = $options['strategy'];
        }
        $this -> setStrategy($strategy);

        if ( array_key_exists('outputDir', $options) ) {
            $this -> setOutputDir($options['outputDir']);
        }

        if ( array_key_exists('quality', $options) ) {
            $this -> setQuality($options['quality']);
        }

        if ( array_key_exists('height', $options) ) {
            $this -> setHeight($options['height']);
        }

        if ( array_key_exists('width', $options) ) {
            $this -> setWidth($options['width']);
        }

        if ( !file_exists($filename) ) {
            throw new \Exception('Image ' . $filename  . "isn't exists");
        }

        $this -> _resize($filename);
        return $filename;
    } // process()


    /**
     * Set the strategy for resizing.
     *
     * @param string $strategy
     * @return ImageResizer
     * @throws \LogicException
     */
    public function setStrategy($strategy = '')
    {
        $strategy = $strategy ? ucfirst($strategy) : 'Fit';
        $strategyClass = '\Core\Helper\Collection\Imagick\ResizerStrategy\\' . $strategy;

        if ( !class_exists($strategyClass) ) {
            throw new \LogicException('Wrong strategy class');
        }
        $this -> _strategy = new $strategyClass();
        if ( !($this -> _strategy instanceof AStrategy) ) {
            throw new \LogicException(get_class($this -> _strategy) . ' is not "\Core\Helper\Collection\Imagick\ResizeStrategy\AStrategy" subclass');
        }
        return $this;
    } // setStrategy()


    /**
     * Returns the strategy for resizing.
     * @return AStrategy
     */
    public function getStrategy()
    {
        return $this -> _strategy;
    } // getStrategy()


    /**
     * Set the output width in pixels.
     * @param $width
     * @return ImageResizer
     */
    public function setWidth($width)
    {
        $this -> _width = $width > 0 ? $width : 0;
        return $this;
    } // setWidth()


    /**
     * Get the output width in pixels.
     * @return int
     */
    public function getWidth()
    {
        return $this -> _width;
    } // getWidth()


    /**
     * Set the output height in pixels.
     * @param int $height
     * @return ImageResizer
     */
    public function setHeight($height)
    {
        $this->_height = $height > 0 ? $height : 0;
        return $this;
    } // setHeigth()


    /**
     * Get the output height in pixels.
     * @return int
     */
    public function getHeight()
    {
        return $this -> _height;
    } // getHeight()

    /**
     * Set the JPEG output compression.
     * @param int $q A value between 1 and 100.
     * @return ImageResizer
     */
    public function setQuality($q)
    {
        $q = (int) $q;

        if ($q > 100) {
            $q = 100;
        } elseif ($q < 1) {
            $q = 1;
        }
        $this -> _quality = $q;
        return $this;
    } // setQuality()


    /**
     * Get the JPEG output compression.
     * @return int
     */
    public function getQuality()
    {
        return $this->_quality;
    } // getQuality()


    /**
     * @param string $outputDir
     * @return App_Filter_ImagickResize
     */
    public function setOutputDir($outputDir)
    {
        if ( !is_dir($outputDir) ) {
            mkdir($outputDir);
        }
        $this -> _outputDir = $outputDir;
        return $this;
    } // setOutputDir()

    /**
     * @return string
     */
    public function getOutputDir()
    {
        return $this -> _outputDir;
    } // getOutputDir()


    /**
     * Load the image and resize it using the assigned strategy.
     * @return Filename of the output file.
     */
    protected function _resize($filename)
    {
        $this -> getStrategy() -> resize(
            $filename,
            $this -> getWidth(),
            $this -> getHeight(),
            $this -> getQuality(),
            $this -> getOutputDir());
    } // _resize()

} // \Core\Helper\Collection\ImageResizer
