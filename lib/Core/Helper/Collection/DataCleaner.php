<?php
namespace Core\Helper\Collection;

class DataCleaner extends \Core\Helper\AHelper
{
    public function process()
    {
        $value = func_get_arg(0);
        $result = null;

        if ( is_array($value) ) {
            $result = $this -> cleanArray($value);
        }
        else if ( is_scalar($value) ) {
            $result = $this -> cleanValue($value);
        }
        return $result;
    } // process()


    public function cleanArray(array $data)
    {
        $result = array();

        foreach ($data as $key => $value) {
            if ( is_scalar($value) ) {
                if ( is_int($value) ) {
                    $result[$key] = (int) $value;
                }
                else {
                    $result[$key] = $this -> cleanValue($value);
                }
            }
            else if ( is_array($value) ) {
                $result[$key] = $this -> cleanArray($value);
            }
        }
        return $result;
    } // cleanArray()


    public function cleanValue($value)
    {
        if ( get_magic_quotes_gpc() ) {
            $value = stripslashes($value);
        }
        $value = str_replace(array("\n", "\r"), array(" ", ""), strip_tags($value));
        return trim($value);
    } // cleanValue()

} // \Core\Helper\Collection\DataCleaner
