<?php
namespace Core\Helper\Collection;

use \Core\App\App,
    \Core\Auth\Auth;

class CacheIdGenerator extends \Core\Helper\AHelper
{
    /**
     * @var \Core\App\App
     */
    protected $_app;

    public function __construct()
    {
        $this -> _app = App::GetInstance();
    } // __construct()


    public function process()
    {
        return $this -> generate();
    } // filter()


    /**
     * @return string
     */
    public function generate()
    {
        $conName = $this -> _getControllerName();
        $actName = $this -> _getActionName();
        $params  = $this -> _getParams();
        $locale  = $this -> _getLocale();

        return $this -> _genCacheId($conName, $actName, $locale, $params);
    } // generate()


    /**
     * @return string
     * @throws \LogicException
     */
    protected function _getControllerName()
    {
        $cn = $this -> _app -> getController() -> getControllerName();
        if ( !$cn ) {
            throw new \LogicException('Controller name is undefined');
        }
        return $cn;
    } // _getControllerName()


    /**
     * @return string
     * @throws \LogicException
     */
    protected function _getActionName()
    {
        $an = $this -> _app -> getController() -> getActionName();
        if ( !$an ) {
            throw new \LogicException('Action name is undefined');
        }
        return $an;
    } // _getActionName()


    /**
     * @return array
     * @throws \LogicException
     */
    protected function _getParams()
    {
        $request = $this -> _app -> getRequest();
        if ( !$request ) {
            throw new \LogicException('Request is undefined');
        }
        return $request -> getParams();
    } // _getParams()


    /**
     * @return string
     * @throws \LogicException
     */
    protected function _getLocale()
    {
        $translate = $this -> _app -> getTranslate();
        if ( !$translate ) {
            throw new \LogicException('Locale is undefiend');
        }
        return $translate -> getLocale();
    } // _getLocale()


    /**
     * generate cache id
     *
     * @param string $controllerName
     * @param string $actionName
     * @param string $locale
     * @param array $params
     * @return string
     */
    protected function _genCacheId($controllerName, $actionName, $locale, array $params = array())
    {
        if ( isset($params['q']) ) {
            unset($params['q']);
        }

        $controllerName = strtolower($controllerName);
        $actionName     = strtolower($actionName);
        $locale         = strtolower($locale);

        $auth = \Core\Auth\Auth::GetInstance();
        $userRole = $auth -> hasIdentity() ? strtolower($auth -> getIdentity() -> role) : '';

        if ( !$this -> _isAllowedCache($controllerName, $actionName) ) {
            return null;
        }

        $result = array($controllerName, $actionName, $locale, $userRole);
        if ( count($params) ) {
            foreach ( $params as $key => $val ) {
                $val = str_replace('-', '_', $val);
                array_push($result, strtolower($key), strtolower($val));
            }
        }
        return implode('_', $result);
    } // _genCacheId()


    protected function _isAllowedCache($controllerName, $actionName)
    {
        $cfgFile = $this -> _app -> getConfig('cache_config_file');
        $cfg     = \Core\Config::Factory($cfgFile) -> toArray();

        $controllerName = strtolower($controllerName);
        $actionName     = strtolower($actionName);

        if ( isset($cfg[$controllerName]) ) {
            if ( !is_array($cfg[$controllerName]) ) {
                throw new \LogicException('Actions list must have array type');
            }

            if ( in_array($actionName, $cfg[$controllerName])) {
                return true;
            }
        }
        return false;
    } // _isAllowedCache()

} // Translit
