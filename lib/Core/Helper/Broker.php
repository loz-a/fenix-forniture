<?php
namespace Core\Helper;


class Broker
{
    /**
     * @var string $_helpersPath
     */
    protected $_helpersPath = '';

    /**
     * @var array $_helpers
     */
    protected $_helpers = array();


    protected $_defaultHelperPath = '/../lib/Core/Helper/Collection';

    protected $_defaultHelperNs = '\Core\Helper\Collection';


    public function __construct($helpersPath)
    {
        $this -> _helpersPath = $helpersPath;
    } // __construct()


    public function __call($name, $arguments)
    {
        $helper = null;

        if ( !isset($this -> _helpers[$name]) ) {
            $helperClass = ucfirst($name);

            $helperFile = sprintf('%s%s/%s.php', APPLICATION_PATH, $this -> _helpersPath, $helperClass);
            $defaultHelperFile = sprintf('%s%s/%s.php', APPLICATION_PATH, $this -> _defaultHelperPath, $helperClass);

            if ( is_file($helperFile) ) {
                $nsClass = \Core\App\App::Path2ns($this -> _helpersPath) . $helperClass;
            }
            else if ( is_file($defaultHelperFile) ) {
                $nsClass = $this -> _defaultHelperNs . '\\' . $helperClass;
            }
            else {
                throw new \LogicException('Wrong path to helper ' . $name);
            }

            $helper = new $nsClass();
            if($helper instanceof \Core\Helper\AHelper) {
                $this -> _helpers[$name] = $helper;
            }
            else {
                throw new \LogicException('Helper ' . $name . ' is not \Core\Helper subclass');
            }
            unset($nsClass, $defaultHelperFile, $helperFile, $helperClass);

        }
        else {
            $helper = $this -> _helpers[$name];
        }
        return call_user_func_array(array($helper , 'process'), $arguments);
    } // __call()

} // Broker
