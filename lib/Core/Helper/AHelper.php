<?php
namespace Core\Helper;

abstract class AHelper
{
    public function __invoke($arguments)
    {
        return $this -> process($arguments);
    } // __invoke()


    public function process()
    {} // process()


    public static function Factory()
    {
        return new static();
    } // Factory()

} // \Core\AHelp
