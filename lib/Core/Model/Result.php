<?php
namespace Core\Model;

class Result
{
    const STATUS_SUCCESS = 'success';
    const STATUS_ERROR = 'error';

    /**
     * @var string $_status
     */
    protected $_status = self::STATUS_SUCCESS;

    /**
     * @var array $_messages
     */
    protected $_messages = array();

    /**
     * @param array $data
     * @return \Core\Model\Result
     */
    public function populate(array $data)
    {
        foreach ( $data as $key => $value ) {
            $this -> $key = $value;
        }
        return $this;
    } // populate()


    /**
     * @return array
     */
    public function toArray()
    {
        return get_object_vars($this);
    } // toArray()


    /**
     * @return \Core\Model\Result
     */
    public function setSuccessStatus()
    {
        $this -> _status = self::STATUS_SUCCESS;
        return $this;
    } // setSuccessStatus()


    /**
     * @return \Core\Model\Result
     */
    public function setErrorStatus()
    {
        $this -> _status = self::STATUS_ERROR;
        return $this;
    } // setErrorStatus()


    /**
     * @return bool
     */
    public function isSuccess()
    {
        return $this -> _status === self::STATUS_SUCCESS;
    } // isSuccess()


    /**
     * @return bool
     */
    public function isError()
    {
        return $this -> _status === self::STATUS_ERROR;
    } // isError()


    /**
     * @param array $messages
     * @return \Core\Model\Result
     */
    public function setMessages(array $messages)
    {
        $this -> _messages = $messages;
        return $this;
    } // setMessages()


    /**
     * @param string $key
     * @param string $message
     */
    public function setMessage($key, $message)
    {
        $this -> _messages[$key] = $message;
    } // setMessage()


    /**
     * @param string $key
     * @return null|string
     */
    public function getMessage($key)
    {
        if ( array_key_exists($key, $this -> _messages) ) {
            return $this -> _messages[$key];
        }
        return null;
    } // getMessage()


    /**
     * @return array
     */
    public function getMessages()
    {
        return $this -> _messages;
    } // getMessages()

} // \Core\Model\Result
