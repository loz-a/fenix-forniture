<?php

namespace Core\Plugin;

abstract class APlugin implements \Core\Plugin\IPlugin
{
    /**
     * @var \Core\App\IApp $_app
     */
    protected $_app;

    /**
     * @var \Core\Controller\IController $_controller
     */
    protected $_controller;

    /**
     * @var \Core\View\IView $_view
     */
    protected $_view;

    /**
     * @param \Core\App\IApp $app
     * @param \Core\View\IView $view
     * @param null|\Core\Controller\IController $controller
     */
    public function __construct(
        \Core\App\IApp $app,
        \Core\View\IView $view = null,
        \Core\Controller\IController $controller = null)
	{
		$this -> _app = $app;

        if ( $view ) {
            $this -> setView($view);
        }

        if ( $controller ) {
		    $this -> setController($controller);
        }
	} // _construct()


    /**
     * @param \Core\Controller\IController $controller
     */
    public function setController(\Core\Controller\IController $controller)
    {
         $this -> _controller = $controller;
    } // setController()


    /**
     * @return null|\Core\Controller\IController
     */
    public function getController()
    {
        return $this -> _controller;
    } // getController()


    /**
     * @param \Core\View\IView $view
     */
    public function setView(\Core\View\IView $view)
    {
        $this -> _view = $view;
    } // setView()


    /**
     * @return \Core\View\IView|null
     */
    public function getView()
    {
        return $this -> _view;
    } // getView()

} // \Core\APlugin
