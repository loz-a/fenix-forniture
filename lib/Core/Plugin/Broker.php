<?php
namespace Core\Plugin;

class Broker
{
    /**
     * @var \Core\App\IApp $_app
     */
    protected $_app;

    /**
     * @var string $_pluginPath
     */
    protected $_pluginsPath;

    /**
     * @var array $_plugins
     */
    protected $_plugins = array();

    /**
     * @var array $_hooks
     */
    protected $_hooks = array();


    public function __construct(\Core\App\IApp $app, $pluginsPath)
    {
        $this -> _app = $app;
        $this -> _pluginsPath = $pluginsPath;

        $this -> _loadPlugins();
    } // __construct


    /**
     * Load plugins
     */
    public function _loadPlugins()
    {
        $files = glob(APPLICATION_PATH . $this -> _pluginsPath . '/*.php');

        foreach ( $files as $fileName ) {
            $ns = \Core\App\App::Path2ns($this -> _pluginsPath);
            $pluginName = $ns . rtrim(basename($fileName), '.php');
            $this -> _hooks[$pluginName] = array();

            $methodNames = get_class_methods($pluginName);
            foreach ( $methodNames as $methodName ) {
                $method = new \ReflectionMethod($pluginName, $methodName);

                if (
                    $method -> isPublic()
                    and ! $method -> isFinal()
                    and ! $method -> isConstructor()
                ) {
                   $this -> _hooks[$pluginName][] = $methodName;
                }
            }
        }
        ksort($this -> _hooks);
    } // _loadPlugins()


    /**
     * Register a hook for plugins to implement
     * @param string $hookName
     * @param array $params
     */
    public function registerHook($hookName, array $params = array())
    {
        foreach ( $this -> _hooks as $pluginName => $hooks ) {
            if ( in_array($hookName, $hooks) ) {
                if ( ! isset($this -> _plugins[$pluginName]) ) {
                    if ( ! $this -> _app -> isDispatched() ) {
                        $this -> _plugins[$pluginName] = new $pluginName($this -> _app);
                    }
                    else {
                        $this -> _plugins[$pluginName] = new $pluginName(
                            $this -> _app, $this-> _app -> getView(), $this -> _app -> getController());
                    }
                }
                else {
                    if ( $this -> _app -> isDispatched() ) {
                        if ( ! $this -> _plugins[$pluginName] -> getController() ) {
                            $this -> _plugins[$pluginName] -> setController($this -> _app -> getController());
                        }
                        if ( ! $this -> _plugins[$pluginName] -> getView() ) {
                            $this -> _plugins[$pluginName] -> setView($this -> _app -> getView());
                        }
                    }
                }
                $this -> _plugins[$pluginName] -> {$hookName}($params);
            }
        }
    } // registerHook()

} // \Core\Plugin\Broker
