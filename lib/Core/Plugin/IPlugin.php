<?php

namespace Core\Plugin;

interface IPlugin
{
	public function __construct(
        \Core\App\IApp $app,
        \Core\View\IView $view = null,
        \Core\Controller\IController $controller = null);

    public function setController(\Core\Controller\IController $controller);

    public function setView(\Core\View\IView $view);
}
