<?php
namespace Core;

class Locale
{
    /**
     * @static
     * @return null|string
     */
    public static function FromUri()
    {
        if ( preg_match('/\/lang\/(\w+)($|\/)/i',  $_SERVER['REQUEST_URI'], $match) ) {
            return $match[1];
        }
        return null;
    } // FromGet()


    /**
     * @static
     * @return null|string
     */
    public static function FromSession()
    {
        if ( isset($_SESSION['Core_Locale']['lang']) ) {
            return $_SESSION['Core_Locale']['lang'];
        }
        return null;
    } // FromSession()


    public static function ToSession($locale)
    {
        $_SESSION['Core_Locale']['lang'] = $locale;
    } // ToSession()


    public static function IsInSession()
    {
        return isset($_SESSION['Core_Locale']['lang']);
    } // IsInSession()


    /**
     * @static
     */
    public static function ClearSession()
    {
        $_SESSION['Core_Locale']['lang'] = null;
    } // ClearSession()

} // \Core\Locale
