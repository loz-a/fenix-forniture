<?php
namespace Core\Auth\Adapter;

interface IAdapter
{
    /**
     * Performs an authentication attempt
     *
     * @throws \Exception If authentication cannot be performed
     * @return \Core\Auth\Result
     */
    public function authenticate();

} // \Core\Auth\Adapter\IAdapter
