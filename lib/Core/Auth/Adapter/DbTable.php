<?php
namespace Core\Auth\Adapter;

use \Core\Auth\Crypter;

class DbTable implements \Core\Auth\Adapter\IAdapter
{
    /**
     * @var string $_tableName
     */
    protected $_tableName;

    /**
     * @var string $_identityColumn
     */
    protected $_identityColumn;

    /**
     * @var string $_credentialColumn
     */
    protected $_credentialColumn;

    /**
     * @var string $_identity
     */
    protected $_identity;

    /**
     * @var string $_credential
     */
    protected $_credential;


    /**
     * @param array $config
     */
    public function __construct(array $config = array())
    {
        if ( array_key_exists('tableName', $config) ) {
            $this -> setTableName($config['tableName']);
        }

        if ( array_key_exists('identityColumn', $config) ) {
            $this -> setIdentityColumn($config['identityColumn']);
        }

        if ( array_key_exists('credentialColumn', $config) ) {
            $this -> setCredentialColumn($config['credentialColumn']);
        }

        if ( array_key_exists('identity', $config) ) {
            $this -> setIdentity($config['identity']);
        }

        if ( array_key_exists('credential', $config) ) {
            $this -> setCredential($config['credential']);
        }
    } // __contruct()


    /**
     * Performs an authentication attempt
     *
     * @throws \Exception If authentication cannot be performed
     * @return \Core\Auth\Result
     */
    public function authenticate()
    {
        $this -> _verifyNecessaryParams();

        $user = \Core\Orm::for_table($this -> _tableName)
            -> select($this -> _identityColumn)
            -> select($this -> _credentialColumn)
            -> select('role')
            -> where_equal($this -> _identityColumn, $this -> _identity)
            -> find_one();

        $result = new \Core\Auth\Result(array('password'));
        if ( $user ) {
            $valid = Crypter::Check($this -> _credential, $user -> password);

            if ( $valid ) {
                $result -> populate($user -> as_array());
                $result -> setSuccessStatus();
                return $result;
            }
        }
        $result -> setErrorStatus();
        return $result;
    } // authenticate()


    /**
     * @param string $tableName
     *
     * @return \Core\Auth\Adapter\DbTable
     */
    public function setTableName($tableName)
    {
        $this -> _tableName = $tableName;
        return $this;
    } // setTableName()


    /**
     * @param string $identityColumn
     * @return \Core\Auth\Adapter\DbTable
     */
    public function setIdentityColumn($identityColumn)
    {
        $this -> _identityColumn = $identityColumn;
        return $this;
    } // setIdentityColumn()


    /**
     * @param string $credentialColumn
     * @return \Core\Auth\Adapter\DbTable
     */
    public function setCredentialColumn($credentialColumn)
    {
        $this -> _credentialColumn = $credentialColumn;
        return $this;
    } // setCredentialColumn()


    /**
     * @param string $identity
     * @return \Core\Auth\Adapter\DbTable
     */
    public function setIdentity($identity)
    {
        $this -> _identity = $identity;
        return $this;
    } // setIdentity()


    /**
     * @param string $credential
     * @return \Core\Auth\Adapter\DbTable
     */
    public function setCredential($credential)
    {
        $this -> _credential = $credential;
        return $this;
    } // setCredential()


    /**
     * @throws \Exception
     */
    protected function _verifyNecessaryParams()
    {
        $exception = null;

        if ( $this -> _tableName == '' ) {
            $exception = 'A table must be supplied for the \Core\Auth\Adapter\DbTable authentication adapter.';
        }
        elseif ( $this -> _identityColumn == '' ) {
            $exception = 'An identity column must be supplied for the \Core\Auth\Adapter\DbTable authentication adapter.';
        }
        elseif ( $this -> _credentialColumn == '' ) {
            $exception = 'A credential column must be supplied for the \Core\Auth\Adapter\DbTable authentication adapter.';
        }
        elseif ( $this -> _identity == '' ) {
            $exception = 'A value for the identity was not provided prior to authentication with \Core\Auth\Adapter\DbTable.';
        }
        elseif ( $this -> _credential === null ) {
            $exception = 'A credential value was not provided prior to authentication with \Core\Auth\Adapter\DbTable.';
        }

        if ( $exception ) {
            throw new \Exception($exception);
        }
    } // _verifyNecessaryParams()

} // \Core\Auth\Adapter\DbTable
