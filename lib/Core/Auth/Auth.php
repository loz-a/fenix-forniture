<?php
namespace Core\Auth;

class Auth
{
    /**
     * @var \Core\Auth\Auth $_instance
     */
    protected static $_inctance;

    protected $_storage;


    protected function __construct()
    {} // __construct()


    protected function __clone()
    {} // __clone()


    /**
     * @static
     * @return \Core\Auth\Auth
     */
    public static function GetInstance()
    {
        if ( null === self::$_inctance ) {
            self::$_inctance = new self();
        }
        return self::$_inctance;
    } // GetInstance()


    /**
     * @return \Core\Auth\Storage\IStorage
     */
    public function getStorage()
    {
        if ( null === $this -> _storage ) {
            $this -> setStorage(new \Core\Auth\Storage\Session());
        }
        return $this -> _storage;
    } // getStorage()


    /**
     * @param Storage\IStorage $storage
     * @return \Core\Auth\Storage\IStorage
     */
    public function setStorage(\Core\Auth\Storage\IStorage $storage)
    {
        $this -> _storage = $storage;
        return $this;
    } // setStorage()


    /**
     * @param \Core\Auth\Adapter\IAdapter $adapter
     * @return \Core\Auth\Result
     */
    public function authenticate(\Core\Auth\Adapter\IAdapter $adapter)
    {
        $result = $adapter -> authenticate();

        if ( $this -> hasIdentity() ) {
            $this -> clearIdentity();
        }

        if ( $result -> isSuccess() ) {
            $this -> getStorage() -> write($result);
        }
        return $result;
    } // authenticate()


    /**
     * @return bool
     */
    public function hasIdentity()
    {
        return ( !$this -> getStorage() ->  isEmpty() );
    } // hasIdentity()


    public function getIdentity()
    {
        $storage = $this -> getStorage();
        if ( $storage -> isEmpty() ) {
            return null;
        }
        return $storage -> read();
    } // getIdentity()


    /**
     * @return \Core\Auth\Auth
     */
    public function clearIdentity()
    {
        $this -> getStorage() -> clear();
        return $this;
    } // clearIdentity()

} // \Core\Auth\Auth
