<?php
namespace Core\Auth;

class Result
{
    const STATUS_SUCCESS = 'success';
    const STATUS_ERROR = 'error';

    /**
     * @var string $_status
     */
    protected $_status = self::STATUS_ERROR;

    /**
     * @var array $_protectedFields
     */
    protected $_protectedFields = array();


    public function __construct(array $protectedFields = array())
    {
        $this -> _protectedFields = $protectedFields;
    } // __construct()


    /**
     * @param array $data
     *
     * @return \Core\Auth\Result
     */
    public function populate(array $data)
    {
        foreach ( $data as $key => $value ) {
            if ( !in_array($key, $this -> _protectedFields) ) {
                $this -> $key = $value;
            }
        }
        return $this;
    } // populate()


    /**
     * @return \Core\Model\Result
     */
    public function setSuccessStatus()
    {
        $this -> _status = self::STATUS_SUCCESS;
        return $this;
    } // setSuccessStatus()


    /**
     * @return \Core\Model\Result
     */
    public function setErrorStatus()
    {
        $this -> _status = self::STATUS_ERROR;
        return $this;
    } // setErrorStatus()


    /**
     * @return bool
     */
    public function isSuccess()
    {
        return $this -> _status === self::STATUS_SUCCESS;
    } // isSuccess()


    /**
     * @return bool
     */
    public function isError()
    {
        return $this -> _status === self::STATUS_ERROR;
    } // isError()

} // \Core\Auth\Result
