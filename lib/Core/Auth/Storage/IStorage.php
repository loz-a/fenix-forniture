<?php
namespace Core\Auth\Storage;

interface IStorage
{
    /**
     * @abstract
     * @return bool
     */
    public function isEmpty();

    /**
     * @abstract
     * @return mixed
     */
    public function read();

    /**
     * @abstract
     *
     * @param object $dataObject
     * @return \Core\Auth\Storage\IStorage
     */
    public function write($dataObject);

    /**
     * @abstract
     *
     */
    public function clear();

} // \Core\Auth\Storage\IStorage
