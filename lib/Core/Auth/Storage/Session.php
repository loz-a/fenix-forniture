<?php
namespace Core\Auth\Storage;

class Session implements \Core\Auth\Storage\IStorage
{
    const NAMESPACE_DEFAULT = 'Core_Auth_Storage';

    /**
     * @var string $_namespace
     */
    protected $_namespace;


    /**
     * @param string $namespace
     */
    public function __construct($namespace = self::NAMESPACE_DEFAULT)
    {
        $this -> _namespace = $namespace;
    } // __construct()


    /**
     * @return bool
     */
    public function isEmpty()
    {
        return !isset($_SESSION[$this -> _namespace]);
    } // isEmpty()


    /**
     * @return mixed
     */
    public function read()
    {
        return $_SESSION[$this -> _namespace];
    } // read()


    /**
     * @param object $dataObject
     *
     * @return \Core\Auth\Storage\Session
     */
    public function write($dataObject)
    {
        if ( !is_object($dataObject) ) {
            throw new \InvalidArgumentException('Wrong argument type');
        }
        $_SESSION[$this -> _namespace] = $dataObject;
        return $this;
    } // write()


    public function clear()
    {
        unset($_SESSION[$this -> _namespace]);
    } // clear()

} // \Core\Auth\Storage\Session
