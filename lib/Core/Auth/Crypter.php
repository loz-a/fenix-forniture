<?php
namespace Core\Auth;

/**
 * see http://net.tutsplus.com/tutorials/php/understanding-hash-functions-and-keeping-passwords-safe/
 */
class Crypter
{
    const SALT_LENGTH = 22;

    /**
     * crypt blowfish
     * @var string $_algo
     */
    protected static $_algo = '$2a$';

    /**
     * cost parameter
     * @var string $_const
     */
    protected static $_cost = '10';


    /**
     * @static
     * @return string
     */
    public static function UniqueSalt()
    {
        return substr(sha1(mt_rand()), 0, self::SALT_LENGTH);
    } // UniqueSalt()


    /**
     * @static
     *
     * @param $value
     *
     * @return string
     */
    public static function Crypt($value)
    {
        return crypt($value, self::$_algo . self::$_cost . '$' . self::UniqueSalt());
    } // Hash()


    /**
     * @static
     * @param $value
     * @param $hash
     * @return bool
     */
    public static function Check($value, $hash)
    {
        $salt = substr($hash, 0, (self::SALT_LENGTH + 7));
        $orignHash = crypt($value, $salt);
        return ( $hash === $orignHash );
    } // Check()


    public static function GetSaltLength()
    {
        return self::SALT_LENGTH + 7;
    } // GetSaltLength()

} // \Core\App\PasswordHash
