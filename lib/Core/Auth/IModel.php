<?php
namespace Core\Auth;

interface IModel
{
    public static function AuthAdapterFactory($login, $passwd);
}
