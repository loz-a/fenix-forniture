<?php
namespace Core;

/**
 * example:
 *      $translate = new \Core\Translate($app);
 *      $translate -> setLocal('uk');
 *      $translate -> loadData();
 *      $app -> translate = $translate;
 *
 * some other script
 *      $app -> translate -> _('Hello world'); => 'Привіт світ'
 */
class Translate
{
    /**
     * @var string $_locale
     */
    protected $_locale;

    /**
     * @var string $_defaultLocale
     */
    protected $_defaultLocale = 'en';


    /**
     * @var array $_availablesLocales
     */
    protected $_availablesLocales = array();

    /**
     * @var string $_localeFile
     */
    protected $_localeFile;

    /**
     * @var array $_data
     */
    protected $_data = array();

    public function __construct($optons)
    {
        $this -> _availablesLocales = $optons['availablesLocales'];
        $this -> _defaultLocale     = $optons['defaultLocale'];
        $this -> _fileLocale        = $optons['localeFile'];
    } // __construct()


    /**
     * @param string $locale
     * @return Translate
     * @throws \DomainException
     */
    public function setLocale($locale)
    {
        if ( in_array($locale, $this -> _availablesLocales) ) {
            $this -> _locale = $locale;
            \Core\Locale::ToSession($locale);
        }
        else {
            $this -> _locale = $this -> _defaultLocale;
        }
        return $this;
    } // setLocale()


    /**
     * @return string
     * @throws \LogicException
     */
    public function getLocale()
    {
        if ( ! $this -> _locale ) {
            throw new \LogicException('Locale is undefined');
        }
        return $this -> _locale;
    }


    /**
     * @throws \Exception|\LogicException
     */
    public function loadData()
    {
        $locale = $this -> getLocale();

        $file = sprintf('%s/%s.php', $this -> _fileLocale, $locale);
        if ( !file_exists($file) ) {
            throw new \Exception('Wrong path to translate config file');
        }

        ob_start();
        $this -> _data = include($file);
        ob_end_clean();

        if ( !is_array($this -> _data) ) {
            throw new \LogicException('Error including array or file');
        }
    } // _loadData()


    /**
     * @param string $messageId
     * @param null $locale
     * @return string
     */
    public function translate($messageId, $locale = null)
    {
        if ( $locale === null ) {
            $locale = $this -> getLocale();
        }

        if ( array_key_exists($messageId, $this -> _data) ) {
            return $this -> _data[$messageId];
        }
        return $messageId;
    } // translate()


    /**
     * @param $messageId
     * @param null $locale
     * @return string
     */
    public function _($messageId, $locale = null)
    {
        return $this -> translate($messageId, $locale);
    } // _()

} // Core\Translate
