<?php
namespace Core;

class Config
{
    /**
     * @var array $_config
     */
    protected $_config = array();

    /**
     * @param string $configFile
     */
    public function __construct($configFile)
    {
        if ( !file_exists($configFile) ) {
            throw new \Exception('Wrong path to config file "' . $configFile . '"');
        }

        ob_start();
        $this -> _config = include($configFile);
        ob_end_clean();

        if ( !is_array($this -> _config) ) {
            throw new \LogicException('Error including array or file');
        }
    } // __construct()


    /**
     * @return array
     */
    public function toArray()
    {
        return $this -> _config;
    } // toArray()


    public static function Factory($configFile)
    {
        return new self($configFile);
    } // Factory()

} // \Core\Config
