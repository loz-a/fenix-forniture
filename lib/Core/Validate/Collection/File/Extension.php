<?php
namespace Core\Validate\Collection\File;

use \Core\Validate\AValidator as ValidateAbstract;

class Extension extends ValidateAbstract
{
    /**
     * @const
     */
    const WRONG_EXTENSION = 'fileExtensionWrong';

    /**
     * @var array $_messageTemplates
     */
    protected $_messageTemplates = array(
        self::WRONG_EXTENSION => "File '%s' has a wrong extension. Allowed extensions: %s"
    );

    /**
     * @var array $_extensions
     */
    protected $_extensions = array();

    /**
     * @param null|string $title
     * @param array $options
     */
    public function __construct($title = null, array $options = array())
    {
        parent::__construct($title, $options);

        if ( array_key_exists('extensions', $options) ) {
            $this -> setExtensions($options['extensions']);
        }
    } // __construct()


    /**
     * @param array $extensions
     *
     * @return Extension
     */
    public function setExtensions(array $extensions)
    {
        $this -> _extensions = $extensions;
        return $this;
    } // setExtensions()


    /**
     * @param string $value
     * @return bool
     */
    public function isValid($value)
    {
        $file = strtolower($this -> _title);
        $isFile = is_uploaded_file($_FILES[$file]['tmp_name']);

        if ( !$isFile ) {
            return true;
        }

        $filename = trim($value);
        $fileExt = strtolower(pathinfo($filename, PATHINFO_EXTENSION));
        foreach ( $this -> _extensions as $ext ) {
            if ( strtolower($ext) === strtolower($fileExt) ) {
                return true;
            }
        }
        $this -> _error(self::WRONG_EXTENSION, $filename, implode(', ', $this -> _extensions));
        return false;
    } // isValid()

} // \Core\Validate\Collection\File\Extension
