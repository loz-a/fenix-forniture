<?php
namespace Core\Validate\Collection\File;

use \Core\Validate\AValidator as ValidateAbstract;

class Size extends ValidateAbstract
{
    const TOO_BIG   = 'fileSizeTooBig';
    const TOO_SMALL = 'fileSizeTooSmall';

    protected $_messageTemplates = array(
        self::TOO_BIG   => "Maximum allowed size for file '%s' is '%s' but '%s' detected",
        self::TOO_SMALL => "Minimum expected size for file '%s' is '%s' but '%s' detected"
    );

    protected $_min;

    protected $_max;

    public function __construct($title = null, array $options = array())
    {
        parent::__construct($title, $options);

        if ( array_key_exists('min', $options) ) {
            $this -> setMin($options['min']);
        }

        if ( array_key_exists('max', $options) ) {
            $this -> setMax($options['max']);
        }
    } // __construct()


    public function setMin($min)
    {
        if ( !is_scalar($min) ) {
            throw new \InvalidArgumentException('Invalid options to validator provided');
        }
        $min = (int) self::FromByteString($min);

        if ($this -> _max and $min > $this -> _max) {
            $max = self::ToByteString($this -> _max);
            $min = self::ToByteString($this -> _min);
            throw new \InvalidArgumentException(
                "The minimum must be less than or equal to the maximum filesize, but $min > $max");
        }
        $this -> _min = $min;
        return $this;
    } // setMin()


    public function setMax($max)
    {
        if ( !is_scalar($max) ) {
            throw new \InvalidArgumentException('Invalid options to validator provided');
        }
        $max = (integer) self::FromByteString($max);
        if ( $this -> _min !== null and ($max < $this -> _min) ) {
            $max = self::ToByteString($this -> _max);
            $min = self::ToByteString($this -> _min);
            throw new \InvalidArgumentException(
                "The maximum must be greater than or equal to the minimum filesize, but $max < $min");
        }
        $this -> _max = $max;
        return $this;
    } // setMax()


    public function isValid($value)
    {
        $file = strtolower($this -> _title);
        $isFile = is_uploaded_file($_FILES[$file]['tmp_name']);

        if ( !$isFile ) {
            return true;
        }
        $size = sprintf("%u", @filesize($_FILES[$file]['tmp_name']));

        $min = self::FromByteString($this -> _min);
        $max = self::FromByteString($this -> _max);
        $filename = $_FILES[$file]['name'];

        if ( $min !== null and $size < $min ) {
            $min  = self::ToByteString($min);
            $size = self::ToByteString($size);
            $this -> _error(self::TOO_SMALL, $filename, $min, $size);
            return false;
        }

        if ( $max !== null and $max < $size ) {
            $max  = self::ToByteString($max);
            $size = self::ToByteString($size);
            $this -> _error(self::TOO_BIG, $filename, $max, $size);
            return false;
        }
        return true;
    } // isValid()


    /**
     * Returns the formatted size
     *
     * @static
     * @param  integer $size
     * @return string
     */
    public static function ToByteString($size)
    {
        $sizes = array('B', 'kB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB');
        for ( $i=0; $size >= 1024 && $i < 9; $i++ ) {
            $size /= 1024;
        }

        return round($size, 2) . $sizes[$i];
    } // ToByteString()


    /**
     * Returns the unformatted size
     *
     * @static
     * @param  string $size
     * @return integer
     */
    public static function FromByteString($size)
    {
        if ( is_numeric($size) ) {
            return (integer) $size;
        }

        $type  = trim(substr($size, -2, 1));

        $value = substr($size, 0, -1);
        if ( !is_numeric($value) ) {
            $value = substr($value, 0, -1);
        }

        switch ( strtoupper($type) ) {
            case 'Y':
                $value *= (1024 * 1024 * 1024 * 1024 * 1024 * 1024 * 1024 * 1024);
                break;
            case 'Z':
                $value *= (1024 * 1024 * 1024 * 1024 * 1024 * 1024 * 1024);
                break;
            case 'E':
                $value *= (1024 * 1024 * 1024 * 1024 * 1024 * 1024);
                break;
            case 'P':
                $value *= (1024 * 1024 * 1024 * 1024 * 1024);
                break;
            case 'T':
                $value *= (1024 * 1024 * 1024 * 1024);
                break;
            case 'G':
                $value *= (1024 * 1024 * 1024);
                break;
            case 'M':
                $value *= (1024 * 1024);
                break;
            case 'K':
                $value *= 1024;
                break;
            default:
                break;
        }

        return $value;
    } // FromByteString()

} // \Core\Validate\Collection\File\Size
