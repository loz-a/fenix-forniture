<?php
namespace Core\Validate\Collection\File;

use \Core\Validate\AValidator as ValidateAbstract;

class Upload extends ValidateAbstract
{
    const UPLOAD_ERR = 'uploadError';

    protected $_messageTemplates = array(
        self::UPLOAD_ERR => 'File not uploaded. Something went wrong'
    );

    protected $_uploadErrors = array(
        UPLOAD_ERR_INI_SIZE    => 'Larger than upload_max_filesize',
        UPLOAD_ERR_FORM_SIZE   => 'Larger than form MAX_FILE_SIZE',
        UPLOAD_ERR_PARTIAL     => 'Partial upload',
        UPLOAD_ERR_NO_FILE     => 'No file',
        UPLOAD_ERR_NO_TMP_DIR  => 'No temporary directory',
        UPLOAD_ERR_CANT_WRITE  => "Can't write to disk",
        UPLOAD_ERR_EXTENSION   => 'File upload stopped by extension'
    );

    protected $_isRequiredUpload = false;


    public function __construct($title = null, array $options = array())
    {
        parent::__construct($title, $options);

        if ( array_key_exists('requiredUpload', $options) ) {
            $this -> setRequiredUpload($options['requiredUpload']);
        }
    } // __construct()


    public function setRequiredUpload($flag)
    {
        $this -> _isRequiredUpload = (bool) $flag;
        return $this;
    } // setRequiredUploadFlag()


    public function isValid($value)
    {
        $file = strtolower($this -> _title);
        $error = $_FILES[$file]['error'];

        if (
            $error !== UPLOAD_ERR_OK
            and ( $this -> _isRequiredUpload
                 and $error === UPLOAD_ERR_NO_FILE )
        ) {
            $date = date('d.m.Y h:i:s');
            $msg = $this -> _uploadErrors[$error] . '| Date: ' . $date . PHP_EOL;
            error_log($msg, 3, APPLICATION_LOG_FILE);

            $this -> _error(self::UPLOAD_ERR);
            return false;
        }
        return true;
    } // isValid()

} // Upload
