<?php
namespace Core\Validate\Collection\File;

use \Core\Validate\AValidator as ValidateAbstract;

class IsImage extends ValidateAbstract
{
    const NOT_IMAGE = 'fileIsNotImage';

     protected $_messageTemplates = array(
         self::NOT_IMAGE   => "File '%s' is not image",
     );

    public function isValid($value)
    {
        $file = strtolower($this -> _title);
        $filename = $_FILES[$file]['tmp_name'];

        $isFile = is_uploaded_file($filename);
        if ( !$isFile ) {
            return true;
        }

        $result = null;

        if ( extension_loaded('imagick') ) {
            try {
                $image = new \Imagick($filename);
                $result = $image -> getimagegeometry();
            }
            catch (\ImagickException $e) {
            }
        }
        else if ( extension_loaded('gd') ) {
            $result = getimagesize($filename);
        }
        else {
            throw new \Exception('No one extension for working with images is not loaded');
        }

        if ( empty($result) ) {
            $filename = $_FILES[$file]['name'];
            $this -> _error(self::NOT_IMAGE, $filename);
            return false;
        }
        return true;
    } // isValid()

} // \Core\Validate\Collection\File\IsImage
