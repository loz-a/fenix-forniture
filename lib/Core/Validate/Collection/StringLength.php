<?php
namespace Core\Validate\Collection;

use \Core\Validate\AValidator as ValidatorAbstract;

class StringLength extends ValidatorAbstract
{
    const INVALID   = 'stringLengthInvalid';
    const TOO_SHORT = 'stringLengthTooShort';
    const TOO_LONG  = 'stringLengthTooLong';

    protected $_messageTemplates = array(
        self::INVALID   => "Invalid type given. String expected",
        self::TOO_SHORT => "'%s' is less than %s characters long",
        self::TOO_LONG  => "'%s' is more than %s characters long",
    );

    /**
     * Minimum length
     *
     * @var integer
     */
    protected $_min = 0;

    /**
     * Maximum length
     *
     * If null, there is no maximum length
     *
     * @var integer|null
     */
    protected $_max;

    /**
     * Encoding to use
     *
     * @var string
     */
    protected $_encoding = 'UTF-8';

    /**
     * @var string $_title
     */
    protected $_title = 'String length';


    public function __construct($title = null, array $options = array())
    {
        parent::__construct($title, $options);

        if ( array_key_exists('min', $options) ) {
            $this -> setMin($options['min']);
        }

        if ( array_key_exists('max', $options) ) {
            $this -> setMax($options['max']);
        }

        if ( array_key_exists('encoding', $options) ) {
            $this -> setEncoding($options['encoding']);
        }
    } // __construct()


    /**
     * @param $min
     * @return \Core\Validate\Collection\StringLength
     * @throws \InvalidArgumentException
     */
    public function setMin($min)
    {
        if ( $this -> _max and  $min > $this -> _max ) {
            throw new \InvalidArgumentException('The minimum must be less than or equal to the maximum length');
        }

        $this->_min = max(0, (int) $min);
        return $this;
    } // setMin()


    /**
     * @return int
     */
    public function getMin()
    {
        return $this -> _min;
    } // getMin()


    /**
     * @param $max
     * @return \Core\Validate\Collection\StringLength
     * @throws \InvalidArgumentException
     */
    public function setMax($max)
    {
        if ( !$max ) {
            $this -> _max = null;
        }
        else if ( $max < $this -> _min ) {
            throw new \InvalidArgumentException('The maximum must be greater than or equal to the minimum length');
        }
        else {
            $this -> _max = (int) $max;
        }
        return $this;
    } // setMax()


    /**
     * @return int|null
     */
    public function getMax()
    {
        return $this -> _max;
    } // getMax()


    /**
     * @param $encoding
     * @return \Core\Validate\Collection\StringLength
     * @throws \InvalidArgumentException
     */
    public function setEncoding($encoding)
    {
        if ( $encoding ) {
            $orig = iconv_get_encoding('internal_encoding');
            $result = iconv_set_encoding('internal_encoding', $encoding);

            if ( ! $result ) {
                throw new \InvalidArgumentException('Given encoding not supported on this OS!');
            }
            iconv_set_encoding('internal_encoding', $orig);
        }
        $this -> _encoding = $encoding;
        return $this;
    } // setEncoding()


    /**
     * @return null|string
     */
    public function getEncoding()
    {
        return $this -> _encoding;
    } // getEndcoding()


    public function isValid($value)
    {
        if ( !is_string($value) ) {
            $this -> _error(self::INVALID, $this -> _title);
        }

        if ( $this -> _encoding ) {
            $length = iconv_strlen($value, $this -> _encoding);
        }
        else {
            $length = iconv_strlen($value);
        }

        if ( $length < $this -> _min ) {
            $this -> _error(self::TOO_SHORT, $this -> _title, $this -> _min);
        }

        if ( $this -> _max and $this -> _max < $length ) {
            $this -> _error(self::TOO_LONG, $this -> _title, $this -> _max);
        }

        if ( count($this -> _messages) ) {
            return false;
        }
        return true;
    } // isValid()

} // \Core\Validate\Collection\Length
