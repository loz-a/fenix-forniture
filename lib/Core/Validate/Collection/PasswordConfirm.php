<?php
namespace Core\Validate\Collection;

use \Core\Validate\AValidator as ValidatorAbstract;

class PasswordConfirm extends ValidatorAbstract
{
    const NOT_MATCH = 'notMatch';

    protected $_messageTemplates = array(
        self::NOT_MATCH => 'Password and confirmation do not match');

    public function isValid($value, array $context = array())
    {
        $confirm = $context['password'];

        if ( $confirm === $value ) {
            return true;
        }
        $this -> _error(self::NOT_MATCH);
        return false;
    } // isValid()

} // \Core\Validate\Collection\PasswordConfirm
