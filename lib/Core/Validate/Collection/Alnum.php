<?php
namespace Core\Validate\Collection;

use \Core\Validate\Collection\Alpha;

class Alnum extends Alpha
{
    protected $_messageTemplates = array(
        self::INVALID      => "Invalid type given. String expected",
        self::NOT_ENGLISH  => "'%s' may contain characters English alphabet and digits",
        self::NOT_CYRILLIC => "'%s' may contain characters English alphabet, Cyrillic alphabet and digits",
    );

    public function _generatePattern()
    {
        $pattern = '/^[a-zA-Z0-9';

        if ( $this -> _cyrillic ) {
            $pattern .= chr(0x7F) . '-' . chr(0xff);
        }

        if ( $this -> _dash ) {
            $pattern .= '\-';
        }

        if ( $this -> _underscore ) {
            $pattern .= '_';
        }

        if ( $this -> _whitespace ) {
            $pattern .= '\s';
        }

        if ( $this -> _dot ) {
            $pattern .= '\.';
        }

        if ( $this -> _coma ) {
            $pattern .= '\,';
        }

        $pattern .= ']+$/';
        return $pattern;
    } // _generatePattern()

} // \Core\Validate\Collection\Alnum;
