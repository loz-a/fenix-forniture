<?php
namespace Core\Validate\Collection;

use \Core\Validate\AValidator as ValidatorAbstract;

class Regexpr extends ValidatorAbstract
{
    const INVALID   = 'regexInvalid';
    const NOT_MATCH = 'regexNotMatch';
    const ERROROUS  = 'regexErrorous';

    protected $_messageTemplates = array(
        self::INVALID   => "Invalid type given. String, integer or float expected",
        self::NOT_MATCH => "'%s' does not match against pattern '%s'",
        self::ERROROUS  => "There was an internal error while using the pattern '%s'",
    );


    /**
     * Regular expression pattern
     *
     * @var string $_pattern
     */
    protected $_pattern;

    /**
     * @var bool $_skipEmpty
     */
    protected $_skipEmpty = true;


    public function __construct($title = null, array $options = array())
    {
        parent::__construct($title, $options);

        if ( array_key_exists('pattern', $options) ) {
            $this -> setPattern($options['pattern']);
        }

        if ( array_key_exists('skipEmpty', $options) ) {
            $this -> setSkipEmpty($options['skipEmpty']);
        }
    } // __construct()


    /**
     * Returns the pattern option
     * @return string
     */
    public function getPattern()
    {
        return $this -> _pattern;
    } // getPattern


    /**
     * Sets the pattern option
     *
     * @param string $pattern
     * @return Regexpr
     * @throws \LogicException
     */
    public function setPattern($pattern)
    {
        $this -> _pattern = (string) $pattern;
        $status = @preg_match($this->_pattern, "Test");

        if ( false === $status ) {
            throw new \LogicException("Internal error while using the pattern '$this->_pattern'");
        }
        return $this;
    } // setPattern()


    /**
     * @param boolean $flag
     * @return Regexpr
     */
    public function setSkipEmpty($flag)
    {
        $this -> _skipEmpty = (bool) $flag;
        return $this;
    } // setSkipEmpty()


    /**
     * @return bool
     */
    public function isSkipEmpty()
    {
        return $this -> _skipEmpty;
    } // isSkipEmpty()


    public function isValid($value)
    {
        if ( $this -> isSkipEmpty() and empty($value) ) {
            return true;
        }

        if (
            !is_string($value)
            and !is_int($value)
            and !is_float($value)
        ) {
            $this -> _error(self::INVALID);
            return $this;
        }

        $status = @preg_match($this -> _pattern, $value);
        if ( false === $status ) {
            $this -> _error(self::ERROROUS, $this -> _pattern);
            return false;
        }

        if ( !$status ) {
            $this -> _error(self::NOT_MATCH, $value, $this -> _pattern);
            return false;
        }
        return true;
    } // isValid()

} // Regexpr
