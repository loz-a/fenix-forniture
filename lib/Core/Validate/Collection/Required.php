<?php
namespace Core\Validate\Collection;

use \Core\Validate\AValidator as ValidatorAbstract;

class Required extends ValidatorAbstract
{
    const EMPTY_VALUE = 'empty';

    protected $_messageTemplates = array(
        self::EMPTY_VALUE => "Value is required and can't be empty"
    );

    public function isValid($value)
    {
        $value = (string) $value;
        if ( strlen($value) > 0 ) {
            return true;
        }
        $this -> _error(self::EMPTY_VALUE);
        return false;
    } // isValid()

} // \Core\Validate\Collection\Required
