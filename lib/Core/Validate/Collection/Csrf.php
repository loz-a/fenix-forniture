<?php
namespace Core\Validate\Collection;

class Csrf implements \Core\Validate\IValidator
{
    /**
     * @var string
     */
    protected $_token;

    /**
     * @var int
     */
    protected $_timeout = 300;

    /**
     * @var string
     */
    protected $_salt = 'salt';

    public function __construct($salt = '')
    {
        if ( ! isset($_SESSION) ) {
            session_start();
        }

        if ( $salt ) {
           $this -> setSalt($salt);
        }
    } // __construct()


    /**
     * @static
     * @param string $salt
     * @return \Core\Validate\Collection\Csrf
     */
    public static function Factory($salt = '')
    {
        return new self($salt);
    } // Factory()


    public function isValid($token)
    {
        $sessionName = $this -> getSessionName();
        if (
            isset($_SESSION[$sessionName])
            and $token === $_SESSION[$sessionName]
        ) {
            $age = $_SESSION[$sessionName . '_time'];
            if ( (time() - $age) <= $this -> getTimeout() ) {
                return true;
            }
        }
        return false;
    } // isValid


    /**
     * @param $timeout
     *
     * @return \Core\Validate\Collection\Csrf
     */
    public function setTimeout($timeout)
    {
        $this->_timeout = $timeout;
        return $this;
    } // setTimeout()


    /**
     * @return int
     */
    public function getTimeout()
    {
        return $this->_timeout;
    } // getTimeout()


    /**
     * @param string $salt
     *
     * @return \Core\Validate\Collection\Csrf
     */
    public function setSalt($salt)
    {
        $this -> _salt = (string) $salt;
        return $this;
    } // setSalt()


    public function getSalt()
    {
        return $this -> _salt;
    } // getSalt()


    public function getSessionName()
    {
        return 'Core_Validate_Csrf' . '_' . $this -> getSalt();
    } // getSessionName()


    public function getToken()
    {
        if (null === $this -> _token) {
            $this -> _generateToken();
            $this -> saveTokenInSession();
        }
        return $this->_token;
    } // getToken()


    public function saveTokenInSession()
    {
        $sessionName = $this -> getSessionName();
        $_SESSION[$sessionName] = $this -> getToken();
        $_SESSION[$sessionName . '_time'] = time();
    } // saveTokenInSession()


    /**
     * Generate CSRF token
     */
    protected function _generateToken()
    {
        $this -> _token = md5(
            mt_rand(1, 1000000)
            . $this -> getSalt()
            . uniqid(mt_rand(1, 1000000), true));
    } // _generateToken()


    public function hasMessages()
    {
        throw new \LogicException('Method not implement');
    } // hasMessages()


    public function getMessages()
    {
        throw new \LogicException('Method not implement');
    } // getMessages();


} // \Core\Validate\Collerction\Csrf
