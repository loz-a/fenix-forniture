<?php
namespace Core\Validate\Collection;

class Form implements \Core\Validate\IValidator
{
    /**
     * @var array $_options
     */
    protected $_options = array();

    /**
     * @var array $_chains
     */
    protected $_chains = array();

    /**
     * @var array $_messages
     */
    protected $_messages = array();


    public function __construct(array $optins = array())
    {
        $this -> _options = $optins;

        $this -> init();
    } // __construct()


    public function init()
    {
    }// init()


    /**
     * @param string                $formElement
     * @param \Core\Validate\Chain $chain
     * @param bool                  $replace
     *
     * @return Form
     * @throws \OutOfBoundsException
     */
    public function addChain($formElement, \Core\Validate\Chain $chain, $replace = true)
    {
        $formElement = strtolower($formElement);
        if (
            !$replace
            and array_key_exists($formElement, $this -> _chains)
        ) {
            throw new \OutOfBoundsException('Validators chain for form element ' . $formElement . 'is exists');
        }
        $this -> _chains[$formElement] = $chain;
        return $this;
    } // addChain()


    /**
     * @param string                $formElement
     * @param \Core\Validate\Chain $chain
     * @param bool                  $onlyIfExists
     * @return Form
     * @throws \OutOfBoundsException
     */
    public function removeChain($formElement, \Core\Validate\Chain $chain, $onlyIfExists = false)
    {
        $formElement = strtolower($formElement);
        if (
            $onlyIfExists
            and !array_key_exists($formElement, $this -> _chains)
        ) {
            throw new \OutOfBoundsException('Validators chain for form element ' . $formElement . 'is not exists');
        }
        if ( array_key_exists($formElement, $this -> _chains) ) {
            unset($this -> _chains[$formElement]);
        }
        return $this;
    } // removeVAlidators()


    /**
     * @param string $formElement
     * @return null
     */
    public function getChain($formElement)
    {
        $formElement = strtolower($formElement);
        if ( array_key_exists($formElement, $this -> _chains) ) {
            return $this -> _chains[$formElement];
        }
        return null;
    } // getValidators()


    /**
     * @param mixed $data
     * @return bool
     * @throws \UnexpectedValueException
     */
    public function isValid($data)
    {
        if ( !is_array($data) ) {
            throw new \UnexpectedValueException('Expected type must be array');
        }

        $result = true;
        foreach ( $this -> _chains as $formElement => $chain ) {
            $formElement = strtolower($formElement);
            if ( array_key_exists($formElement, $data) ) {
                $result = $chain -> isValid($data[$formElement], $data) && $result;
                if ( $chain -> hasMessages() ) {
                    $this -> _messages[$formElement] = $chain -> getMessages();
                }
            }
        }
        return $result;
    } // isValid()


    /**
     * @return array
     */
    public function getMessages()
    {
        return $this -> _messages;
    } // getMessages()


    /**
     * @return bool
     */
    public function hasMessages()
    {
        return (bool) count($this -> _messages);
    } // hasMessages()

} // \Core\Validate\Collection\Form
