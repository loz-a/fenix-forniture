<?php
namespace Core\Validate\Collection;

use \Core\Validate\AValidator as ValidateAbstract;

class Alpha extends ValidateAbstract
{
    const INVALID      = 'alphaInvalid';
    const NOT_ENGLISH  = 'notEnglishAlpha';
    const NOT_CYRILLIC = 'notCyrillicAlpha';

    protected $_messageTemplates = array(
        self::INVALID      => "Invalid type given. String expected",
        self::NOT_ENGLISH  => "'%s' may contain characters English alphabet",
        self::NOT_CYRILLIC => "'%s' may contain characters English and Cyrillic alphabet",
    );

    /**
     * @var bool $_whitespace
     */
    protected $_whitespace = false;

    /**
     * @var bool $_undersoce
     */
    protected $_underscore = true;

    /**
     * @var bool $_dash
     */
    protected $_dash = true;

    /**
     * @var bool $_dash
     */
    protected $_dot = false;

    /**
     * @var bool $_coma
     */
    protected $_coma = false;


    /**
     * @var bool $_cyrillic
     */
    protected $_cyrillic = false;


    public function __construct($title = null, array $options = array())
    {
        parent::__construct($title, $options);

        if ( array_key_exists('whitespace', $options) ) {
            $this -> setWhitespace($options['whitespace']);
        }

        if ( array_key_exists('underscore', $options) )  {
            $this -> setUnderscore($options['underscore']);
        }

        if ( array_key_exists('dash', $options) ) {
            $this -> setDash($options['dash']);
        }

        if ( array_key_exists('dot', $options) ) {
            $this -> setDot($options['dot']);
        }

        if ( array_key_exists('coma', $options) ) {
            $this -> setComa($options['coma']);
        }

        if ( array_key_exists('cyrillic', $options) ) {
            $this -> setCyrillic($options['cyrillic']);
        }
    } // __construct()


    /**
     * @param $flag
     * @return \Core\validate\Collection\Alpha
     */
    public function setDash($flag)
    {
        $this -> _dash = (bool) $flag;
        return $this;
    } // setDash()


    /**
     * @return bool
     */
    public function getDash()
    {
        return $this -> _dash;
    } // getDash()


    /**
     * @param bool $flag
     * @return \Core\Validate\Collection\Alpha
     */
    public function setUnderscore($flag)
    {
        $this -> _underscore = (bool) $flag;
        return $this;
    } // setUderscore()


    /**
     * @return bool
     */
    public function getUnderscore()
    {
        return $this -> _underscore;
    } // getUnderscore()


    /**
     * @param bool $flag
     * @return \Core\Validate\Collection\Alpha
     */
    public function setWhitespace($flag)
    {
        $this -> _whitespace = (bool) $flag;
        return $this;
    } // setWhitespace()


    /**
     * @return bool
     */
    public function getWhitespace()
    {
        return $this -> _whitespace;
    } // getWhitespace()


    /**
     * @param bool $flag
     *
     * @return \Core\Validate\Collection\Alpha
     */
    public function setDot($flag)
    {
        $this -> _dot = (bool) $flag;
        return $this;
    } // setDot()


    /**
     * @return bool
     */
    public function getDot()
    {
        return $this -> _dot;
    } // getDot()


    /**
     * @param bool $flag
     * @return Alpha
     */
    public function setComa($flag)
    {
        $this -> _coma = (bool) $flag;
        return $this;
    } // setDot()


    /**
     * @return bool
     */
    public function getComa()
    {
        return $this -> _coma;
    } // getDot()


    /**
     * @param bool $flag
     * @return \Core\Validate\Collection\Alpha
     */
    public function setCyrillic($flag)
    {
        $this -> _cyrillic = (bool) $flag;
        return $this;
    } // setCyrillyc()


    /**
     * @return bool
     */
    public function getCyrillic()
    {
        return $this -> _cyrillic;
    } // getCyrillic()


    public function isValid($value)
    {
        if ( empty($value) ) {
            return true;
        }

        $value = trim($value);

        if ( !is_scalar($value) ) {
            $this -> _error(self::INVALID);
            return false;
        }
        $pattern = $this -> _generatePattern();
        $result = @preg_match($pattern, $value);
        if ( !$result ) {
            if ( $this -> _cyrillic ) {
                $this -> _error(self::NOT_CYRILLIC, $this -> _title);
            }
            else {
                $this -> _error(self::NOT_ENGLISH, $this -> _title);
            }
            return false;
        }
        return true;
    } // isValid()


    public function _generatePattern()
    {
        $pattern = '/^[a-zA-Z';

        if ( $this -> _cyrillic ) {
            $pattern .= chr(0x7F) . '-' . chr(0xff);
        }

        if ( $this -> _dash ) {
            $pattern .= '\-';
        }

        if ( $this -> _underscore ) {
            $pattern .= '_';
        }

        if ( $this -> _whitespace ) {
            $pattern .= '\s';
        }

        if ( $this -> _dot ) {
            $pattern .= '\.';
        }

        if ( $this -> _coma ) {
            $pattern .= '\,';
        }

        $pattern .= ']+$/';
        return $pattern;
    } // _generatePattern()


} // \Core\Validate\Collection\Alpha
