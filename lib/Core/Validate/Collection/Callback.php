<?php
namespace Core\Validate\Collection;

use \Core\Validate\AValidator as ValidateAbstract;

/**
 *
 */
class Callback extends ValidateAbstract
{
    /**
     * Invalid callback
     */
    const INVALID_CALLBACK = 'callbackInvalid';

    /**
     * Invalid value
     */
    const INVALID_VALUE = 'callbackValue';

    /**
     * Validation failure message template definitions
     * @var array
     */
    protected $_messageTemplates = array(
        self::INVALID_VALUE    => "'%s' is not valid",
        self::INVALID_CALLBACK => "An exception has been raised within the callback",
    );

    /**
     * @var string $_title
     */
    protected $_title = 'Value';

    /**
     * @var \Closure $_callback
     */
    protected $_callback;


    /**
     * @param null|string  $title
     * @param array $options
     */
    public function __construct($title = null, array $options = array())
    {
        parent::__construct($title, $options);

        if ( array_key_exists('callback', $options) ) {
            $this -> setCallback($options['callback']);
        }
    } // __construct()


    /**
     * @param \Closure $callback
     * @return Callback
     */
    public function setCallback(\Closure $callback)
    {
        $this -> _callback = $callback;
        return $this;
    } // setCallback()


    /**
     * @return \Closure
     * @throws \LogicException
     */
    public function getCallback()
    {
        if ( !$this -> _callback ) {
            throw new \LogicException('Callback is undefined');
        }
        return $this -> _callback;
    } // getCallback()


    /**
     * @param string $value
     * @return bool
     */
    public function isValid($value)
    {
        try {
            $cb = $this -> getCallback();

            if ( !call_user_func($cb, $value) ) {
                $this -> _error(self::INVALID_VALUE, $this -> _title);
                return false;
            }
        }
        catch(\Exception $e) {
            $this -> _error(self::INVALID_CALLBACK);
            return false;
        }
        return true;
    } // isValid()

} // Callback
