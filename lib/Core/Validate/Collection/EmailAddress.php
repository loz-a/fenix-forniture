<?php
namespace Core\Validate\Collection;

use \Core\Validate\AValidator as ValidatorAbstract;

class EmailAddress extends ValidatorAbstract
{
    const INVALID = 'emailAddressInvalid';

    protected $_messageTemplates = array(
        self::INVALID => 'Invalid e-mail address'
    );

    /**
     * @var bool $_skipEmpty
     */
    protected $_skipEmpty = true;


    public function __construct($title = null, array $options = array())
    {
        parent::__construct($title, $options);

        if ( array_key_exists('skipEmpty', $options) ) {
            $this -> setSkipEmpty($options['skipEmpty']);
        }
    } // __construct()


    /**
     * @param boolean $flag
     * @return Regexpr
     */
    public function setSkipEmpty($flag)
    {
        $this -> _skipEmpty = (bool) $flag;
        return $this;
    } // setSkipEmpty()


    /**
     * @return bool
     */
    public function isSkipEmpty()
    {
        return $this -> _skipEmpty;
    } // isSkipEmpty()


    public function isValid($value)
    {
        if ( $this -> isSkipEmpty() and empty($value) ) {
            return true;
        }

        if ( !filter_var($value, FILTER_VALIDATE_EMAIL) ) {
            $this -> _error(self::INVALID);
            return false;
        }
        return true;
    } // isValid()

} // \Core\Validate\Collection\Required
