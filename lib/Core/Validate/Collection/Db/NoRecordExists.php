<?php
namespace Core\Validate\Collection\Db;

use \Core\Validate\Collection\Db\ADb,
    \Core\Orm;

class NoRecordExists extends ADb
{
    public function isValid($value)
    {
        $value = $this -> _callPreValidate($value);

        $select = Orm::for_table($this -> _table)
            -> select($this -> _field)
            -> where_equal($this -> _field, $value);

        if ( $this -> _include ) {
            $select -> where_raw($this -> _include);
        }

        if ( $select -> find_one() ) {
            $this -> _error(self::ERROR_RECORD_FOUND, $this -> _title, $value);
            return false;
        }
        return true;
    } // isValid()

} // \Core\Validate\Collection\Db\NoRecordExists
