<?php
namespace Core\Validate\Collection\Db;

use \Core\Validate\AValidator as ValidateAbstract;

class ADb extends ValidateAbstract
{
    /**
     * Error constants
     */
    const ERROR_NO_RECORD_FOUND = 'noRecordFound';
    const ERROR_RECORD_FOUND    = 'recordFound';

    /**
     * @var array Message templates $_messageTemplates
     */
    protected $_messageTemplates = array(
        self::ERROR_NO_RECORD_FOUND => "%s '%s' not exists",
        self::ERROR_RECORD_FOUND    => "%s '%s' is exists",
    );

    /**
     * @var string $_table
     */
    protected $_table = '';

    /**
     * @var string $_field
     */
    protected $_field = '';

    /**
     * @var string $_include
     */
    protected $_include = '';

    /**
     * @var string $_title
     */
    protected $_title = 'Record';


    /**
     * @var \Closure $_preValidate
     */
    protected $_preValidate;

    public function __construct($title = null, array $options = array())
    {
        parent::__construct($title, $options);

        if ( array_key_exists('table', $options) ) {
            $this -> setTable($options['table']);
        }

        if ( array_key_exists('field', $options) ) {
            $this -> setField($options['field']);
        }

        if ( array_key_exists('include', $options) ) {
            $this -> setExclude($options['include']);
        }

        if ( array_key_exists('pre_validate', $options) ) {
            if ( !$options['pre_validate'] instanceof \Closure ) {
                throw new \InvalidArgumentException('pre_validate must be a Closure');
            }
            $this -> _preValidate = $options['pre_validate'];
        }
    } // __construct()


    /**
     * Returns the set include clause
     *
     * @return string
     */
    public function getInclude()
    {
        return $this -> _include;
    } // getInclude()


    /**
     * Sets a new include clause
     *
     * @param string $include
     * @return \Core\Validate\Collection\Db\ADb
     */
    public function setExclude($include)
    {
        $this -> _include = $include;
        return $this;
    } // setInclude()

    /**
     * Returns the set field
     *
     * @return string
     */
    public function getField()
    {
        return $this -> _field;
    } // getField()


    /**
     * Sets a new field
     *
     * @param string $field
     * @return \Core\Validate\Collection\Db\ADb
     */
    public function setField($field)
    {
        $this -> _field = (string) $field;
        return $this;
    } // setField


    /**
     * Returns the set table
     *
     * @return string
     */
    public function getTable()
    {
        return $this -> _table;
    } // getTable()


    /**
     * Sets a new table
     *
     * @param string $table
     * @return \Core\Validate\Collection\Db\ADb
     */
    public function setTable($table)
    {
        $this -> _table = (string) $table;
        return $this;
    } // setTable()


    public function isValid($value)
    {
        throw new \Exception('Method not implement');
    } // isValid()


    protected function _callPreValidate($value)
    {
        if ( $this -> _preValidate ) {
            $pv = $this -> _preValidate;
            $value = call_user_func($pv, $value);
            if ( !$value ) {
                throw new \LogicException('pre_validate must return value');
            }
        }
        return $value;
    } // _callPreValidate()

} // Core\Validate\Collection\Db\ADb
