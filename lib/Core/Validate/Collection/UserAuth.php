<?php
namespace Core\Validate\Collection;

use \Core\Validate\AValidator as ValidatorAbstract;

class UserAuth extends ValidatorAbstract
{
    const USER_NOT_VALID = 'notValid';

    protected $_messageTemplates = array(
        self::USER_NOT_VALID => 'Password is wrong');


    public function isValid($value, array $context = array())
    {
        $login = $context['login'];

        $auth = \Models\Users::AuthAdapterFactory($login, $value) -> authenticate();
        if ( $auth -> isSuccess()) {
            return true;
        }
        $this -> _error(self::USER_NOT_VALID);
        return false;
    } // isValid()

} // \Core\Validate\Collection\UserAuth
