<?php
namespace Core\Validate;

class Chain implements \Core\Validate\IValidator
{
    /**
     * validators chain
     * @var array $_chain
     */
    protected $_chain = array();

    /**
     * error messages array
     * @var array $_messages
     */
    protected $_messages = array();


    /**
     * Add validator if not exists
     *
     * @param \Core\Validate\IValidator $validator
     * @param bool $replace
     *
     * @return \Core\Validate\Chain
     * @throws \OutOfBoundsException
     */
    public function addValidator(\Core\Validate\IValidator $validator, $replace = true)
    {
        $key = $this -> _getKey($validator);

        if (
            !$replace
            and array_key_exists($key, $this -> _chain)
        ) {
            $valSet = $this -> _chain[$key];
            if ( !is_array($valSet) ) {
                $valSet = array($valSet);
            }
            array_push($valSet, $validator);
            $this -> _chain[$key] = $valSet;
        }
        else {
            $this -> _chain[$key] = $validator;
        }
        return $this;
    } // addValidator()

    /**
     * Remove validator
     *
     * @param string $key
     * @param bool   $onlyIfExists
     *
     * @return \Core\Validate\Chain
     * @throws \OutOfBoundsException
     */
    public function removeValidator($key, $onlyIfExists = false)
    {
        if (
            $onlyIfExists
            and !array_key_exists($key, $this -> _chain)
        ) {
            throw new \OutOfBoundsException('Validator with key' . $key . ' not exists');
        }

        if ( array_key_exists($key, $this -> _chain) ) {
            unset($this -> _chain[$key]);
        }
        return $this;
    } // removeValidator()


    /**
     * @param mixed $data
     * @return bool
     */
    public function isValid($data, array $context = array())
    {
        $result = true;

        foreach ( $this -> _chain as $key => $validator ) {
            if ( is_array($validator) ) {
                foreach( $validator as $v ) {
                    $result = $v -> isValid($data, $context) && $result;
                    if ( $v -> hasMessages() ) {
                        $this -> _messages[$key] = $v -> getMessages();
                    }
                }
            }
            else {
                $result = $validator -> isValid($data, $context) && $result;
                if ( $validator -> hasMessages() ) {
                    $this -> _messages[$key] = $validator -> getMessages();
                }
            }
        }
        return $result;
    } // isValid()


    /**
     * return errors messages
     *
     * @return array
     */
    public function getMessages()
    {
        return $this -> _messages;
    } // getMessages()


    /**
     * @return bool
     */
    public function hasMessages()
    {
        return (bool) count($this -> _messages);
    } // hasMessages()


    /**
     * @param \Core\Validate\IValidator $validator
     * @return string
     */
    protected function _getKey(\Core\Validate\IValidator $validator)
    {
        $result = str_replace(array('/', '\\'), '_', get_class($validator));
        $result = explode('_', $result);
        $result = array_pop($result);
        return $result;
    } // _getKey

} // \Core\Validate\Chain
