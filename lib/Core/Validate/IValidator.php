<?php
namespace Core\Validate;

interface IValidator
{
    public function isValid($value);

    public function hasMessages();

    public function getMessages();
}
