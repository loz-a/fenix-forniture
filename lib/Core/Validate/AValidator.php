<?php
namespace Core\Validate;

abstract class AValidator implements IValidator
{
    /**
     * @var array $_options
     */
    protected $_options = array();

    /**
     * Validation failure message template definitions
     *
     * @var array $_messageTemplates
     */
    protected $_messageTemplates = array();

    /**
     * Array of validation failure messages
     *
     * @var array $_messages
     */
    protected $_messages = array();

    /**
     * @var string $_title
     */
    protected $_title = 'Value';


    public function __construct($title = null, array $options = array())
    {
        $this -> _options = $options;
        $this -> _title = $title;

        if ( array_key_exists('messages', $options) ) {
            $this -> setMessages($options['messages']);
        }
    } // __construct()


    /**
     * @return array
     */
    public function getMessages()
    {
        return $this -> _messages;
    } // getMessages()


    /**
     * @return bool
     */
    public function hasMessages()
    {
        return (bool) count($this -> _messages);
    } // hasMessages()


    /**
     * @param string $message
     * @param string $key
     *
     * @return AValidator
     * @throws \OutOfBoundsException
     */
    public function setMessage($message, $key)
    {
        if ( !array_key_exists($key, $this -> _messageTemplates) ) {
            throw new \OutOfBoundsException("No message template exists for key '$key'");
        }
        $this -> _messageTemplates[$key] = $message;
        return $this;
    } // setMessage()


    public function setMessages($messages)
    {
        if ( !is_array($messages) ) {
            throw new \InvalidArgumentException('Expected parameter should be an array');
        }
        foreach ( $messages as $key => $value ) {
            $this -> setMessage($value, $key);
        }
        return $this;
    } // setMessages()


    /**
     * @return array
     */
    public function getMessageTemplates()
    {
        return $this -> _messageTemplates;
    } // getMessagesTemplates()


    /**
     * @param string $title
     */
    public function setTitle($title)
    {
        $this -> $title = $title;
    } // setTitle()


    /**
     * @return string
     */
    public function getTitle()
    {
        return $this -> _title;
    } // getTitle()


    protected function _error($key)
    {
        $translate = \Core\App\App::GetInstance() -> getTranslate();
        $args = func_get_args();

        array_shift($args);

        if ( !array_key_exists($key, $this -> _messageTemplates) ) {
            throw new \UnexpectedValueException('Message template with key ' . $key . 'not exists');
        }
        $template = $this -> _messageTemplates[$key];
        $template = $translate -> _($template);

        $count = substr_count($template, '%s');
        if ( $count > 0 ) {
            if ( $count !== count($args) ) {
                throw new \LogicException('The number of substitutions is not equal to the number of arguments to replace');
            }

            $transargs = array();
            foreach ( $args as $arg ) {
                $transargs[] = (string) $translate -> _($arg);
            }
            $template = vsprintf($template, $transargs);
            unset($transargs, $args);
        }

        $this -> _messages[$key] = $template;
    } // _error()

} // \Core\Validate\AValidator
