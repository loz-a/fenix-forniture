# --------------------------------------------------------
# Host:                         127.0.0.1
# Server version:               5.1.50-community
# Server OS:                    Win32
# HeidiSQL version:             6.0.0.3603
# Date/time:                    2012-09-16 21:19:37
# --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

# Dumping structure for table fenix_forniture.catalogs
DROP TABLE IF EXISTS `catalogs`;
CREATE TABLE IF NOT EXISTS `catalogs` (
  `id` tinyint(3) unsigned NOT NULL AUTO_INCREMENT,
  `alias` varchar(50) NOT NULL,
  `img` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `alias` (`alias`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;


# Dumping structure for table fenix_forniture.catalogs_translation
DROP TABLE IF EXISTS `catalogs_translation`;
CREATE TABLE IF NOT EXISTS `catalogs_translation` (
  `id` tinyint(3) unsigned NOT NULL,
  `lang` enum('ru','uk') NOT NULL,
  `title` varchar(50) NOT NULL,
  PRIMARY KEY (`id`,`lang`),
  UNIQUE KEY `name` (`title`,`lang`),
  CONSTRAINT `FK_catalogs_translation_catalogs` FOREIGN KEY (`id`) REFERENCES `catalogs` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


# Dumping structure for table fenix_forniture.contacts
DROP TABLE IF EXISTS `contacts`;
CREATE TABLE IF NOT EXISTS `contacts` (
  `lang` enum('ru','uk') NOT NULL,
  `key` varchar(50) NOT NULL,
  `value` varchar(255) NOT NULL,
  PRIMARY KEY (`lang`,`key`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


# Dumping structure for table fenix_forniture.products
DROP TABLE IF EXISTS `products`;
CREATE TABLE IF NOT EXISTS `products` (
  `id` smallint(5) unsigned NOT NULL AUTO_INCREMENT,
  `alias` varchar(50) NOT NULL,
  `img` varchar(255) DEFAULT NULL,
  `catid` tinyint(3) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `alias` (`alias`,`catid`),
  KEY `FK_products_catalogs` (`catid`),
  CONSTRAINT `FK_products_catalogs` FOREIGN KEY (`catid`) REFERENCES `catalogs` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8;


# Dumping structure for table fenix_forniture.products_translation
DROP TABLE IF EXISTS `products_translation`;
CREATE TABLE IF NOT EXISTS `products_translation` (
  `id` smallint(5) unsigned NOT NULL,
  `lang` enum('uk','ru') NOT NULL,
  `title` varchar(50) NOT NULL,
  `size` varchar(25) DEFAULT NULL,
  `material` varchar(255) DEFAULT NULL,
  `price` varchar(25) DEFAULT NULL,
  `description` text,
  PRIMARY KEY (`id`,`lang`),
  UNIQUE KEY `title` (`title`,`lang`),
  CONSTRAINT `FK_products_translation_products` FOREIGN KEY (`id`) REFERENCES `products` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


# Dumping structure for table fenix_forniture.users
DROP TABLE IF EXISTS `users`;
CREATE TABLE IF NOT EXISTS `users` (
  `id` smallint(5) unsigned NOT NULL AUTO_INCREMENT,
  `login` varchar(50) NOT NULL,
  `password` char(60) NOT NULL,
  `role` enum('admin','guest') NOT NULL,
  `last_failure_timeout` timestamp NULL DEFAULT NULL,
  `last_failure_count` tinyint(3) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `login` (`login`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

# Dumping data for table fenix_forniture.users: ~1 rows (approximately)
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` (`id`, `login`, `password`, `role`, `last_failure_timeout`, `last_failure_count`) VALUES
	(1, 'admin', '$2a$10$dcec72f4dcdb58583362fuuNwtgX4vYUIcpyYavlbhoi/Sr7M3B06', 'admin', '2012-07-29 12:37:36', 0);
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
